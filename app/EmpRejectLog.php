<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpRejectLog extends Model
{
    protected $table = 'emp_reject_logs'; 
	

    protected $fillable =[
		'user_id','comments',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
	];	
	
	
    public function rejected_user(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }		
	
}
