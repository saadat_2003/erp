<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
	
    protected $fillable =[
		'user_id', 'department_id', 'title','description','status','is_delete','att_id'
	];	
	protected $dates = [
        'created_at',
		'updated_at'
	];    

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id')->withDefault();
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'complaint_id');
    }

    public function lastcomments()
    {
        return $this->hasOne('App\Comment', 'complaint_id')->latest();
    }
	

}
