<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable =[
		'dated', 'description','leavetype','ispaid','isgroup','user_id', 'status' ,'created_by','modified_by'
	];
	
	protected $dates = [
        'dated',
        'created_at',
		'updated_at'
	];
	
    public function applicant(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }
    
    public function createdby(){
		return $this->belongsTo('App\User','created_by')->withDefault();
	}
    
    public function modifiedby(){
		return $this->belongsTo('App\User','modified_by')->withDefault();
	}

	public function leaveapprovalcheck(){
		return $this->hasMany('App\LeaveApproval','leave_id','id');
    }
	


    public function qa_leave_dept(){
		return $this->belongsTo('App\User','user_id','id')->withDefault();
    }










	
	
}
