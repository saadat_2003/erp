<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeNumber extends Model
{
    protected $table = 'employee_numbers'; 
	

    protected $fillable =[
		'user_id','staff_id','type',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
	];	
	
}
