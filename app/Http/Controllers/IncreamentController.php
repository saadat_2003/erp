<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use \App\UserIncreament;

class IncreamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=\App\User::where('iscustomer',0)->where('status',1)->get();
		//$data = \App\UserIncreament::with('increamentofuser')->with('createdby')->orderBy('id','ASC')->get();
        return view('increament.index',compact('users'));
    }

    public function fetch(Request $request){

        $data = \App\UserIncreament::with('increamentofuser')->with('createdby')->orderBy('id','ASC')->get();
        //dd($data);
        return DataTables::of($data)
        ->addColumn('id',function($data){
            return $data->id;
        })		
        ->addColumn('user_id',function($data){
            return $data->increamentofuser->fname.' '.$data->increamentofuser->lname;
        })
        ->addColumn('amount',function($data){
            return $data->amount;
        })
        ->addColumn('effective_date',function($data){
            return $data->effective_date;
        })
        ->addColumn('comments',function($data){
            return $data->comments;
        })
        ->addColumn('createdby',function($data){
			return $data->createdby->fname.' '.$data->createdby->lname;
         
        })
        ->addColumn('options',function($data){
            $action = '<span class="action_btn">';
			//edit
			if(Auth::user()->can('increament-edit')){
			$action .= '<button type="button" name="Edit" id="'.$data->id.'" 
												class="edit btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>';
												$action .= '&nbsp;&nbsp;';
			}
			
			//delete
			if(Auth::user()->can('increament-destroy')){
			$action .= '<button type="button" name="Delete" id="'.$data->id.'" 
												class="delete btn btn-danger" title="Delete"><i class="fa fa-trash"></i></button>';
												$action .= '&nbsp;&nbsp;';
			}
		
            $action .= '</span>';
            return $action; 
                                
        })
        ->rawColumns(['options','id','name','amount','effective_date','comments','createdby'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		//dd($request->all());
		$rules = array(
			'user_id' => 'required',
			'increament' => 'required',
			'effective_date' => 'required',
			'comments' => 'required',
			
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		
			$form_data = array(
				'user_id'         => $request->get('user_id'),
				'amount'         => $request->get('increament'),
				'effective_date'         => $request->get('effective_date'),
				'comments'        => $request->get('comments'),
				'created_by'        => auth()->user()->id,
				'created_at'         => date('Y-m-d H:i:s'),
				'updated_at'         => date('Y-m-d H:i:s'),
				
			);
			\App\UserIncreament::create($form_data);


		return response()->json(['success' => 'Increament Added.']);			
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(request()->ajax())
        {
            $data = \App\UserIncreament::findOrFail($id);
            return response()->json(['data' => $data]);
        }		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        //
		$rules = array(
			//'increament_of_user' => 'required',
			'increament' => 'required',
			'effective_date' => 'required',
			'comments' => 'required',
			
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		
			$form_data = array(
				//'user_id'         => $request->get('increament_of_user'),
				'amount'         => $request->get('increament'),
				'effective_date'         => $request->get('effective_date'),
				'comments'        => $request->get('comments'),
				//'created_by'        => auth()->user()->id,
				'updated_at'         => date('Y-m-d H:i:s'),
				
			);
			
			UserIncreament::whereId($request->hidden_id)->update($form_data);


		return response()->json(['success' => 'Increament Updated.']);		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = UserIncreament::findOrFail($id);
        $data->delete();
    }
}
