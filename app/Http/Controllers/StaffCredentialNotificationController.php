<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use \App\Staffdetail;
use \App\StaffConfirmationDetail;

class StaffCredentialNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=\App\User::where('iscustomer',0)->where('status',1)->get();
		//$data = \App\UserIncreament::with('increamentofuser')->with('createdby')->orderBy('id','ASC')->get();
        return view('staffcredentialnotification.index',compact('users'));
    }

    public function fetch(Request $request){
		$status = 1;
		$iscustomer = 0;		
        //$data = \App\Staffdetail::with('user')->where('is_confirm',1)->get();
/* 		$data = Staffdetail::with('user')->with('StaffConfirmationDetail_relation')->where('is_confirm', '=', 1)
		->whereHas('user', function($q) use($status,$iscustomer){
					$q->where('status', '=', $status);
					$q->where('iscustomer', '=', $iscustomer);
					//$q->where('user_id', '=', 269);	
				})
		->whereHas('StaffConfirmationDetail_relation', function($q) {
					$q->orwhere('user_id','!=',0);
					$q->orderBy('date_confirm','DESC');
				})		
				->get(); */
				
				$data =  
				 \App\Staffdetail:: select(
				'staffdetails.id as id',
				'staffdetails.user_id as sd_user_id',
				'staffdetails.is_confirm as is_confirm',
				'staffdetails.comments_confirm as comments_confirm',
				'staffdetails.date_confirm as date_confirm',

				
				'users.id as u_id',
				'users.fname as fname',
				'users.lname as lname', 
				
				'staff_confirmation_details.id as scd_id','staff_confirmation_details.user_id as scd_user_id')
				->with('user')->with('StaffConfirmationDetail_relation')
				->selectRaw('staff_confirmation_details.user_id, count(staff_confirmation_details.user_id) AS `countd`')
				->leftjoin('users','users.id','=','staffdetails.user_id')
				->leftjoin('staff_confirmation_details','staff_confirmation_details.user_id', '=', 'staffdetails.user_id')
				->groupBy('staff_confirmation_details.user_id')
				
				->where('users.status', '=', 1)
				->where('users.iscustomer', '=', 0)
				->where('staffdetails.is_confirm', '=', 1)
				->orderBy('countd', 'ASC')
				->orderBy('staffdetails.date_confirm', 'DESC')
				->get();					
        //dd($data);
		//$data = collect($data);
		//dd($data->count());
        return DataTables::of($data)
/*         ->addColumn('id',function($data){
            return $data->id;
        }) */		
        ->addColumn('name',function($data){
            return $data->fname.' '.$data->lname;
        })
        ->addColumn('date_confirm',function($data){
            return date('d-M-Y H:i:s',strtotime($data->date_confirm));
        })

		->addColumn('status', function($data){
			$status = "";
			if($data->scd_user_id==NULL) {
			  $status .= '<span class="label label-warning">No Details</span>';
			}else{
			  $status .= '<span class="label label-success">With Deatils</span>';
			}  
			$status .= '&nbsp;&nbsp;';                    
			return $status;			
		})						
		
        ->addColumn('options',function($data){
            $action = '<span class="action_btn">';
			$action .= '<button type="button" name="user_pass" empId_user_pass="'.$data->sd_user_id.'" class="emp_user_pass btn btn-primary" title="Add Username/Password"><i class="fa fa-plus"></i></button>';
												$action .= '&nbsp;&nbsp;';
            $action .= '</span>';
            return $action; 
                                
        })
        ->rawColumns(['options','id','name','date_confirm','status'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
