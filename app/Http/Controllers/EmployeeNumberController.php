<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use \App\EmployeeNumber;

use Illuminate\Contracts\Encryption\DecryptException;

class EmployeeNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		//dd($request->all());
		if($request->get('type')=="Mobile"){
			$rules = array(
				'curr_emp_id' => 'required',
				'staff_id' => 'required',
				
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}	
			
				$form_data = array(
					'user_id'				=> $request->get('curr_emp_id'),
					'staff_id'				=> $request->get('staff_id'),
					'type'					=> $request->get('type'),
					'created_at'			=> date('Y-m-d H:i:s'),
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);			
				\App\EmployeeNumber::create($form_data);
				$empdata = User::find($request->get('staff_id'));
				//dd($empdata);
				// decrypt exception
				try {
					$decrypted_phonenumber = decrypt($empdata->phonenumber);
				} catch (DecryptException $e) {
					//
					return response()->json(['errors' => "decrypt error"]);
				}				
				$returnHTML = $decrypted_phonenumber;
			//return response()->json(['success' => 'Number Shown.']);	
			return response()->json(array('success' => 'Mobile Shown.', 'html'=>$returnHTML));		
		}	
		
		if($request->get('type')=="Landline"){
			$rules = array(
				'curr_emp_id' => 'required',
				'staff_id' => 'required',
				
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}	
			
				$form_data = array(
					'user_id'				=> $request->get('curr_emp_id'),
					'staff_id'				=> $request->get('staff_id'),
					'type'					=> $request->get('type'),
					'created_at'			=> date('Y-m-d H:i:s'),
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);			
				\App\EmployeeNumber::create($form_data);
				$empdata = User::with('staffdetails')->find($request->get('staff_id'));
				//dd($empdata);
				$returnHTML = decrypt($empdata->staffdetails->landline);
			//return response()->json(['success' => 'Number Shown.']);	
			return response()->json(array('success' => 'Landline Shown.', 'html'=>$returnHTML));		
		}
		
		if($request->get('type')=="Gaurdian"){
			$rules = array(
				'curr_emp_id' => 'required',
				'staff_id' => 'required',
				
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}	
			
				$form_data = array(
					'user_id'				=> $request->get('curr_emp_id'),
					'staff_id'				=> $request->get('staff_id'),
					'type'					=> $request->get('type'),
					'created_at'			=> date('Y-m-d H:i:s'),
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);			
				\App\EmployeeNumber::create($form_data);
				$empdata = User::with('staffdetails')->find($request->get('staff_id'));
				//dd($empdata);
				$returnHTML = decrypt($empdata->staffdetails->gaurdiancontact);
			//return response()->json(['success' => 'Number Shown.']);	
			return response()->json(array('success' => 'Gaurdian Number Shown.', 'html'=>$returnHTML));		
		}	
	
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
