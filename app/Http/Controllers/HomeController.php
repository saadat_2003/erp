<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Calendar;
use Carbon\Carbon;
use App\Hrlead;
use App\Notification;
use Session;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('customer');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('dashboard');
        return redirect()->route('dashboard');
    }

    public function dashboard(Request $request)
    {
		$currentMonth = date('m');
		$currentYear = date('Y');        
		//filtered dates
		$from = $request->get('dateFrom');
		$to = $request->get('dateTo');		
        /* Sales, Project, Calendar and Lead Stats Begins */
		if( $request->get('dateFrom') && $request->get('dateTo') ){		
			$leads = \App\Lead::with('user')->with('createdby')->whereBetween('created_at',[$from, $to])->orderBy('id', 'DESC')->limit(5)->get();
		}else{
			$leads = \App\Lead::with('user')->with('createdby')->whereBetween('created_at',[$currentMonth, $currentYear])->orderBy('id', 'DESC')->limit(5)->get();
		}
		if( $request->get('dateFrom') && $request->get('dateTo') ){
			$appointments = \App\Appointment::with('lead')->with('createdby')->whereBetween('created_at',[$from, $to])->orderBy('id', 'DESC')->limit(5)->get();
		}else{
			$appointments = \App\Appointment::with('lead')->with('createdby')->whereBetween('created_at',[$currentMonth, $currentYear])->orderBy('id', 'DESC')->limit(5)->get();
		}
        $recentappointments = \App\Appointment::with('lead')->with('createdby')->whereBetween('appointtime', array(date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')))->orderBy('appointtime', 'ASC')->limit(5)->get(); 
        $recordings = \App\Recording::with('lead')->with('createdby')->orderBy('id', 'DESC')->limit(5)->get();
        $proposals = \App\Proposal::with('lead')->with('createdby')->whereNull('docfile')->orderBy('id', 'DESC')->limit(10)->get();
        $myappointments = auth()->user()->appointments()->withPivot('user_id')->orderby('id','DESC')->get(); 
        //Get Counts
        $statistics_count=array();
        //Project Counts
		//$statistics_count['projects']=\App\Project::count();
		//$statistics_count['leads']=\App\Lead::count();
		//$statistics_count['recordings']=\App\Recording::count();
        //$statistics_count['appointments']=\App\Appointment::count();		
		 
		//Projects
		if( $request->get('dateFrom') && $request->get('dateTo') ){
			$statistics_count['projects']=\App\Project::whereBetween('created_at', [$from, $to])->count();
		}else{
			$statistics_count['projects'] =  DB::table("projects")
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
		}
		//leads
		if( $request->get('dateFrom') && $request->get('dateTo') ){	
			$statistics_count['leads']=\App\Lead::whereBetween('created_at', [$from, $to])->count();		
		}else{	
			$statistics_count['leads']=\App\Lead::whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])->count();
		}
		//recordings
		if( $request->get('dateFrom') && $request->get('dateTo') ){	
			$statistics_count['recordings']=\App\Recording::whereBetween('created_at', [$from, $to])->count();		
		}else{	
			$statistics_count['recordings']=\App\Recording::whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])->count();
		}
		//appointments
		if( $request->get('dateFrom') && $request->get('dateTo') ){	
			$statistics_count['appointments']=\App\Appointment::whereBetween('created_at', [$from, $to])->count();		
		}else{	
			$statistics_count['appointments']=\App\Appointment::whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])->count();
		}		
		

        $date1 = date('Y-m-d', strtotime('-10 days'));
        $date2 = date('Y-m-d');
        $period = new DatePeriod(new DateTime($date1), new DateInterval('P1D'), new DateTime($date2));
        foreach ($period as $date) {
            $dates[] = $date->format("Y-m-d");
        }
        //For Leads
        foreach($dates as $date){
            $leadcount = DB::table('leads')
             ->select('created_at',DB::raw("DATE_FORMAT(created_at, '%d-%b') as createdat"), DB::raw('count(*) as leads'))           
             ->whereDate('created_at',$date)
             ->groupBy('created_at')
             ->first();            
            if(!empty($leadcount)){
                    $linecharts['createdat'][]=$leadcount->createdat;
                    $linecharts['leads'][]=$leadcount->leads;
               
            }else {
                $date = strtotime($date);
                $dated=date('d-M', $date);
                $linecharts['createdat'][]=$dated;
                $linecharts['leads'][]=0;
            }
        }
        
        $chartcreatedat=json_encode($linecharts['createdat']);
        $chartleads=json_encode($linecharts['leads']);
        //For Appointments
        foreach($dates as $date){
            $appointmentcount = DB::table('appointments')
             ->select('created_at',DB::raw("DATE_FORMAT(created_at, '%d-%b') as createdat"), DB::raw('count(*) as appointments'))           
             ->whereDate('created_at',$date)
             ->groupBy('created_at')
             ->first();            
            if(!empty($appointmentcount)){
                    $appointmentcharts['createdat'][]=$appointmentcount->createdat;
                    $appointmentcharts['appointments'][]=$appointmentcount->appointments;
               
            }else {
                $date = strtotime($date);
                $dated=date('d-M', $date);
                $appointmentcharts['createdat'][]=$dated;
                $appointmentcharts['appointments'][]=0;
            }
        }
        
        $chartcreatedat=json_encode($linecharts['createdat']);
        $chartleads=json_encode($linecharts['leads']);

        $appointmentchartcreatedat=json_encode($appointmentcharts['createdat']);
        $chartleadsappointments=json_encode($appointmentcharts['appointments']);
        $calendar=[];
        if(auth()->user()->can('show-dashboard-calendar')){
            //Appointment Calendar Begins
            $events = [];
            $apppointmentdata = \App\Appointment::with('lead')->with('createdby')->whereDate('appointtime', '>', Carbon::now())->get(); 
            //dd($apppointmentdata->toArray());
            if($apppointmentdata->count()) {
                foreach ($apppointmentdata as $key => $value) {
                    $events[] = Calendar::event(
                        $value->note,
                        false,
                        new \DateTime($value->appointtime),
                        new \DateTime($value->appointtime.' +2 hours'),
                        $value->id,
                        // Add color and link on event
                        [
                            'color' => '#f05050',
                            'url' => url('/leads/'.$value->lead->id ),
                        ]
                    );
                }
            }
            $calendar = Calendar::addEvents($events);
            //Appointment Calendar Ends
        }
        /* Sales, Project, Calendar and Lead Stats ends */
        /* HR Stats Begins */
            $hrstats=array();
            $hrstats['activestaff']= 0;
            $hrstats['joined']=0;
            $hrstats['left']=0;
            $hrstats['requests']=0;
            $hrstats['totalrequests']=0;
            $hrstats['completedrequest']=0;
            $hrstats['inprocessdrequest']=0;
            $hrstats['pendingrequest']=0;
            if(auth()->user()->can('stats-hr')){
                $currentMonth = date('m');
                $currentYear = date('Y');
                $hrstats['activestaff']= \App\User::where('iscustomer', 0)->where('status', 1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){
					$joined =  DB::table("staffdetails")
                            ->whereRaw('joiningdate >= ? AND joiningdate <= ?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();					
				}else{
					$joined =  DB::table("staffdetails")
                            ->whereRaw('MONTH(joiningdate) = ? and YEAR(joiningdate) = ?',[$currentMonth, $currentYear])
                            ->count();
				}
                $hrstats['joined']=$joined;

				if( $request->get('dateFrom') && $request->get('dateTo') ){				
					$left =  DB::table("staffdetails")
                            ->whereRaw('endingdate >= ? AND endingdate <= ?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();	
				}else{
					$left =  DB::table("staffdetails")
                            ->whereRaw('MONTH(endingdate) = ? and YEAR(endingdate) = ?',[$currentMonth, $currentYear])
                            ->count();					
				}
                $hrstats['left']=$left;

				if( $request->get('dateFrom') && $request->get('dateTo') ){
					$hrstats['totalrequests']=DB::table("staff_requireds")
											->whereRaw('created_at >= ? AND created_at <= ? AND is_deleted=0',[$from." 00:00:00", $to." 23:59:59"])
                                            ->count();
				}else{				
					$hrstats['totalrequests']=DB::table("staff_requireds")
                                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ? AND is_deleted=0',[$currentMonth, $currentYear])
                                            ->count();
				}
                $reqstatus=['Completed', 'Fullfilled'];
				if( $request->get('dateFrom') && $request->get('dateTo') ){
					$hrstats['completedrequest']=DB::table("staff_requireds")
                                            ->whereRaw('created_at >= ? AND created_at <= ? AND status in (?,?) AND is_deleted=0',[$from." 00:00:00", $to." 23:59:59", $reqstatus])
                                            ->count();
				}else{				
					$hrstats['completedrequest']=DB::table("staff_requireds")
                                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ? AND status in (?,?) AND is_deleted=0',[$currentMonth, $currentYear, $reqstatus])
                                            ->count();
				}
				if( $request->get('dateFrom') && $request->get('dateTo') ){
					$hrstats['inprocessdrequest']=DB::table("staff_requireds")
                                            ->whereRaw('created_at >= ? AND created_at <= ? AND status = ? AND is_deleted=0',[$from." 00:00:00", $to." 23:59:59",'In Progress'])
                                            ->count();
				}else{				
					$hrstats['inprocessdrequest']=DB::table("staff_requireds")
                                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ? AND status = ? AND is_deleted=0',[$currentMonth, $currentYear,'In Progress'])
                                            ->count();
				}
				if( $request->get('dateFrom') && $request->get('dateTo') ){
					$hrstats['pendingrequest']=DB::table("staff_requireds")
                                            ->whereRaw('created_at >= ? AND created_at <= ? AND status = ? AND is_deleted=0',[$from." 00:00:00", $to." 23:59:59",'Pending'])
                                            ->count();
				}else{				
					$hrstats['pendingrequest']=DB::table("staff_requireds")
                                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ? AND status = ? AND is_deleted=0',[$currentMonth, $currentYear,'Pending'])
                                            ->count();
				}



            }
        /* HR Stats Ends */
		
        /* HR Stats Leads Begins */
            $hrstats['leadsthismonth']= 0;
            $hrstats['leads_rejected']=0;
			$hrstats['leads_new']=0;
			$hrstats['leads_not_picking_call']=0;
			$hrstats['leads_incorrect_information']=0;
			$hrstats['leads_call_made']=0;
			$hrstats['leads_interview_scheduled']=0;
			$hrstats['leads_not_appeared']=0;
			$hrstats['leads_rescheduled']=0;
			$hrstats['leads_appeared']=0;
			$hrstats['leads_second_interview']=0;
			$hrstats['leads_short_listed']=0;
			$hrstats['leads_selected']=0;
			$hrstats['leads_not_joined']=0;
			$hrstats['leads_joined']=0;
			$hrstats['leads_unknown']=0;
			
            if(auth()->user()->can('stats-hr-leads')){
                $currentMonth = date('m');
                $currentYear = date('Y');
                //$hrstats['leadsthismonth']= \App\Hrlead::count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					//$hrstats['leadsthismonth'] =  \App\Hrlead::whereBetween('created_at', [$from, $to])->count();	
					$hrstats['leadsthismonth'] =  DB::table("hrleads")
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();
				}else{	
					$hrstats['leadsthismonth'] =  DB::table("hrleads")
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}
				
				//Rejected
				//$hrstats['leads_rejected']= \App\Hrlead::where('status',0)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_rejected'] =  DB::table("hrleads")
							->where('status',0)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();							
				}else{					
					$hrstats['leads_rejected'] =  DB::table("hrleads")
							->where('status',0)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}
				
				//New
				//$hrstats['leads_new']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_new'] =  DB::table("hrleads")
							->where('status',1)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();							
				}else{				
					$hrstats['leads_new'] =  DB::table("hrleads")
							->where('status',1)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}
							
				//leads_not_picking_call
				//$hrstats['leads_not_picking_call']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_not_picking_call'] =  DB::table("hrleads")
							->where('status',2)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();						
				}else{				
					$hrstats['leads_not_picking_call'] =  DB::table("hrleads")
							->where('status',2)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();	
				}							
							
				//leads_incorrect_information
				//$hrstats['leads_incorrect_information']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_incorrect_information'] =  DB::table("hrleads")
							->where('status',3)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();							
				}else{					
					$hrstats['leads_incorrect_information'] =  DB::table("hrleads")
							->where('status',3)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}							
							
				//leads_call_made
				//$hrstats['leads_call_made']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_call_made'] =  DB::table("hrleads")
							->where('status',4)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();						
				}else{					
					$hrstats['leads_call_made'] =  DB::table("hrleads")
							->where('status',4)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}							
							
				//leads_interview_scheduled
				//$hrstats['leads_interview_scheduled']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_interview_scheduled'] =  DB::table("hrleads")
							->where('status',5)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();						
				}else{					
					$hrstats['leads_interview_scheduled'] =  DB::table("hrleads")
							->where('status',5)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}							
							
				//leads_not_appeared
				//$hrstats['leads_not_appeared']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_not_appeared'] =  DB::table("hrleads")
							->where('status',6)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();						
				}else{					
					$hrstats['leads_not_appeared'] =  DB::table("hrleads")
							->where('status',6)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}

				//leads_rescheduled
				//$hrstats['leads_rescheduled']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_rescheduled'] =  DB::table("hrleads")
							->where('status',7)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();							
				}else{				
					$hrstats['leads_rescheduled'] =  DB::table("hrleads")
							->where('status',7)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}

				//leads_appeared
				//$hrstats['leads_appeared']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_appeared'] =  DB::table("hrleads")
							->where('status',8)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();					
				}else{					
					$hrstats['leads_appeared'] =  DB::table("hrleads")
							->where('status',8)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}

				//leads_second_interview
				//$hrstats['leads_second_interview']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_second_interview'] =  DB::table("hrleads")
							->where('status',9)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();					
				}else{					
					$hrstats['leads_second_interview'] =  DB::table("hrleads")
							->where('status',9)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}

				//leads_short_listed
				//$hrstats['leads_short_listed']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_short_listed'] =  DB::table("hrleads")
							->where('status',10)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();						
				}else{				
					$hrstats['leads_short_listed'] =  DB::table("hrleads")
							->where('status',10)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}

				//leads_selected
				//$hrstats['leads_selected']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_selected'] =  DB::table("hrleads")
							->where('status',11)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();					
				}else{					
					$hrstats['leads_selected'] =  DB::table("hrleads")
							->where('status',11)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}
							
				//leads_not_joined
				//$hrstats['leads_not_joined']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_not_joined'] =  DB::table("hrleads")
							->where('status',12)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();					
				}else{				
					$hrstats['leads_not_joined'] =  DB::table("hrleads")
							->where('status',12)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}							
							
				//leads_joined
				//$hrstats['leads_joined']= \App\Hrlead::where('status',1)->count();
				if( $request->get('dateFrom') && $request->get('dateTo') ){	
					$hrstats['leads_joined'] =  DB::table("hrleads")
							->where('status',13)
                            ->whereRaw('created_at>=? AND created_at<=?',[$from." 00:00:00", $to." 23:59:59"])
                            ->count();						
				}else{				
					$hrstats['leads_joined'] =  DB::table("hrleads")
							->where('status',13)
                            ->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?',[$currentMonth, $currentYear])
                            ->count();
				}
            }
		/* HR Stats Leads Ends */
		
		/* Get Preference Begins */
			$preferences= \App\Preference::whereIn('option',['manualatt'])->get();
			foreach($preferences as $preference){
				/*if($preference->option=='manualatt'){
					$settings['manualatt']=$preference->value;
				}*/
				$settings[$preference->option]=$preference->value;
			}
		/* Get Preference Ends */

		/* Check If Last Checkout is Missing begins */
		$checkoutmissing=0;
		$attdated="";
		$attdatedsrv="";
		$checkatt = \App\Attendancesheet::where('user_id',auth()->user()->id)->where('status','!=','X')->orderBy('id', 'desc')->first();
		if(!empty($checkatt)){
			if($checkatt->checkoutfound=='No'){
				$checkoutmissing=1;
				$attdated=$checkatt->dated->format('Y-m-d');
				$attdatedsrv=$checkatt->dated->format('d-M-Y');
			}else{
				$attdated=$checkatt->dated->format('Y-m-d');
				$attdatedsrv=$checkatt->dated->format('d-M-Y');
			}
		}
		/* Check If Last Checkout is Missing ends */

		$meetinglinks = \App\Meetinglink::get();      
        return view('dashboard',compact('leads','appointments','recentappointments','myappointments','recordings','proposals','statistics_count','chartcreatedat','chartleads','appointmentchartcreatedat','chartleadsappointments','calendar','hrstats','settings','checkoutmissing','attdated','attdatedsrv','meetinglinks'));
        
	}
	

	public function markmanualatt(Request $request)
    {
        
        $this->authorize('manualatt');
		/* Add New Attendance Record Begins */
			$user=auth()->user();
			$attdated=date_create($request->get('dated'));
			$format = date_format($attdated,"Y-m-d H:i:s");
			$timed = date_format($attdated,"H:i:s");
			$dated = date_format($attdated,"Y-m-d");
			$msg_dated = date_format($attdated,"d-M-Y");
			$checkin = $timed;
			$checkout= $timed;
			$status="P";
			$tardycount=0;
			$shortleavecount=0;
			$paid=1;
			$remarks="Present -  Manual Attendance By User";
			$day = date('D', strtotime($format));
			$workedhours=0;
			//Check If today is holiday begins
			$holiday=\App\Holiday::where('dated', $dated)->first();
			if(isset($holiday) && $holiday->isworking==0){
				$status="H";
				$tardycount=0;
				$shortleavecount=0;
				$remarks=$holiday->description;
				$paid=1;
			}elseif(isset($holiday) && $holiday->isworking==1){
				$status="P";
				$tardycount=0;
				$shortleavecount=0;
				$remarks=$holiday->description;
				$paid=1;
			}
			unset($holiday);
			//Check If today is holiday ends
			
			//Create object and Insert or Update begins
                                        
			//Check if record already exists begins
			$updateatt=1;

			//Check If any exisiting checkout found beings
			$checkatt = \App\Attendancesheet::where('user_id',auth()->user()->id)->where('status','!=','X')->orderBy('id', 'desc')->first();
			if(!empty($checkatt) && $checkatt->checkoutfound=='No' ){
				$dated=$checkatt->dated->format('d-M-Y');
				$checkatt->checkout=$checkout;
				$checkatt->checkoutfound='Yes';
				$checkatt->updated_at = strtotime($format);
				$checkatt->save();
				$message='Check-out marked successfully for: '.$dated;
				return response()->json(['success'=>$message]);
			}
			//Check If any exisiting checkout found beings

			$newatt = \App\Attendancesheet::where('user_id',$user->id)->where('dated',$dated)->first();
			if(empty($newatt)){
				$newatt = new \App\Attendancesheet;
			}else{                                            
				//Check if Att record manually updated begins
				if($newatt->isupdated==1){
					$updateatt=0;
				}
				//Check if Att record manually updated ends                    
			}
			//Check if record already exists ends
			if($updateatt==1){
				$newatt->user_id=$user->id;
				$newatt->dated=$dated;
				$newatt->dayname=$day;
				$newatt->remarks=$remarks;
				$newatt->paid=$paid;
				$newatt->attendancedate=$attdated;
				$newatt->checkin=$checkin;
				$newatt->checkout=$checkout;
				$newatt->checkoutfound='No';
				$newatt->tardies=$tardycount;
				$newatt->shortleaves=$shortleavecount;
				$newatt->workedhours=$workedhours;
				$newatt->status=$status;
				$newatt->isupdated=1;
				$newatt->modifiedby=$user->id;
				$newatt->created_at = strtotime($format);
				$newatt->updated_at = strtotime($format);
				$newatt->save();
				$message='Check-in marked successfully: '.$msg_dated;
			}else{
				$newatt->checkout=$checkout;
				$newatt->checkoutfound='Yes';
				$newatt->updated_at = strtotime($format);
				$newatt->save();
				$message='Check-out marked successfully: '.$msg_dated;
			}
			//Create object and Insert or Update ends
        return response()->json(['success'=>$message]);
        
	}



	public function markmanualattnew(Request $request)
    {
        
		$this->authorize('manualatt');
			/* Add New Attendance Record Begins */
			//Get Preferences begins
			$latecoming=0;
			$earlygoing=0;
			$tardylimit=0;
			$satrudayearlyleaving=0;

			$generallatecoming=0;
			$generalearlygoing=0;
			
			$preferences= \App\Preference::whereIn('option',['latecomming','earlyleaving', 'tardylimit', 'satrudayearlyleaving'])->get();
			
			foreach($preferences as $preference){
				if($preference->option=='latecomming'){
					$latecoming=$preference->value;
					$generallatecoming=$preference->value;
				}
				if($preference->option=='earlyleaving'){
					$earlygoing=$preference->value;
					$generalearlygoing=$preference->value;
				}
				if($preference->option=='tardylimit'){
					$tardylimit=$preference->value;
				}
				if($preference->option=='satrudayearlyleaving'){
					$satrudayearlyleaving=$preference->value;
				}
			}


		
			$user=auth()->user();
			$attdated=date_create($request->get('dated'));
			$format = date_format($attdated,"Y-m-d H:i:s");
			$timed = date_format($attdated,"H:i:s");
			$dated = date_format($attdated,"Y-m-d");
			$msg_dated = date_format($attdated,"d-M-Y");
			$checkin = $timed;
			$checkout= $timed;
			$status="P";
			$tardycount=0;
			$shortleavecount=0;
			$paid=1;
			$remarksmanual="Present -  Manual Attendance By User. ";
			$remarks="";
			$day = date('D', strtotime($format));
			$checkinlog="Input Dated: ".$format." <br>";
			$checkoutlog="<hr>Input Dated: ".$format." <br>";



			//New Code Begins			
			$attendancedate = date_format($attdated,"Y-m-d H:i:s");
			$attendancedatelast = date_format($attdated,"Y-m-d H:i:s");
			$checkindate = date_format($attdated,"Y-m-d");                   
			//$dated = date_format($firstlog->attendancedate,"Y-m-d");
			//$dated = date('Y-m-d H:i:s', strtotime($row['from']));
			$checkindate=$checkindate.' '.$user->staffdetails->starttime;
			$checkoutdate = date_format($attdated,"Y-m-d");                   
			$checkoutdate=$checkoutdate.' '.$user->staffdetails->endtime;

			$checkinlog.='Clock Marked='.$attendancedate." <br>";
			$checkinlog.='Setting Clock In='.$checkindate." <br>";
			$checkinlog.='Setting Clock Out='.$checkoutdate." <br>";
			/*echo 'Clock Marked='.$attendancedate;
			echo "<br>";
			echo 'Setting Clock In='.$checkindate;
			echo "<br>";
			echo 'Setting Clock Out='.$checkoutdate;
			echo "<br>";*/
			
			//CheckIn status based on Time
			if(!empty($latecoming) or $latecoming!==null){
				$to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $checkindate);
				$from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedate);
				$diff_in_minutes = $to->diffInMinutes($from,false); 
				/*echo "Check in time different: ".$diff_in_minutes;
				echo "<br> Late Coming Margin: ".$latecoming;
				echo "<br> Tardy Limit Margin: ".$tardylimit;*/
				$checkinlog.='Check In (To)='.$to." <br>";
				$checkinlog.='Check Out (From)'.$from." <br>";
				$checkinlog.="Check in time different: ".$diff_in_minutes." <br>";

				if($diff_in_minutes > 0 && $diff_in_minutes > $latecoming && $diff_in_minutes < $tardylimit){
					$status="P";
					$tardycount=1;
					$remarks="Late Arrival";
					$paid=2;
				}elseif($diff_in_minutes > 0 &&  $diff_in_minutes > $latecoming && $diff_in_minutes > $tardylimit){
					$status="P";
					$shortleavecount=1;
					$remarks="Short Leave Due to Late arrival";
					$paid=2;
				}
			}
			//echo "<br> Remarks: ".$remarks."<br>";
			//echo $status."<br>";		
			//Check If today is holiday begins
				$holiday=\App\Holiday::where('dated', $dated)->first();
				if(isset($holiday) && $holiday->isworking==0){
					$status="H";
					$tardycount=0;
					$shortleavecount=0;
					$remarks=$holiday->description;
					$paid=1;
				}elseif(isset($holiday) && $holiday->isworking==1){
					$status="P";
					$tardycount=0;
					$shortleavecount=0;
					$remarks=$holiday->description;
					$paid=1;
				}
				unset($holiday);
			//Check If today is holiday ends

			//Sunday Check begins
			if($day=='Sun'){
				$status="P";
				$tardycount=0;
				$shortleavecount=0;
				$remarks='Sunday';
				$paid=1;
			}
			//Sunday Check ends

			//New Code Ends
			//Create object and Insert or Update begins
										
			//Check if record already exists begins
			$updateatt=1;

			//Check If any exisiting checkout found beings
			$checkatt = \App\Attendancesheet::where('user_id',auth()->user()->id)->where('status','!=','X')->orderBy('id', 'desc')->first();
			if(!empty($checkatt) && $checkatt->checkoutfound=='No' ){
				$tardycount=0;
				$shortleavecount=0;
			//Check if its checkout time begins				
			if(!empty($earlygoing) or $earlygoing!==null){
				$workedhours=0;
				$to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', date_format($checkatt->dated,"Y-m-d").' '.$checkatt->checkin);
				$from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedatelast);
				$workedhours = $to->diffInHours($from,false);
				$checkoutlog.="Workd Hours Begins<br>";
				$checkoutlog.='Check In (To)='.$to." <br>";
				$checkoutlog.='Check Out (From)'.$from." <br>";
				$checkoutlog.="Workd Hours: ".$workedhours." <br>";
				$checkoutlog.="Worked mins: ".$workedmins = $to->diffInMinutes($from,false)." <br>";
				$checkoutlog.="Workd Hours Ends <br>";
				
				/*echo $to;
				echo "<br>";
				echo $from;
				echo "<br>Worked mins: ".$workedmins = $to->diffInMinutes($from,false);
				echo "<br>";
				echo $workedhours;
				echo "<br>Check Out condition<br>";*/
				$to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedatelast);
				$from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $checkoutdate);
				$checkout_check_min = $to->diffInMinutes($from,false);
				$checkoutlog.="Check out time different: ".$checkout_check_min."<br>";
				$checkoutlog.="Early going margin: ".$earlygoing."<br>";
				$checkoutlog.="Tardy limit margin: ".$tardylimit."<br>";
				/*echo $to;
				echo "<br>";
				echo $from;
				echo "<br> Check out time different: ".$checkout_check_min;
				echo "<br> Early going margin: ".$earlygoing;
				echo "<br> Tardy limit margin: ".$tardylimit;
				echo "<br>";
				exit;*/
				if($checkout_check_min > 0 && $checkout_check_min > $earlygoing && $checkout_check_min < $tardylimit ){
					//Saturday Check begins
					if($day=='Sat' && $checkout_check_min <= $satrudayearlyleaving ){
						$status="P";
						$remarks="Saturday Exception";
					}else{
						$status="P";
						$remarks="Early Left";						
						$paid=2;
						$tardycount=1;
					}
					//Saturday Check ends

				}elseif($checkout_check_min > 0  && $checkout_check_min > $earlygoing && $checkout_check_min > $tardylimit){
					//Saturday Check begins
					if($day=='Sat' && $checkout_check_min <= $satrudayearlyleaving ){
						$status="P";
						$remarks="Saturday Exception";
					}else{
						$status="P";
						$remarks="Short Leave Due to Early Left";
						$paid=2;
						$shortleavecount=1;
						
					}
					//Saturday Check ends
				}

				}
				//Check If today is holiday begins
				$holiday=\App\Holiday::where('dated', $dated)->first();
				if(isset($holiday) && $holiday->isworking==0){
					$status="H";
					$tardycount=0;
					$shortleavecount=0;
					$remarks=$holiday->description;
					$paid=1;
				}elseif(isset($holiday) && $holiday->isworking==1){
					$status="P";
					$tardycount=0;
					$shortleavecount=0;
					$remarks=$holiday->description;
					$paid=1;
				}
				unset($holiday);
				//Check If today is holiday ends

				//Sunday Check begins
				if($day=='Sun'){
					$status="P";
					$tardycount=0;
					$shortleavecount=0;
					$remarks='Sunday';
					$paid=1;
				}
				//Sunday Check ends

				$dated=$checkatt->dated->format('d-M-Y');
				if($user->staffdetails->attendancecheck!==1){
					$tardycount=0;
					$shortleavecount=0;
					$paid=1;
				}
				$checkatt->checkout=$checkout;
				if($checkatt->tardies==0){
					$checkatt->tardies=$tardycount;
				}
				if($checkatt->shortleaves==0){
					$checkatt->shortleaves=$shortleavecount;
				}
				$alert="";
				if($tardycount > 0){
					$alert=" with early going.";
				}
				if($shortleavecount > 0){
					$alert=" with short leave due to early going.";
				}
				$checkatt->workedhours=$workedhours;
				$checkatt->checkoutfound='Yes';
				$checkatt->remarks=$remarksmanual.$remarks;
				$checkatt->updated_at = strtotime($format);
				$checkatt->checkoutlog = $checkoutlog.$remarks;
				$checkatt->save();
				$message='Check-out marked successfully for: '.$dated.$alert;
				return response()->json(['success'=>$message]);
			}
			//Check If any exisiting checkout found beings

			$newatt = \App\Attendancesheet::where('user_id',$user->id)->where('dated',$dated)->first();
			if(empty($newatt)){
				$newatt = new \App\Attendancesheet;
				if($user->staffdetails->attendancecheck!==1){
					$tardycount=0;
					$shortleavecount=0;
					$paid=1;
				}
				

				$newatt->user_id=$user->id;
				$newatt->dated=$dated;
				$newatt->dayname=$day;
				$newatt->remarks=$remarksmanual.$remarks;
				$newatt->paid=$paid;
				$newatt->attendancedate=$attdated;
				$newatt->checkin=$checkin;
				$newatt->checkout=$checkout;
				$newatt->checkoutfound='No';
				$newatt->tardies=$tardycount;
				$newatt->shortleaves=$shortleavecount;
				$newatt->workedhours=0;
				$newatt->status=$status;
				$newatt->isupdated=1;
				$newatt->modifiedby=$user->id;
				$newatt->created_at = strtotime($format);
				$newatt->updated_at = strtotime($format);
				$newatt->checkinlog = $checkinlog.$remarks;		
				$newatt->save();
				$alert="";
				if($tardycount > 0){
					$alert=" with late comming.";
				}

				if($shortleavecount > 0){
					$alert=" with short leave.";
				}

				$message='Check-in marked successfully for '.$msg_dated.$alert;
			}
			//Create object and Insert or Update ends
		return response()->json(['success'=>$message]);
        
	}


	//Fix Daily Att Begins
	public function fixdailyatt(Request $request)
    {
		echo "From Date: ".$request->get('fromdate');
		echo "<br>";
		echo "To Date: ". $request->get('todate');
		echo "<br>";
            //exit;      
            //Get Preferences begins
            $latecoming=0;
            $earlygoing=0;
            $tardylimit=0;
            $satrudayearlyleaving=0;

            $generallatecoming=0;
            $generalearlygoing=0;
            
            $preferences= \App\Preference::whereIn('option',['latecomming','earlyleaving', 'tardylimit', 'satrudayearlyleaving'])->get();
            
            foreach($preferences as $preference){
                if($preference->option=='latecomming'){
                    $latecoming=$preference->value;
                    $generallatecoming=$preference->value;
                }
                if($preference->option=='earlyleaving'){
                    $earlygoing=$preference->value;
                    $generalearlygoing=$preference->value;
                }
                if($preference->option=='tardylimit'){
                    $tardylimit=$preference->value;
                }
                if($preference->option=='satrudayearlyleaving'){
                    $satrudayearlyleaving=$preference->value;
                }
            }
            /*
            echo "Settings<br>";
            echo "Late Coming=". $latecoming;
            echo "<br>Early Going=". $earlygoing;
            echo "<br>Trady Limit=". $tardylimit;
            echo "<hr>";*/
            //Get Preferences ends
            //Get User from begins
            $users=\App\User::where('iscustomer',0)
                //->where('status',1)
                ->whereHas('staffdetails', function ($query) {
                    $query->where('showinsalary', '=', 1);
                })
                ->get();
            //Get User from Ends
            //Foreach Loop Begins
            foreach($users as $user){
                //Get Att Log begins
                //$userinfo=\App\User::where('id',$user->user_id)->first();
                $userinfo=$user;
                //echo "<hr>";
                echo "<b>".$userinfo->id.' '.$userinfo->fname.' '.$userinfo->lname." <br> Shift:".$userinfo->staffdetails->shift ."</b><br>";
                //Get Att log ends
                //Check if user Attendance Check is true begins
                //echo "End time: ";
                //echo $userinfo->staffdetails->endtime;

                //Creating Monthly Date range begins
                
                //$fromdate='2020-02-20';
                $fromdate=$request->get('fromdate');
                $currentmonth=date("m", strtotime($fromdate));
                $currentyear=date("Y", strtotime($fromdate));
                $firstday = new DateTime(date('Y-m-d', strtotime($fromdate)));                
                $firstday->format('Y-m-d');
				$firstday->sub(new DateInterval('P1D'));
				
                //$lastday = new DateTime('last day of this month');
                $yesterday=date('Y-m-d',strtotime("-1 days")); //Till Yesterday
                $todate=$request->get('todate');
                //$todate=date('2020-02-20'); 
                $lastday = new DateTime(date('Y-m-d', strtotime($todate)));
                $lastday->format('Y-m-d');
                $begin = $firstday;
                $end = $lastday;
                $end = $end->modify( '+1 day' ); 
                
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod($begin, $interval ,$end);
                //dd($daterange);
                //Creating Monthly Date range ends
                //echo $userinfo->staffdetails->attendancecheck;exit;
                if($userinfo->staffdetails->attendancecheck==1){
                        //Check User Execption begins
                        if($userinfo->staffdetails->latecomming!=null && $userinfo->staffdetails->latecomming > 0){
                            $latecoming=$userinfo->staffdetails->latecomming;
                        }else{
                            $latecoming=$generallatecoming;
                        }

                        if($userinfo->staffdetails->earlygoing!=null && $userinfo->staffdetails->earlygoing > 0){
                            $earlygoing=$userinfo->staffdetails->earlygoing;
                        }else{
                            $earlygoing=$generalearlygoing;
                        }
                        
                        //Check User Execption ends 
                        
                        $dateArray=array();
                        $i=0;
                        
                        foreach($daterange as $date){
                            //echo $date->format("Y-m-d") . "<br>";
                             $datefrom=$date->format("Y-m-d").' '.$userinfo->staffdetails->starttime;
                             $dt_from = date('Y-m-d H:i:s', strtotime($datefrom . "-2 hour"));                            
							 $dateArray[$i]['from']=$dt_from;
							 $dateArray[$i]['dated']=$date->format("Y-m-d");
                            
                             $dt_to=$date->format("Y-m-d").' '.$userinfo->staffdetails->endtime;
                             $hr=substr($userinfo->staffdetails->endtime,0,2);
                             if($hr > '16' && $hr < '23'){
                                $dt_to = date('Y-m-d H:i:s', strtotime($dt_to . "+5hour"));
                             }else{
                                $dt_to = date('Y-m-d H:i:s', strtotime($dt_to . "+1 days +5hour"));
                             }
                             //$dt_to = date('Y-m-d H:i:s', strtotime($dt_to . "+1 days +4hour"));
                             $dateArray[$i]['to']=$dt_to;
                             $i++;
                        }
                        /*echo "<pre>";
                        print_r($dateArray);
                        echo "</pre>";*/
                        
                        foreach($dateArray as $row){
							//print_r($row);
							echo "<br><b>". $row['dated']."</b>";
							 $checkin = 0;
							 $checkout= 0;
							//$attlogdata=\App\Attendance::where('status', 0)->where('user_id',$user->id)->whereBetween('attendancedate', [$row['from'], $row['to']])->orderby('attendancedate','ASC')->get();
							//$attlogdata=\App\Attendancesheet::where('status', 'P')->where('user_id',$user->id)->where('dated', $row['dated'])->first();
							$attlogdata=\App\Attendancesheet::where('user_id',$user->id)->where('dated', $row['dated'])->first();
                            if(!empty($attlogdata)){
									//print_r($attlogdata->toArray());
									echo "<br>Data found<br>";
									if($attlogdata->status==='X'){
										continue;
									}
					

                                        $firstlog=$attlogdata;
                                        $lastlog=$attlogdata;
                                        
                                        /*echo "First= ".$firstlog;
                                        echo "<br>";
                                        echo "Last= ".$lastlog;
										echo "<br>";*/
										//exit;
										$checkoutfound=$lastlog->checkoutfound;
										$checkinmakred=date_format($firstlog->dated,"Y-m-d")." ".$firstlog->checkin;
										if($userinfo->staffdetails->shift==='day'){
											$checkoutmakred=date_format($firstlog->dated,"Y-m-d")." ".$firstlog->checkout;
											$checkoutdate = date_format($lastlog->dated,"Y-m-d");                   
										}else{
											$checkoutmakred=date_format($firstlog->dated->addDays(1),"Y-m-d")." ".$firstlog->checkout;
											$checkoutdate = date_format($lastlog->dated->addDays(1),"Y-m-d");                   
										}
										
										$attendancedate = $checkinmakred;
                                        $attendancedatelast = $checkoutmakred;
                                        $checkindate = date_format($firstlog->dated,"Y-m-d");                   
                                        //$dated = date_format($firstlog->attendancedate,"Y-m-d");
                                        //$dated = date('Y-m-d H:i:s', strtotime($row['from']));
                                        $dated = date('Y-m-d', strtotime($row['from']));
                                        $checkindate=$checkindate.' '.$userinfo->staffdetails->starttime;
                                        
                                        $checkoutdate=$checkoutdate.' '.$userinfo->staffdetails->endtime;
                                        
                                        echo 'Clock Marked='.$attendancedate;
                                        echo "<br>";
                                        echo 'Setting Clock In='.$checkindate;
                                        echo "<br>";
                                        echo 'Setting Clock Out='.$checkoutdate;
										echo "<br>";
										echo "Check In: ";
										echo $checkin = $firstlog->checkin;
										echo "<br>Check Out:";
                                        echo $checkout= $lastlog->checkout;
                                        $status="P";
                                        $tardycount=0;
                                        $shortleavecount=0;
                                        $paid=1;
                                        $remarks="Present";
                                        //Get day begins
                                        $day = date('D', strtotime($row['from']));
                                        echo  "<br>". $day;
                                        //Get day ends

                                        //CheckIn status based on Time
                                        if(!empty($latecoming) or $latecoming!==null){
                                            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $checkindate);
                                            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedate);
                                            $diff_in_minutes = $to->diffInMinutes($from,false);       
                                            echo "<br><b>Check in time different:</b> ".$diff_in_minutes;
                                            echo "<br> Late Coming Margin: ".$latecoming;
                                            echo "<br> Tardy Limit Margin: ".$tardylimit;

                                            if($diff_in_minutes > 0 && $diff_in_minutes > $latecoming && $diff_in_minutes < $tardylimit){
                                                $status="P";
                                                $tardycount=1;
                                                $remarks.=" Late Arrival";
                                                $paid=2;
                                            }elseif($diff_in_minutes > 0 &&  $diff_in_minutes > $latecoming && $diff_in_minutes > $tardylimit){
                                                $status="P";
                                                $shortleavecount=1;
                                                $remarks.=" Short leave due to late coming";
                                                $paid=2;
                                            }
										}
										echo "<br> Late Comming: " .$tardycount;
										echo "<br> Short Leave: " .$shortleavecount;
                                        echo "<br> Check In Remarks: ".$remarks;
                                        //echo $status."<br>";
                                        //Check if its checkout time begins
                                        if(!empty($earlygoing) or $earlygoing!==null){
											echo "<br><b>Check Out condition</b>";
											echo "<br>User Checkout Date n Time: ". $attendancedatelast;
											echo "<br>Setting Checkout Date n Time: ". $checkoutdate;
                                            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedatelast);
                                            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $checkoutdate);
                                            $checkout_check_min = $to->diffInMinutes($from,false);
                                            echo "<br><b> Check out time different:</b> ".$checkout_check_min;
                                            echo "<br> Early going margin: ".$earlygoing;
                                            echo "<br> Tardy limit margin: ".$tardylimit;
                                            echo "<br>";
                                            if($checkout_check_min > 0 && $checkout_check_min > $earlygoing && $checkout_check_min < $tardylimit ){
                                                //Saturday Check begins
                                                if($day=='Sat' && $checkout_check_min <= $satrudayearlyleaving ){
                                                    $status="P";
                                                    $remarks.=" Saturday Exception";
                                                }else{
                                                    $status="P";
                                                    if($remarks=="Late Arrival"){
                                                        $remarks.=" Late Arrival and Early Left";
                                                    }else{
                                                        $remarks.=" Early Left";
                                                    }
                                                    $paid=2;
                                                    $tardycount=1;
                                                }
                                                //Saturday Check ends

                                            }elseif($checkout_check_min > 0  && $checkout_check_min > $earlygoing && $checkout_check_min > $tardylimit){
                                                //Saturday Check begins
                                                if($day=='Sat' && $checkout_check_min <= $satrudayearlyleaving ){
                                                    $status="P";
                                                    $remarks.=" Saturday Exception";
                                                }else{
                                                    $status="P";
                                                    $remarks.=" Short Leave due to early checkout";
                                                    $paid=2;
                                                    $shortleavecount=1;
                                                    
                                                }
                                                //Saturday Check ends
											}
                                            /*else{
                                                $status="Present";
                                                $remarks="Present";
                                                $paid=1;
                                                $shortleavecount=0;
                                            }*/

										}
										echo "<br> <b>Remarks:</b> ".$remarks;
										echo "<br> Late Comming: " .$tardycount;
										echo "<br> Short Leave: " .$shortleavecount;
										echo "<br>=================================================<br>";
                                        $workedhours=0;
                                        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedate);
                                        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attendancedatelast);
                                        $workedhours = $to->diffInHours($from,false);
                                        //echo $workedhours;
                                        
                                        //Check If today is holiday begins
                                            $holiday=\App\Holiday::where('dated', $dated)->first();
                                            if(isset($holiday) && $holiday->isworking==0){
                                                $status="H";
                                                $tardycount=0;
                                                $shortleavecount=0;
                                                $remarks=$holiday->description;
												$paid=1;
												$checkin="0";
												$checkout="0";
                                            }elseif(isset($holiday) && $holiday->isworking==1){
                                                $status="P";
                                                $tardycount=0;
                                                $shortleavecount=0;
                                                $remarks=$holiday->description;
												$paid=1;
												$checkin="0";
												$checkout="0";
                                            }
                                            unset($holiday);
                                        //Check If today is holiday ends

                                        //Sunday Check begins
                                        if($day=='Sun'){
                                            $status="P";
                                            $tardycount=0;
                                            $shortleavecount=0;
											$remarks='Sunday';
											$checkin="0";
											$checkout="0";
                                            $paid=1;
                                        }
                                        //Sunday Check ends
										

                                        //echo "<b>".$remarks."</b><br>";
                                        //echo $status."<br>";
                                        //Check if its checkout time ends
								
								//Create object and Insert or Update begins
								$attlogdata->remarks="Manual Attendance By User. ". $remarks;
								$attlogdata->paid=$paid;
								$attlogdata->checkin=$checkin;
								$attlogdata->checkout=$checkout;
								$attlogdata->checkoutfound=$checkoutfound;
								$attlogdata->tardies=$tardycount;
								$attlogdata->shortleaves=$shortleavecount;
								$attlogdata->workedhours=$workedhours;
								$attlogdata->status=$status;
								$date=date_create($request->get('date'));
								$format = date_format($date,"Y-m-d H:i:s");
								$attlogdata->updated_at = strtotime($format);
								$attlogdata->save();
                                
                            }else{
								//print_r($row);
								echo "<hr>Record Not Found<br>";
								$paid=1;
                                $day = date('D', strtotime($row['from']));
                                $dated=date('Y-m-d', strtotime($row['from']));
                                $joiningdate=\Carbon\Carbon::parse($user->staffdetails->joiningdate);
                                $endingdate=\Carbon\Carbon::parse($user->staffdetails->endingdate);
                                $second= \Carbon\Carbon::parse($row['from']);    
                                if($day=='Sun'){
                                        $remarks="Sunday"; 
                                        $status="P";                                    
                                }else{
                                    $holiday=\App\Holiday::where('dated', $dated)->first();
                                    $leave=\App\Leave::where('dated', $dated)->where('user_id',$user->id)->where('status','Approved')->first();
                                    if(isset($holiday)){
                                        if($holiday->isworking==0){
                                            $status="P";
                                            $remarks=$holiday->description;
                                            $paid=1;
                                        }elseif($holiday->isworking==1){
                                            $status="X";
                                            $remarks="Absent";
                                            $paid=0;
                                        }
                                        //Check If today is Sunday or Public Holiday begins
                                        //$remarks=$holiday->description;
                                        //$status="Holiday";
                                        //Check If today is Sunday or Public Holiday ends
                                    }elseif(isset($leave)){
                                        //Check If user is on leave begins
                                        //Check if Paid leave
                                        if($leave->ispaid==1){
                                            $remarks=$leave->leavetype .'-'. $leave->description ;
                                            $status=$leave->leavetype;
                                            $paid=1;
                                        }else{
                                            $remarks=$leave->leavetype .'-'. $leave->description ;
                                            $status='UL';
                                            $paid=0;
                                        }
                                        //Check If user is on leave ends
                                    }else{
                                        //Else user is on Absent begins
                                        $remarks="Absent";
                                        $status="X";
                                        $paid=0;
                                        //Else user is on Absent ends
                                    }                               
                                }
                                //Check if joining is not reached begins
                                if($joiningdate->greaterThan($second)){
                                    $remarks="Not Applicable/Joined";
                                    $status="-";
                                    $paid=0;
                                }
                                //Check if joining is not reached ends
                                //Check if ending date begins
                                if($user->staffdetails->endingdate != null && $endingdate->lessThanOrEqualTo($second)){
                                    $remarks="Not Applicable/Left";
                                    $status="-";
                                    $paid=0;
                                }
                                //Check if ending date ends
                                
                                //Create object and Insert or Update begins
                                
                                //Check if record already exists begins                                
                                $newatt = new \App\Attendancesheet;                 
                                $newatt->user_id=$userinfo->id;
								$newatt->dated=$dated;
								$newatt->dayname=$day;
								$newatt->remarks=$remarks;
								$newatt->paid=$paid;
								$newatt->attendancedate=$dated;
								$newatt->checkin=0;
								$newatt->checkout=0;
								$newatt->checkoutfound='No';
								$newatt->tardies=0;
								$newatt->shortleaves=0;
								$newatt->workedhours=0;
								$newatt->status=$status;
								$date=date_create($request->get('date'));
								$format = date_format($date,"Y-m-d H:i:s");
								$newatt->created_at = strtotime($format);
								$newatt->updated_at = strtotime($format);
								$newatt->save();
                                //Create object and Insert or Update ends
                            }
                            
                        }
                }
                //Check if user Attendance Check is true ends
        }//End foreach
	}
	//Fix Daily Att Ends




	//Manual Entries for Staff Members in Att Begins
	public function manualentries(Request $request)
    {
		echo "From Date: ".$request->get('fromdate');
		echo "<br>";
		echo "To Date: ". $request->get('todate');
		echo "<br>";
		echo "User Id: ". $request->get('uid');
		echo "<br>";
		$userid=$request->get('uid');
            //exit;      
            
            
                //Get Att Log begins
                $userinfo=\App\User::where('id',$userid)->first();
                echo "<b>".$userinfo->id.' '.$userinfo->fname.' '.$userinfo->lname." <br> Shift:".$userinfo->staffdetails->shift ."</b><br>";
                
                
                $fromdate=$request->get('fromdate');
                $currentmonth=date("m", strtotime($fromdate));
                $currentyear=date("Y", strtotime($fromdate));
                $firstday = new DateTime(date('Y-m-d', strtotime($fromdate)));
                $firstday->format('Y-m-d');
				$firstday->sub(new DateInterval('P1D'));
				
                $todate=$request->get('todate');
                $lastday = new DateTime(date('Y-m-d', strtotime($todate)));
                $lastday->format('Y-m-d');
                $begin = $firstday;
                $end = $lastday;
                $end = $end->modify( '+1 day' ); 
                
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod($begin, $interval ,$end);
                //dd($daterange);
                //Creating Monthly Date range ends
                //echo $userinfo->staffdetails->attendancecheck;exit;
                        $dateArray=array();
                        $i=0;
                        foreach($daterange as $date){
                            //echo $date->format("Y-m-d") . "<br>";
                             $datefrom=$date->format("Y-m-d").' '.$userinfo->staffdetails->starttime;
                             $dt_from = date('Y-m-d H:i:s', strtotime($datefrom));                            
							 $dateArray[$i]['from']=$dt_from;
							 $dateArray[$i]['dated']=$date->format("Y-m-d");
                            
                             $dt_to=$date->format("Y-m-d").' '.$userinfo->staffdetails->endtime;
							 $hr=substr($userinfo->staffdetails->endtime,0,2);
							 $dt_to = date('Y-m-d H:i:s', strtotime($dt_to));
                             $dateArray[$i]['to']=$dt_to;
                             $i++;
                        }
                        /*echo "<pre>";
                        print_r($dateArray);
                        echo "</pre>";*/
                        
                        foreach($dateArray as $row){
							//print_r($row);
							echo "<br><b>". $row['dated']."</b>";
							 $checkin = 0;
							 $checkout= 0;
							$attlogdata=\App\Attendancesheet::where('user_id',$userid)->where('dated', $row['dated'])->first();
                            if(!empty($attlogdata)){
									echo "<br>Data found<br>";
									continue;                                
                            }else{
								//print_r($row);
								echo "<hr>Record Not Found<br>";
								$paid=1;
								$remarks="Present";
                                $day = date('D', strtotime($row['from']));
                                $dated=date('Y-m-d', strtotime($row['from']));
                                $joiningdate=\Carbon\Carbon::parse($userinfo->staffdetails->joiningdate);
                                $endingdate=\Carbon\Carbon::parse($userinfo->staffdetails->endingdate);
                                $second= \Carbon\Carbon::parse($row['from']);    
                                if($day=='Sun'){
                                        $remarks="Sunday"; 
                                        $status="P";                                    
                                }else{
                                    $holiday=\App\Holiday::where('dated', $dated)->first();
                                    $leave=\App\Leave::where('dated', $dated)->where('user_id',$userid)->where('status','Approved')->first();
                                    if(isset($holiday)){
                                        if($holiday->isworking==0){
                                            $status="P";
                                            $remarks=$holiday->description;
                                            $paid=1;
                                        }elseif($holiday->isworking==1){
                                            $status="X";
                                            $remarks="Absent";
                                            $paid=0;
                                        }
                                        //Check If today is Sunday or Public Holiday begins
                                        //$remarks=$holiday->description;
                                        //$status="Holiday";
                                        //Check If today is Sunday or Public Holiday ends
                                    }elseif(isset($leave)){
                                        //Check If user is on leave begins
                                        //Check if Paid leave
                                        if($leave->ispaid==1){
                                            $remarks=$leave->leavetype .'-'. $leave->description ;
                                            $status=$leave->leavetype;
                                            $paid=1;
                                        }else{
                                            $remarks=$leave->leavetype .'-'. $leave->description ;
                                            $status='UL';
                                            $paid=0;
                                        }
                                        //Check If user is on leave ends
                                    }                          
                                }
                                //Check if joining is not reached begins
                                if($joiningdate->greaterThan($second)){
                                    $remarks="Not Applicable/Joined";
                                    $status="-";
                                    $paid=0;
                                }
                                //Check if joining is not reached ends
                                //Check if ending date begins
                                if($userinfo->staffdetails->endingdate != null && $endingdate->lessThanOrEqualTo($second)){
                                    $remarks="Not Applicable/Left";
                                    $status="-";
                                    $paid=0;
                                }
                                //Check if ending date ends
                                
                                //Create object and Insert or Update begins
                                
                                //Check if record already exists begins                                
                                $newatt = new \App\Attendancesheet;                 
                                $newatt->user_id=$userinfo->id;
								$newatt->dated=$dated;
								$newatt->dayname=$day;
								$newatt->remarks=$remarks;
								$newatt->paid=$paid;
								$newatt->attendancedate=$dated;
								$newatt->checkin=$row['from'];
								$newatt->checkout=$row['to'];
								$newatt->checkoutfound='Yes';
								$newatt->tardies=0;
								$newatt->shortleaves=0;
								$newatt->workedhours=0;
								$newatt->status=$status;
								$date=date_create($request->get('date'));
								$format = date_format($date,"Y-m-d H:i:s");
								$newatt->created_at = strtotime($format);
								$newatt->updated_at = strtotime($format);
								$newatt->save();
                                //Create object and Insert or Update ends
                            }
                            
                        }
               
	}
	//Manual Entries for Staff Members in Att Ends
	
	public function notification(Request $request)
	{
		//$data['notification'] = Notification::all
		$notifications = Auth::user()->Notifications()->paginate(10);
		// $data['readNotifications'] = Auth::user()->readNotifications()->paginate(10);
		// dd($data['notifications']);
		if ($request->ajax()) {
		$view = view('notifications.presult', compact('notifications'))->render();
		return response()->json(['html' => $view]);
		}
		return view('notifications.index')->with('notifications', $notifications);
	}

	public function markUnread(Request $request)
	{
		$id = $request->id;
		$notification = Notification::where('id', $id)->first();
		$notification->read_at = NULL;
		$notification->save();
		Session::Flash('success', 'Mark Unread Successfully');
		return response()->json('true');
	}
}

