<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Hash;


class ForgotPasswordController extends Controller
{
    public function postForgotPassword(Request $request)
	{
	
     $validator = Validator::make($request->all(), [
        'email' => 'required|email'
      ]);

    if( ! $validator->fails() )
    {


        if( $user = User::where('email', $request->input('email') )->first() )
        {
        	//dd($user);
            $password = str_random(10);
            $hashPass = Hash::make($password);
            //dd($hashPass);

            $users = User::findOrFail($user->id);
            $users->password = $hashPass;
            $users->save();


            $this->sendEmail($user,$password);
            $success = 'We have e-mailed your new password!';
            $response_data =[
                'success' => 1,
                'data' => $success
            ];
           return response()->json($response_data);
        }else{
        	
              
            $success = 'E-mail address is Invalid!';
            $response_data =[
                'success' => 0,
                'data' => $success
            ];
            return response()->json($response_data);

        }
    }

    }

	
	
	public function resetPassword($email, $code)
	{
	  // dd($email);
	   $user = User::byEmail($email);
	   
	   
	   $sentinelUser = Sentinel::findById($user->id);
	   if(count($user)==0)
	     abort(404);
		 
		if($reminder = Reminder::exists($sentinelUser)){
			
			if($code == $reminder->code)
	   
	        return $this->render('front.customer.resetPassword')->with('email',$email)->with('code',$code);
			
			else 
			return redirect()->route('customer.login.form');
			
			}else{
				
			return redirect()->route('customer.login.form');
				
			} 
	   
	
	}
	
	
	
	
	public function postResetPassword(Request $request)
	{
	   
	   $this->validate($request,[
	    
	   'password'=> 'confirmed|required|min:6',
	   'password_confirmation'=> 'required|min:6',
	   ]);
	   //dd($request->email);
	   $user = User::byEmail($request->email);
	   $sentinelUser = Sentinel::findById($user->id);
	   if(count($user)==0)
	     abort(404);
		 
		if($reminder = Reminder::exists($sentinelUser)){
			
			if($request->code == $reminder->code){
	        
			Reminder::complete($sentinelUser, $request->code, $request->password);
            return redirect()->route('customer.login.form')->with('forgot','Please login with your new password.');			
			}
			else 
			return redirect()->route('customer.login.form');
			
			}else{
				
			return redirect()->route('customer.login.form');
				
			} 
	   
	
	}
	
	
	
	
	
	private function sendEmail($user, $password)
	{
      
      
      //dd($resetcode);
	  Mail::send('emails.forgotPassword', [
	  
	  'user' => $user,
	  'password' => $password
	  ], function($message) use ($user){
		  
		  $message->to($user->email);
		  $message->subject("New Password for Your ERP Account");
		  
		  });

	}
	
	
	
	
}
