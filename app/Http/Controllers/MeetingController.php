<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Lead;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Http\Response;
use App\Notifications\LeadNotification;
use App\Notifications\AppointmentNotification;
use App\Notifications\RecordingNotification;
use App\Notifications\ProposalLeadNotification;
use Notification;
use App\User;
use App\Preference;
use App\Proposal;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Datatable;
use Yajra\Datatables\Datatables;
use \App\Meeting;
use App\Notifications\MeetingNotification;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return view('meetings.index');
    }
	
    public function fetch(){

        $permissions_arr=json_decode(auth()->user()->role->permissions,true);         
        if(isset($permissions_arr['all-meetings'])==true){
            $data = \App\Meeting::where('archive',0)->orderBy('id','ASC')->get();
        }else{
            $data = \App\Meeting::where('archive',0)->where('created_by',auth()->user()->id)->orderBy('id','ASC')->get();
        }
        
        return DataTables::of($data)
        ->addColumn('id',function($data){
            return $data->id;
        })		
        ->addColumn('title',function($data){
            return $data->title;
        })
        ->addColumn('description',function($data){
            return $data->description;
        })
        ->addColumn('meeting_datetime',function($data){
            return $data->meeting_datetime->format('d-M-Y h:i A');
        })
        ->addColumn('meeting_link',function($data){
            return $data->meeting_link;
        })
        ->addColumn('created_by',function($data){
            return $data->createdby->fname.' '.$data->createdby->lname;
        })
        ->addColumn('options',function($data){
            $action = '<span class="action_btn">';
            if(Auth::user()->can('meetings-show')){
				$action .= '<a href="'.url("/meetings/showteam/".$data->id).'" class="btn btn-primary" title="View Detail"><i class="fa fa-eye"></i> </a>'; 
            }
			//edit
            if(Auth::user()->can('meetings-edit')){
                $action .= '<a href="'.url("/meetings/".$data->id."/edit").'" class="btn btn-success" title="Edit"><i class="fa fa-edit"></i> </a>'; 
            }
			//delete
			if(Auth::user()->can('meetings-destroy')){
			$action .= '<button type="button" name="Delete" id="'.$data->id.'" 
												class="delete_meet btn btn-danger" title="Delete"><i class="fa fa-trash"></i></button>';
												$action .= '&nbsp;&nbsp;';
			}
		
            $action .= '</span>';
            return $action; 
                                
        })
        ->rawColumns(['options','id','title','description','meeting_datetime','meeting_link','created_by'])
        ->make(true);
    }	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles=\App\Role::all();
        $departments = \App\Department::where('status', 1)->orderBy('deptname', 'ASC')->get();
        $designations = \App\Designation::where('status', 1)->orderBy('name', 'ASC')->get();

		$employees = \App\User::with('department')->with('designation')->where('status',1)
		->where('iscustomer',0)
		->get();		
		
        return view('meetings/create',compact('roles','departments','designations','employees'));		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		//dd($request->all());
        $this->validate(request(), [
            'title' => 'required',
			'meeting_datetime' => 'required',
			'empID' => 'required',
            'recording_file' => ['mimes:mp4,mpeg']
        ]);

        $meeting_datetime=date_create($request->get('meeting_datetime'));
        $meetformat = date_format($meeting_datetime,"Y-m-d H:i:s");		
		
		$meetformat_strtotime = strtotime($meetformat);
		$curr_datetime_strtotime = strtotime(date("Y-m-d H:i:s"));
		
        if($request->hasfile('recording_file'))
         {
            $file = $request->file('recording_file');
            $recordingfile=time().$file->getClientOriginalName();
            //$file->move(public_path().'/leads_assets/recordings/', $recordingfile);
            Storage::disk('local')->put('/public/meetings_assets/videos/'.$recordingfile, File::get($file));
         }else{
            $recordingfile="";
         }
		//Recording Uploading
		$meeting= new \App\Meeting;
        $meeting->title=$request->get('title');
        $meeting->description=$request->get('description');	
        $meeting->meeting_datetime=$meetformat;	
        $meeting->meeting_link=$request->get('meeting_link');
        $meeting->recording_file=$recordingfile;
	
        $meeting->created_by=auth()->user()->id;
		$date=date_create($request->get('date'));
        $format = date_format($date,"Y-m-d");
        $meeting->created_at = strtotime($format);
        $meeting->updated_at = strtotime($format);
        $meeting->save();
		$last_meeting_id = $meeting->id;
		
		//getting meeting_id for pivot table
		$meeting = Meeting::find($last_meeting_id);
		
		$empID_ary = $request->get('empID');
		foreach($empID_ary as $empID){
			
			//Pivot table relation
			$meeting->users()->attach($empID);		
		}		
		
		
        $url=url('/meetings/showteam/'.$last_meeting_id);
        $creator=auth()->user()->fname.' '.auth()->user()->lname;
		if($meetformat_strtotime >= $curr_datetime_strtotime){
			//Nofication////////////////////////////////////////////////////////
			//imploding participants array
			//$imploded_par_ary = implode(",",$empID_ary);	
			//
			//$users=\App\User::with('role')->where('iscustomer',0)->where('status',1)->whereRaw("id IN ($imploded_par_ary)")->get();
			//$message = collect(['title' => 'New meeting has been uploaded','body'=>'A new meeting has been uploaded by '.$creator.' , please review it.','redirectURL'=>$url]);
			//Need to send only to participants
			//Notification::send($users, new MeetingNotification($message));
			////////////////////////////////////////////////////////////////////
        }
        
        //Nofication////////////////////////////////////////////////////////
        //imploding participants array
        $meetingnotifier = Preference::where('option','meetingnotifier')->first();
        $meetingnotifiers_ids = explode(',',$meetingnotifier->value);
        $users = User::where('iscustomer',0)->where('status',1)->whereIn('id',$meetingnotifiers_ids)->get();

        $message = collect(['title' => 'New meeting has been uploaded','body'=>'A new meeting has been uploaded by '.$creator.' , please review it.','redirectURL'=>$url]);
        //Need to send only to participants
        Notification::send($users, new MeetingNotification($message));
        ////////////////////////////////////////////////////////////////////
        return redirect('/meetings/create')->with('success', 'Meeting has been Added Successfully.');		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $permissions_arr=json_decode(auth()->user()->role->permissions,true);         
        if(isset($permissions_arr['all-meetings'])==true){
            $meeting = \App\Meeting::with('createdby')->where('id',$id)->first();
        }else{
            $meeting = \App\Meeting::with('createdby')->where('id',$id)->where('created_by',auth()->user()->id)->first();
        }
        $user = User::where('iscustomer',0)->where('status',1)->get();
		if(!empty($meeting)){
            return view('/meetings/edit',compact('meeting','user'));
        }else{
            return view('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		//dd($request->all());
        $this->validate(request(), [
            'title' => 'required',
			'meeting_datetime' => 'required',
			'emp_id' => 'required',
            'recording_file' => ['mimes:mp4,mpeg']
        ]);
		
        $meeting_datetime=date_create($request->get('meeting_datetime'));
        $meetformat = date_format($meeting_datetime,"Y-m-d H:i:s");
        if($request->hasfile('recording_file'))
         {
            $file = $request->file('recording_file');
            $recordingfile=time().$file->getClientOriginalName();
            //$file->move(public_path().'/leads_assets/recordings/', $recordingfile);
            Storage::disk('local')->put('/public/meetings_assets/videos/'.$recordingfile, File::get($file));
         }else{
            $recordingfile=$request->get('hidden_recording_file');
         }	
	
		
		$emp_id =  $request->get('emp_id');
        
        $permissions_arr=json_decode(auth()->user()->role->permissions,true);         
        if(isset($permissions_arr['all-meetings'])==true){
            $meeting=  \App\Meeting::find($id);
        }else{
            $meeting=  \App\Meeting::where('id',$id)->where('created_by',auth()->user()->id)->first();
        }
		if(empty($meeting)){
           return view('404');
        }
        
        $meeting->title=$request->get('title');
        $meeting->description=$request->get('description');	
        $meeting->meeting_datetime=$meetformat;	
        $meeting->meeting_link=$request->get('meeting_link');
        $meeting->recording_file=$recordingfile;

        //$date=date_create($request->get('date'));
        //$format = date_format($date,"Y-m-d");
        $meeting->updated_at = date('Y-m-d H:i:s');
		$meeting->save();

        if($meeting){
        //$project->users()->detach($emp_id);
        DB::table('meeting_user')->where('meeting_id', $meeting->id)->delete();
        $meeting->users()->attach($emp_id);
        
        }
		$meetingid = $meeting->id;
        return redirect('meetings/'.$meetingid.'/edit')->with('success', 'Meeting has been updated successfully.');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $meet_del = \App\Meeting::findOrFail($id);
        //$data->delete();
			$form_data = array(
				'archive'			=> 1,
				'updated_at'		=> date('Y-m-d H:i:s'),
				
			);
			\App\Meeting::whereId($id)->update($form_data);
    }
	

    public function showteam($id)
    {
        //        
        $permissions_arr=json_decode(auth()->user()->role->permissions,true);         
        if(isset($permissions_arr['all-meetings'])==true){
            $par_val = Meeting::with('participant_name')->find($id);
        }else{
            $par_val = Meeting::with('participant_name')->where('id',$id)->where('created_by',auth()->user()->id)->first();
        }
		if(empty($par_val)){
           return view('404');
        }

		$participants = Meeting::with('participant_name')->find($id);
		$showparticipants = $participants->users()->get();
		
		return view('meetings.showteam',compact('showparticipants','id','par_val'));
    }	
	
	public function getVideo() {
		$video = Storage::disk('local')->get("uploadpath_here");
		$response = Response::make($video, 200);
		$response->header('Content-Type', 'video/mp4');
		return $response;
	}	
	
}
