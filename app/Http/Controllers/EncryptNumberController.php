<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use \App\EmployeeNumber;
use \App\Staffdetail;

class EncryptNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
/*             $users=User::WhereHas('staffdetails', function ($query) {
                    $query->where('showinsalary', '=', 1);
                })
                ->get(); */
				//where('iscustomer',0)
                //->where('status',1)
		$users=User::with('staffdetails')->get(); 		
		//dd($users);
		foreach($users as $user){
			//Get Att Log begins
			//$userinfo=\App\User::where('id',$user->user_id)->first();
			$userinfo=$user;
			//echo "<hr>";
			echo $userinfo->id.' '.$userinfo->fname.' '.$userinfo->lname."<br>";
			
			$phonenumber = encrypt($userinfo->phonenumber);
			$form_data_user = array(
				'phonenumber'		=> $phonenumber,
				//'updated_at'		=> date('Y-m-d H:i:s'),
				
			);			
			\App\User::whereId($userinfo->id)->update($form_data_user);
			
			
			//$staffdetail=\App\Staffdetail::where('user_id', $userinfo->id)->first();
			//$staffdetail = DB::table('staffdetails')->pluck('landline','gaurdiancontact')->where('user_id', $userinfo->id);
			//$staffdetail = DB::table('staffdetails')->where('user_id', $userinfo->id)->first();
			$staffdetail_landline  = encrypt($userinfo->staffdetails->landline);
			$staffdetail_gaurdiancontact  = encrypt($userinfo->staffdetails->gaurdiancontact);
			
			$form_data_staffdetail = array(
				'phonenumber'		=> $phonenumber,
				'landline'		=> $staffdetail_landline,
				'gaurdiancontact'		=> $staffdetail_gaurdiancontact,
				
				
			);			
			\App\Staffdetail::where('user_id',$userinfo->id)->update($form_data_staffdetail);
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
