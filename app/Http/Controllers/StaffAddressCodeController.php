<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use \App\StaffAddressCode;



class StaffAddressCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		//dd($request->all());
		if($request->get('type')=="Present"){
			$rules = array(
				'user_id' => 'required',
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}	
			
				$present_address_code = rand(100000,999999);
				$form_data = array(
					'user_id'				=> $request->get('user_id'),
					'type'					=> $request->get('type'),
					'code'					=> $present_address_code,
					'created_at'			=> date('Y-m-d H:i:s'),
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);			
				\App\StaffAddressCode::create($form_data);
				
				//presentaddresstemplate
				$preferences_presentaddresstemplate= \App\Preference::whereIn('option',['presentaddresstemplate'])->first();
				$presentaddresstemplate = $preferences_presentaddresstemplate->value;				
				//dd($presentaddresstemplate);
				
				$empname = User::find($request->get('user_id'));
				
				$presentaddresstemplate = str_replace("[EMPLOYEE]",$empname->fname." ".$empname->lname,$presentaddresstemplate);
				$presentaddresstemplate = str_replace("[PRESENT_CODE]",$present_address_code,$presentaddresstemplate);
				
				//dd($presentaddresstemplate);
				$returnHTML = view('staffaddresscode.print_address_template',['form_data'=> $presentaddresstemplate])->render();
				//return response()->json(['success' => 'Present Address Code Added.']);
				return response()->json(array('success' => 'Present Address Code Generated.', 'html'=>$returnHTML));				
		}	

		if($request->get('type')=="Permanent"){
			$rules = array(
				'user_id' => 'required',
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}	
			
				$permanent_address_code = rand(100000,999999);
				$form_data = array(
					'user_id'				=> $request->get('user_id'),
					'type'					=> $request->get('type'),
					'code'					=> $permanent_address_code,
					'created_at'			=> date('Y-m-d H:i:s'),
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);			
				\App\StaffAddressCode::create($form_data);
				
				//permanentaddresstemplate
				$preferences_permanentaddresstemplate= \App\Preference::whereIn('option',['permanentaddresstemplate'])->first();
				$permanentaddresstemplate = $preferences_permanentaddresstemplate->value;				
				//dd($permanentaddresstemplate);
				
				$empname = User::find($request->get('user_id'));
				
				$permanentaddresstemplate = str_replace("[EMPLOYEE]",$empname->fname." ".$empname->lname,$permanentaddresstemplate);
				$permanentaddresstemplate = str_replace("[PERMANENT_CODE]",$permanent_address_code,$permanentaddresstemplate);
				
				//dd($permanentaddresstemplate);
				$returnHTML = view('staffaddresscode.print_address_template',['form_data'=> $permanentaddresstemplate])->render();
				//return response()->json(['success' => 'Permanent Address Code Added.']);
				return response()->json(array('success' => 'Permanent Address Code Generated.', 'html'=>$returnHTML));				
		}			
	
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
		//dd($request->all());
		if($request->get('type')=="Present"){
			$rules = array(
				'present_address_code' => 'required',
				'hidden_id_present' => 'required',
				
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}
			
			$check_address_codes = StaffAddressCode::whereId($request->hidden_id_present)
			->where('code',$request->get('present_address_code'))
			->first();		
			//check if address code matches
			if ($check_address_codes !== null) {			
				$form_data = array(
					'verified'		=> 1,
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);
				\App\StaffAddressCode::whereId($request->hidden_id_present)->update($form_data);

				return response()->json(['success' => 'Present Address Verified']);
			}
			else{
				return response()->json(['errors' => 'Error Verifying Present Address.']);
			}	
		}	

		if($request->get('type')=="Permanent"){
			$rules = array(
				//'user_id' => 'required',
				'permanent_address_code' => 'required',
				'hidden_id_permanent' => 'required',
				
			);	
			$error = Validator::make($request->all(), $rules);

			if($error->fails())
			{
				return response()->json(['errors' => $error->errors()->all()]);
			}
			
			$check_address_codes = StaffAddressCode::whereId($request->hidden_id_permanent)
			->where('code',$request->get('permanent_address_code'))
			->first();		
			//check if address code matches
			if ($check_address_codes !== null) {			
				$form_data = array(
					'verified'		=> 1,
					'updated_at'			=> date('Y-m-d H:i:s'),
					
				);
				\App\StaffAddressCode::whereId($request->hidden_id_permanent)->update($form_data);

				return response()->json(['success' => 'Permanent Address Verified']);
			}
			else{
				return response()->json(['errors' => 'Error Verifying Permanent Address.']);
			}	
		}			
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
