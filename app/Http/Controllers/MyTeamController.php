<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use App\Department;
use DB;

class MyTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$user_id = auth()->user()->id;
		$teams = \App\Team::where('teamlead_id',$user_id)->with('teamlead_name')->with('emp_name')->with('dept_name')->first();	
		return view('myteams.index',compact('teams'));
    }
	
    public function myteam()
    {
        //
		$user_id = auth()->user()->id;
		$teams = \App\Team::where('teamlead_id',$user_id)->with('teamlead_name')->with('emp_name')->with('dept_name')->first();
		//dd($teams->id);
		$id = $teams->id;
		$team_val = Team::with('teamlead_name')->find($id);
		$team = Team::with('teamlead_name')->find($id);
		//dd($team_val."---".$team);
		$showteams = $team->users()->get();	
		return view('myteams.myteam',compact('showteams','id','team_val'));
    }	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
