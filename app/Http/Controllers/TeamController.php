<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use App\Department;
use DB;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$user_id = auth()->user()->id;
/* 		if(Auth()->user()->role_id==1){ */
			$teams = \App\Team::with('teamlead_name')->with('emp_name')->with('dept_name')->get();
/* 		}
		else{
			$teams = \App\Team::where('teamlead_id',$user_id)->with('teamlead_name')->with('emp_name')->with('dept_name')->get();
		} */
		return view('teams.list',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$depts = \App\Department::all();
		return view('teams.create',compact('depts'));
		
    }
	
	public function selectAjax(Request $request)
    {
    	if($request->ajax()){
			$team_leads = \App\User::with('department')->with('designation')->where('iscustomer',0)->where('status',1)->get();
			$data = view('teams.ajax-select',compact('team_leads'))->render();
    		return response()->json(['options'=>$data]);
    	}
    }

	public function selectAjaxEmp(Request $request)
    {
    	if($request->ajax()){
			$teamlead_id = $request->teamlead_id;
		//Getting user_id from pivot table, not to be shown under available employees/members
		$pivotteam = DB::table('team_user')->pluck('user_id');
		$emp_available = \App\User::with('department')->with('designation')->where('status',1)->whereNotIn('id',$pivotteam)
			->where('iscustomer',0)
			->get();			
			$data = view('teams.ajax-select-emp',compact('emp_available'))->render();
    		return response()->json(['options'=>$data]);
    	}
    }	

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$this->validate(request(), [
            'team_name' => 'required',
			'department_id' => 'required|not_in:0',
			'teamlead_id' => 'required|not_in:0',
			'empID' => 'required',
        ]);
			//dd($request->all());
		
		$teams= new \App\Team;
		$teams->team_name=$request->get('team_name');
		$teams->teamlead_id=$request->get('teamlead_id');
		$teams->department_id=$request->get('department_id');
		$teams->created_by=auth()->user()->id;
		$date=date_create($request->get('date'));
		$format = date_format($date,"Y-m-d");
		$teams->created_at = strtotime($format);
		$teams->updated_at = strtotime($format);
		$teams->save();
		$last_team_id = $teams->id;
		
		//getting team_id for pivot table
		$team = Team::find($last_team_id);
		
		$empID_ary = $request->get('empID');
		foreach($empID_ary as $empID){
			
			//Pivot table relation
			$team->users()->attach($empID);		
		}
		return redirect('teams/')->with('success', 'Team made successfully.');		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //	
		$depts = \App\Department::all();			
		$team = \App\Team::with('teamlead_name')->find($id);
		$showteams = $team->users()->get();
		//echo $team->department_id;exit;
		$team_leads = \App\User::with('department')->with('designation')->where('iscustomer',0)->where('status',1)->get();
		return view('teams.edit',compact('showteams','id','depts','team','team_leads'));		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$this->validate(request(), [
            'team_name' => 'required',
			'teamlead_id' => 'required|not_in:0',
        ]);
			//dd($request->all());
		
		$teams= \App\Team::find($id);
		$teams->team_name=$request->get('team_name');
		$teams->teamlead_id=$request->get('teamlead_id');
		//$teams->department_id=$request->get('department_id');
		
		$date=date_create($request->get('date'));
		$format = date_format($date,"Y-m-d");
		$teams->updated_at = strtotime($format);
		$teams->save();
		return redirect('teams/')->with('success', 'TeamLead updated successfully.');		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$get_team_id = DB::table('team_user')->where('user_id',$id)->first();
 		$pivotid = $get_team_id->id;
		$team_id = $get_team_id->team_id;
		//using pivot detach
		$team = Team::find($team_id);
		$team->users()->detach($id);
		return redirect('teams/showteam/'.$team_id)->with('success', 'Team member deleted successfully.');	
    }
	
    public function showteam($id)
    {
        //
		$team_val = Team::with('teamlead_name')->find($id);
		$team = Team::with('teamlead_name')->find($id);
		$showteams = $team->users()->get();
		//dd($showteams);
		return view('teams.showteam',compact('showteams','id','team_val'));
    }
	
    public function addmember($id)
    {
        //
		//To show teamname and teamlead in view
		$team_val = Team::with('teamlead_name')->find($id);
		
		//Getting user_id from pivot table, not to be shown under available employees/members
		$pivotteam = DB::table('team_user')->pluck('user_id');
		$emp_available = \App\User::with('department')->with('designation')->where('status',1)->whereNotIn('id',$pivotteam)
		->where('iscustomer',0)
		->get();
		return view('teams.addmember',compact('emp_available','id','team_val'));
		
    }
	
    public function savemember(Request $request)
    {
        //
		$this->validate(request(), [
            'team_name' => 'required',
			'teamlead_id' => 'required|not_in:0',
			'team_id' => 'required|not_in:0',
			'empID' => 'required',
        ]);
			//dd($request->all());

		$team_id = $request->get('team_id');
		
		//getting team_id for pivot table
		$team = Team::find($team_id);
		
		$empID_ary = $request->get('empID');
		foreach($empID_ary as $empID){
			
			//Pivot table relation
			$team->users()->attach($empID);
		}
		return redirect('teams/showteam/'.$team_id)->with('success', 'Team members added successfully.');		
		
    }

    public function showallteam()
    {
        //
		$team_val = Team::with('teamlead_name')->get();
		$showteams = \App\TeamUser::with('username')->with('teamlead_id')->get();
		//$showteams = $team->users()->get();
		return view('teams.showallteam',compact('showteams','id','team_val'));
    }	
	
	
}
