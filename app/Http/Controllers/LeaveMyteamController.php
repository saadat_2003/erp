<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Leave;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use \App\User;
use \App\Preference;
use \App\Holiday;
use \App\Department;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Team;
use App\Notifications\LeaveNotification;
use Notification;

use \App\LeaveApproval;


class LeaveMyteamController extends Controller
{
    



	
    public function myteam_leaves()
    {
        //
        if(request()->ajax())
        {
/* 			$data = \App\Leave::with('leaveapproval')->with('teams')->with('users')
			->with('applicant')->with('createdby')->with('modifiedby')
			->where('user_id','=',auth()->user()->id)->get(); */
/* 			$data = \App\Leave::with('applicant')->with('createdby')->with('modifiedby')
				->with('leaveapproval')
				->get(); */
			//dd($data->leaveapproval[0]->approvedbyteam->memberofteam->toArray());
				$data =  
				 \App\Leave:: select(
				'leaves.id as leaves_id',
				'leaves.dated as dated',
				'leaves.description as description',
				'leaves.leavetype as leavetype',
				'leaves.status as status',
				'leaves.ispaid as ispaid',
				'leaves.user_id as user_id',
				'leaves.created_at as leaves_created_at',
				'leaves.updated_at as leaves_updated_at',
				'leaves.created_by as created_by',
				'leaves.modified_by as modified_by',
				
				'team_user.id as team_user_id',
				'team_user.user_id as team_user_u_id',
				'team_user.team_id as team_user_t_id',
				
				'teams.created_at as teams_created_at',
				'teams.updated_at as teams_updated_at',
				'teams.created_by as teams_created_by',
				'teams.teamlead_id as teamlead_id',
				'teams.team_name as team_name') 			
				->with('applicant')->with('createdby')->with('modifiedby')
				->with('leaveapprovalcheck')
				->join('team_user','team_user.user_id','=','leaves.user_id')
				->join('teams','team_user.team_id', '=', 'teams.id')
				
				//->leftjoin('leave_approvals','leave_approvals.leave_id', '=', 'leaves.id')
				
				->where('teams.teamlead_id','=',auth()->user()->id)	
				->orderBy('leaves.id', 'DESC')->orderBy('leaves.status', 'ASC')
				->get();			
			
            return datatables()->of($data)
					
					->addColumn('id',function($data){
						return $data->id;
					})
					->addColumn('applicant',function($data){
						return $data->applicant->fname.' '.$data->applicant->lname;
					})
					->addColumn('ispaid', function($data){
						$ispaid = "";
						if($data->ispaid=='1') {
						  $ispaid .= 'Yes';
						}else{
						  $ispaid .= 'No';
						}                  
						return $ispaid;
					})

					->addColumn('createdby',function($data){
						return $data->createdby->fname.' '.$data->createdby->lname;
					})
					
					->addColumn('created_at',function($data){
						return date("d-m-Y H:i:s",strtotime($data->created_at));
					})
					
					->addColumn('status', function($data){
						$sta = "";
							if($data->status=='Approved') {
							  $sta .= '<span class="btn btn-success btn-sm">Approved</span>';
							}else if($data->status=='Rejected'){
							  $sta .= '<span class="btn btn-danger btn-sm">Rejected</span>';
							}else if($data->status=='Pending'){
							  $sta .= '<span class="btn btn-warning btn-sm">Pending</span>';
							}else if($data->status=='Bypass'){
							  $sta .= '<span class="btn btn-success btn-sm">Bypass</span>';
							}
						$sta .= '&nbsp;&nbsp;';                    
						return $sta;
					})			
                    ->addColumn('action', function($data){
						//myteam_leaves show
						if(Auth::user()->can('myteam_leaves_show')){
								//showViewDetails
								$button = '<button type="button" name="viewdetails" data-id="'.$data->leaves_id.'" emp_id="'.$data->user_id.'" class="viewdetails btn btn-primary"><i class="fa fa-eye"></i></button>';
								$button .= '&nbsp;&nbsp;';
							}
						if(Auth::user()->can('myleaves-delete')){
							$leave_approval = \App\LeaveApproval::where('leave_id',$data->leaves_id)->where('leave_of_user',$data->user_id)->count();
							//dd($leave_approval);
							if($leave_approval>=1){
								$button .= '';
							}else{							
								$button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger"><i class="fa fa-trash"></i></button>';
								$button .= '&nbsp;&nbsp;';  
							}							
						}	
						if(Auth::user()->can('myTeamLeavesApprovedRejected')){
							$leave_approval = \App\LeaveApproval::where('leave_id',$data->leaves_id)->where('leave_of_user',$data->user_id)->count();
							//dd($leave_approval);
							if($leave_approval>=1){
								$button .= '';
							}else{
								$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->leaves_id.'" 
										leave_of_user="'.$data->user_id.'" 
										class="add_status btn btn-primary" title="Update leave status"><i class="fa fa-check"></i></button>';
										$button .= '&nbsp;&nbsp;';
							}
						}
						return $button;
                    })
                    ->rawColumns(['id','status','action','leaveapproval'])
                    ->make(true);
        }
        return view('leaves.myteam_leaves');	
    }	


	/**
	 * myteam_leaves APPROVED/REJECTED
	 * @param  $request
	 * @return mixed
	 */
	public function myTeamLeavesApprovedRejected(Request $request){

		$rules = array(
			
			'comments' => 'required',
			'statustype' => 'required|not_in:0',
			'leaves_id_form' => 'required',
			'leave_of_user' => 'required',
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		//MONTH START DATE
		$current_date = '01';
		$current_month = date('m');
		$current_year = date('Y');
		$MONTH_START_DATE = $current_year."-".$current_month."-".$current_date;
		//////////////////
		//MONTH END DATE
		$MONTH_END_DATE  = date('Y-m-t');			
		$check_approval = \App\LeaveApproval::where('leave_id',$request->get('leaves_id_form'))->whereBetween('created_at',[$MONTH_START_DATE,$MONTH_END_DATE])->where('leave_of_user',$request->get('leave_of_user'))->where('approved_by',auth()->user()->id)->first();	
		//check if already leave approval has NOT been updated
		if ($check_approval === null ) {
			$form_data = array(
				'leave_id'         => $request->get('leaves_id_form'),
				'approved_by'      => auth()->user()->id,		
				'leave_of_user'      => $request->get('leave_of_user'),
				'comments'        => $request->get('comments'),
				'status'            => $request->get('statustype'),

				//'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				//'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				
			);
			\App\LeaveApproval::create($form_data);
			return response()->json(['success' => 'Leave status updated.']);		
 		}else{
			return response()->json(['errors' => 'Leave status Already updated']);
		}		
		
	}	
	
	
    public function myteam_leaves_show(Request $request)
    {
        //
        //$this->authorize('show-parents');
        /* if(!empty($request->showstatus)){
            $showstatus=$request->showstatus;
        }else{
            $showstatus=0;
        } */
        $leave_approval_details = \App\LeaveApproval::with('approvedby')->with('leaveofuser')->where('leave_id',$request->id)->get();
		//dd($parents);
		$emp_details = \App\User::where('status',1)->where('id',$request->emp_id)
							->where('iscustomer',0)
							->first();//dd($student_details);
        //$hrleadstatus = Hrleadstatus::where('hrlead_id',$request->id)->get();
		//$addressbooks = \App\Addressbook::with('createdby')->where('user_id',$request->id)->where('type',1)->get();
		//$addressbooksphone = \App\Addressbook::with('createdby')->where('user_id',$request->id)->where('type',2)->get();
		
        if($request->ajax()) {
            return  view('leaves.myteam_showajax')->with(compact('leave_approval_details','emp_details'));
            
        }		
    }	
	
}
