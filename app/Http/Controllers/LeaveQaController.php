<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Leave;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use \App\User;
use \App\Preference;
use \App\Holiday;
use \App\Department;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Team;
use App\Notifications\LeaveNotification;
use Notification;

use \App\LeaveApproval;
use \App\Attendancesheet;

use DataTables;

class LeaveQaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$departments = Department::where('status', 1)->orderBy('deptname', 'ASC')->get();
        return view('leaves_qa.index',compact('users','departments'));
    }

    public function fetch(Request $request){
		//dates
		$currentMonth = date('m');
		$currentYear = date('Y');
        $firstday=date('Y-m-01');
        $lastday=date('Y-m-t'); 		
        $firstday_strtotime=strtotime($firstday);
        $lastday_strtotime=strtotime($lastday);
		//filtered dates
		$dateFrom = $request->get('dateFrom');
		$dateTo = $request->get('dateTo');	
		$dateFrom_strtotime = strtotime($request->get('dateFrom'));
		$dateTo_strtotime = strtotime($request->get('dateTo'));	
		
		$deparment_id = $request->get('srcdepartment_id');
		
/* 		if( $request->get('dateFrom') && $request->get('dateTo') ){		
			//dd($from."---".$to);
		}	 */	
/* 		if($request->get('searchsubmit')){
			echo 'HEREF';
		} */
		if($request->get('srcdepartment_id')){
			//dd($deparment_id);
			//dd($request->get('deparment_id'));
			$data = \App\Leave::with('applicant')->with('createdby')->with('modifiedby')->with('leaveapprovalcheck')
			->whereHas('applicant', function ($query) use($deparment_id)  {
				$query->where('department_id',$deparment_id);
			})
			->whereBetween('dated',[$dateFrom, $dateTo])
			->orderBy('id', 'DESC')->orderBy('status', 'ASC')
			->get();
			//dd($data);
		}else{
			if($dateFrom!=NULL || $dateTo!=NULL){
				$data = \App\Leave::with('applicant')->with('createdby')->with('modifiedby')->with('leaveapprovalcheck')
				->whereBetween('dated',[$dateFrom, $dateTo])					
				->orderBy('id', 'DESC')->orderBy('status', 'ASC')
				->get();	
				//dd($data."F1");
			}else{
				$data = \App\Leave::with('applicant')->with('createdby')->with('modifiedby')->with('leaveapprovalcheck')
				->whereBetween('dated',[$firstday, $lastday])		
				->orderBy('id', 'DESC')->orderBy('status', 'ASC')
				->get();	
				//dd($data."F2");
			}
		}
				//dd($data);
				return datatables()->of($data)
					
					->addColumn('leaves_id',function($data){
						return $data->id;
					})
					->addColumn('applicant',function($data){
						return $data->applicant->fname.' '.$data->applicant->lname."[".$data->qa_leave_dept->department->deptname."]";
					})
					->addColumn('ispaid', function($data){
						$ispaid = "";
						if($data->ispaid=='1') {
						  $ispaid .= 'Yes';
						}else{
						  $ispaid .= 'No';
						}                  
						return $ispaid;
					})

					->addColumn('createdby',function($data){
						return $data->createdby->fname.' '.$data->createdby->lname;
					})

					
						->addColumn('status', function($data){
							$sta = "";
							if($data->status=='Approved') {
							  $sta .= '<span class="btn btn-success btn-sm">Approved</span>';
							}else if($data->status=='Rejected'){
							  $sta .= '<span class="btn btn-danger btn-sm">Rejected</span>';
							}else if($data->status=='Pending'){
							  $sta .= '<span class="btn btn-warning btn-sm">Pending</span>';
							}else if($data->status=='Bypass'){
							  $sta .= '<span class="btn btn-success btn-sm">Bypass</span>';
							}
							$sta .= '&nbsp;&nbsp;';                    
							return $sta;
						})	
						
						->addColumn('action', function($data){
							$button = "NA";
							return $button;
						})
                    ->rawColumns(['id','status','action'])
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
