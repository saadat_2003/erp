<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Leave;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use \App\User;
use \App\Preference;
use \App\Holiday;
use \App\Department;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Team;
use App\Notifications\LeaveNotification;
use Notification;

use \App\LeaveApproval;
use \App\Attendancesheet;


class LeaveHrController extends Controller
{
    
    public function hr_leaves()
    {
        //
        if(request()->ajax())
        {

				$data = \App\Leave::with('applicant')->with('createdby')->with('modifiedby')->with('leaveapprovalcheck')
				->orderBy('id', 'DESC')->orderBy('status', 'ASC')
				->get();	
			
            return datatables()->of($data)
					
					->addColumn('leaves_id',function($data){
						return $data->id;
					})
					->addColumn('applicant',function($data){
						return $data->applicant->fname.' '.$data->applicant->lname;
					})
					->addColumn('ispaid', function($data){
						$ispaid = "";
						if($data->ispaid=='1') {
						  $ispaid .= 'Yes';
						}else{
						  $ispaid .= 'No';
						}                  
						return $ispaid;
					})

					->addColumn('createdby',function($data){
						return $data->createdby->fname.' '.$data->createdby->lname;
					})

					->addColumn('modified_by',function($data){
						return $data->modifiedby->fname.' '.$data->modifiedby->lname;
					})
					
/* 					->addColumn('created_at',function($data){
						return $data->created_at;
					}) */
					
						->addColumn('status', function($data){
							$sta = "";
							if($data->status=='Approved') {
							  $sta .= '<span class="btn btn-success btn-sm">Approved</span>';
							}else if($data->status=='Rejected'){
							  $sta .= '<span class="btn btn-danger btn-sm">Rejected</span>';
							}else if($data->status=='Pending'){
							  $sta .= '<span class="btn btn-warning btn-sm">Pending</span>';
							}else if($data->status=='Bypass'){
							  $sta .= '<span class="btn btn-success btn-sm">Bypass</span>';
							}
							$sta .= '&nbsp;&nbsp;';                    
							return $sta;
						})	
						
						->addColumn('action', function($data){
							$button = "";
							if(Auth::user()->can('myteam_leaves_show')){
								//showViewDetails
								$button = '<button type="button" name="viewdetails" data-id="'.$data->id.'" emp_id="'.$data->user_id.'" class="viewdetails btn btn-primary"><i class="fa fa-eye"></i></button>';
								$button .= '&nbsp;&nbsp;';
							}
								
								//HR Manager
								//HR Manager
								if(Auth()->user()->role_id==16){
									//TEACHERS
									//dd(count($data->leaveapprovalcheck));
/* 									foreach ($data->leaveapprovalcheck as $leaveAppChk){
										if($data->applicant->role_id==30){
											$team = DB::table('team_user')->where('user_id','=',$data->user_id)->first();
											
											$shift_manager = \App\Team::where('id','=',$team->team_id)->first();
											
											$team_sm = DB::table('team_user')->where('user_id','=',$shift_manager->teamlead_id)->first();
											
											$shift_manager_sm = \App\Team::where('id','=',$team_sm->team_id)->first();
											
											 if($shift_manager_sm->teamlead_id==$leaveAppChk->approved_by){
											$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
											leave_of_user="'.$data->user_id.'" 
											class="add_status btn btn-primary" title="Update leave status!"><i class="fa fa-check"></i></button>';
											$button .= '&nbsp;&nbsp;'; 
											}
										}
										else{
											if($leaveAppChk->approved_by!='' || $data->applicant->role_id==22){
												$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
												leave_of_user="'.$data->user_id.'" 
												class="add_status btn btn-primary" title="Update leave status!!!"><i class="fa fa-check"></i></button>';
												$button .= '&nbsp;&nbsp;'; 
											}
										}
									} */
									if(Auth::user()->can('hrLeavesApprovedRejected')){
										if(count($data->leaveapprovalcheck)>=2 && $data->applicant->role_id==30){
													$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
													leave_of_user="'.$data->user_id.'" 
													class="add_status btn btn-primary" title="Update leave status!!!"><i class="fa fa-check"></i></button>';
													$button .= '&nbsp;&nbsp;'; 										
										}
										else{
											if(count($data->leaveapprovalcheck)>=1 && $data->applicant->role_id!=30){
													$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
													leave_of_user="'.$data->user_id.'" 
													class="add_status btn btn-primary" title="Update leave status!!!!!"><i class="fa fa-check"></i></button>';
													$button .= '&nbsp;&nbsp;'; 
											}													
										}
									}
									
									
 
								}								
						//Bypass button hide on RECORD FOUND
						if($data->bypass==1){
							//show nothing
						}
						else{
							if(Auth::user()->can('LeavesBypass')){
								$button .= '<button type="button" name="bypass_leave" leave_id_ctrlr_bypass="'.$data->id.'" 
								leave_of_user_bypass="'.$data->user_id.'" 
								class="bypass_leave btn btn-danger" title="Bypass Leave">Bypass Leave</button>';
								$button .= '&nbsp;&nbsp;';  
							}
						}

							return $button;
						})
                    ->rawColumns(['id','status','action'])
                    ->make(true);
        }
        return view('leaves.hr_leaves');	
    }	


	/**
	 * myteam_leaves APPROVED/REJECTED
	 * @param  $request
	 * @return mixed
	 */
	public function hrLeavesApprovedRejected(Request $request){

		$rules = array(
			
			'comments' => 'required',
			'statustype' => 'required|not_in:0',
			'leaves_id_form' => 'required',
			'leave_of_user' => 'required',
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		//MONTH START DATE
		$current_date = '01';
		$current_month = date('m');
		$current_year = date('Y');
		$MONTH_START_DATE = $current_year."-".$current_month."-".$current_date;
		//////////////////
		//MONTH END DATE
		$MONTH_END_DATE  = date('Y-m-t');			
		$check_approval = \App\LeaveApproval::where('leave_id',$request->get('leaves_id_form'))->whereBetween('created_at',[$MONTH_START_DATE,$MONTH_END_DATE])->where('leave_of_user',$request->get('leave_of_user'))->where('approved_by',auth()->user()->id)->first();	
		//check if already leave approval has NOT been updated
		if ($check_approval === null ) {		
			$form_data = array(
				'leave_id'         => $request->get('leaves_id_form'),
				'approved_by'      => auth()->user()->id,		
				'leave_of_user'      => $request->get('leave_of_user'),
				'comments'        => $request->get('comments'),
				'status'            => $request->get('statustype'),

				//'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				//'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				
			);
			\App\LeaveApproval::create($form_data);


		$checkLeaveApproval = LeaveApproval::where('leave_id',$request->get('leaves_id_form'))->where('leave_of_user',$request->get('leave_of_user'))
		->where('status','=','Rejected')
		->first();
		//dd($checkLeaveApproval);
		//check if all status are ACCEPTED and make ispaid=0 on  any 1 REJECTED
		if ($checkLeaveApproval === null) {	
			$leave_data = array(
				'status'         => 'Approved',
				'ispaid'      => 1,		
				
			);
			Leave::whereId($request->get('leaves_id_form'))->update($leave_data);
			//APPROVED
			//----------------------------------------**************-------------------------------------------			
			$leavetype="";
			$NoOfDays="";
			$paid=0;			
			//Get leave dated
			$leave_date = Leave::whereId($request->get('leaves_id_form'))->where('user_id',$request->get('leave_of_user'))->first();
                $dated=date_create($leave_date->dated);
                $dtformat = date_format($dated,"Y-m-d");		
				//dd($dtformat);
				//setting variables
				$user_id = $request->get('leave_of_user');
				$leavetype = $leave_date->leavetype;
				$paid = $leave_date->ispaid;
				$description = $leave_date->description;
				
                //Store in Att Table Begins
				$att=  \App\Attendancesheet::where('dated','=', $dtformat)->where('user_id',$user_id)->first();
				//dd($att);
				if ($att === null) {
				   // user doesn't exist
				   $att_insert= new \App\Attendancesheet;
				   $att_insert->user_id= $user_id;

				   $att_insert->dated= strtotime($dtformat);
					$date = $dtformat;
					$d    = new DateTime($date);
					$dayname = $d->format('D'); //dayname	   
				   $att_insert->dayname= $dayname;
				   $att_insert->attendancedate= strtotime($dtformat);
                    if($paid==1){
                        $status=$leavetype;
                        $paid=1;
                    }else{
                        $status='UL';
                        $paid=0;
                    }
				   $att_insert->status = $status;
				   $att_insert->paid = $paid;
				   $att_insert->remarks= $leavetype.' - '.$description;
				   $att_insert->isupdated = 1;
				   $att_insert->created_at= date('Y-m-d H:i:s');
				   $att_insert->updated_at= date('Y-m-d H:i:s');
				   $att_insert->save();
				}
				else{
					if(!empty($att)){
						if($att->status=='X' or $att->status=='UL'){
							//Update Leave Status begins
							//echo "Data found with absent.";
							if($paid==1){
								$status=$leavetype;
								$paid=1;
							}else{
								$status='UL';
								$paid=0;
							}
							$att->status=$status;
							$att->paid=$paid;							
							$att->remarks=$leavetype.' - '.$description;
							$att->isupdated = 1;
							$att->paid=$paid;
							$att->updated_at= date('Y-m-d H:i:s');
							$att->save();
							//Update Leave Status ends                   
						}else{
							//echo "Data found but not absent.";
						}
						
					}					
				}
                //Store in Att Table Ends			
			//----------------------------------------**************-------------------------------------------					
			
			
			return response()->json(['success' => 'Leave status updated - AS ACCEPTED.']);				

		}
		else{
			$leave_data = array(
				'status'         => 'Rejected',
				'ispaid'      => 0,		
				
			);
			Leave::whereId($request->get('leaves_id_form'))->update($leave_data);
			//REJECTED
			//----------------------------------------**************-------------------------------------------			
			$leavetype="";
			$NoOfDays="";
			$ispaid=0;			
			//Get leave dated
			$leave_date = Leave::whereId($request->get('leaves_id_form'))->where('user_id',$request->get('leave_of_user'))->first();
                $dated=date_create($leave_date->dated);
                $dtformat = date_format($dated,"Y-m-d");		
				//dd($dtformat);
				//setting variables
				$user_id = $request->get('leave_of_user');
				$leavetype = $leave_date->leavetype;
				$paid = $ispaid;
				$description = $leave_date->description;
				
                //Store in Att Table Begins
				$att=  \App\Attendancesheet::where('dated','=', $dtformat)->where('user_id',$user_id)->first();
				//dd($att);
				if ($att === null) {
				   // user doesn't exist
				   $att_insert= new \App\Attendancesheet;
				   $att_insert->user_id= $user_id;

				   $att_insert->dated= strtotime($dtformat);
					$date = $dtformat;
					$d    = new DateTime($date);
					$dayname = $d->format('D'); //dayname	   
				   $att_insert->dayname= $dayname;
				   $att_insert->attendancedate= strtotime($dtformat);
                    if($paid==1){
                        $status=$leavetype;
                        $paid=1;
                    }else{
                        $status='UL';
                        $paid=0;
                    }
				   $att_insert->status = $status;
				   $att_insert->paid = $paid;
				   $att_insert->remarks= $leavetype.' - '.$description;
				   $att_insert->isupdated = 1;
				   $att_insert->created_at= date('Y-m-d H:i:s');
				   $att_insert->updated_at= date('Y-m-d H:i:s');
				   $att_insert->save();
				}
				else{
					if(!empty($att)){
						if($att->status=='X' or $att->status=='UL'){
							//Update Leave Status begins
							//echo "Data found with absent.";
							if($paid==1){
								$status=$leavetype;
								$paid=1;
							}else{
								$status='UL';
								$paid=0;
							}
							$att->status=$status;
							$att->paid=$paid;							
							$att->remarks=$leavetype.' - '.$description;
							$att->isupdated = 1;
							$att->updated_at= date('Y-m-d H:i:s');
							$att->save();
							//Update Leave Status ends                   
						}else{
							//echo "Data found but not absent.";
						}
						
					}					
				}
                //Store in Att Table Ends			
			//----------------------------------------**************-------------------------------------------				
			return response()->json(['success' => 'Leave status updated - AS REJECTED.']);
		}
		}else{
			return response()->json(['errors' => 'Leave status Already updated']);
		}
		
		
	}	
	
	
    public function hr_leaves_show(Request $request)
    {
        //
        //$this->authorize('show-parents');
        /* if(!empty($request->showstatus)){
            $showstatus=$request->showstatus;
        }else{
            $showstatus=0;
        } */
        $leave_approval_details = \App\LeaveApproval::with('approvedby')->with('leaveofuser')->where('leave_id',$request->id)->get();
		//dd($parents);
		$emp_details = \App\User::where('status',1)->where('id',$request->emp_id)
							->where('iscustomer',0)
							->first();//dd($student_details);
        //$hrleadstatus = Hrleadstatus::where('hrlead_id',$request->id)->get();
		//$addressbooks = \App\Addressbook::with('createdby')->where('user_id',$request->id)->where('type',1)->get();
		//$addressbooksphone = \App\Addressbook::with('createdby')->where('user_id',$request->id)->where('type',2)->get();
		
        if($request->ajax()) {
            return  view('leaves.myteam_showajax')->with(compact('leave_approval_details','emp_details'));
            
        }		
    }


	/**
	 * myteam_leaves bypass
	 * @param  $request
	 * @return mixed
	 */
	public function myTeamLeavesBypass(Request $request){

		$rules = array(
			
			'comments_bypass' => 'required',
			'statustype_bypass' => 'required|not_in:0',
			'leaves_id_form_bypass' => 'required',
			'leave_of_user_bypass' => 'required',
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
			
			$form_data = array(
				'leave_id'         => $request->get('leaves_id_form_bypass'),
				'approved_by'      => auth()->user()->id,		
				'leave_of_user'      => $request->get('leave_of_user_bypass'),
				'comments'        => $request->get('comments_bypass'),
				'status'            => $request->get('statustype_bypass'),

				//'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				//'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				
			);
			\App\LeaveApproval::create($form_data);

		if($request->get('statustype_bypass')=='Approved'){				
			$bypass_data = array(

				'status'            => $request->get('statustype_bypass'),
				'bypass'            => 1,
				'ispaid'            => 1,
				'updated_at'         => date('Y-m-d H:i:s')
			);
			Leave::whereId($request->get('leaves_id_form_bypass'))->update($bypass_data);
		}
		
		if($request->get('statustype_bypass')=='Rejected'){				
			$bypass_data = array(

				'status'            => $request->get('statustype_bypass'),
				'bypass'            => 1,
				'ispaid'            => 0,
				'updated_at'         => date('Y-m-d H:i:s')
			);
			Leave::whereId($request->get('leaves_id_form_bypass'))->update($bypass_data);
		}		
			//BYPASS APPROVED/REJECTED
			//----------------------------------------**************-------------------------------------------			
			$leavetype="";
			$NoOfDays="";
			$paid=0;			
			//Get leave dated
			$leave_date = Leave::whereId($request->get('leaves_id_form_bypass'))->where('user_id',$request->get('leave_of_user_bypass'))->first();
                $dated=date_create($leave_date->dated);
                $dtformat = date_format($dated,"Y-m-d");		
				//dd($dtformat);
				//setting variables
				$user_id = $request->get('leave_of_user_bypass');
				$leavetype = $leave_date->leavetype;
				$paid = $leave_date->ispaid;
				$description = $leave_date->description;
				
                //Store in Att Table Begins
				$att=  \App\Attendancesheet::where('dated','=', $dtformat)->where('user_id',$user_id)->first();
				//dd($att);
				if ($att === null) {
				   // user doesn't exist
				   $att_insert= new \App\Attendancesheet;
				   $att_insert->user_id= $user_id;

				   $att_insert->dated= strtotime($dtformat);
					$date = $dtformat;
					$d    = new DateTime($date);
					$dayname = $d->format('D'); //dayname	   
				   $att_insert->dayname= $dayname;
				   $att_insert->attendancedate= strtotime($dtformat);
                    if($paid==1){
                        $status=$leavetype;
                        $paid=1;
                    }else{
                        $status='UL';
                        $paid=0;
                    }
				   $att_insert->status = $status;
				   $att_insert->paid = $paid;
				   $att_insert->remarks= $leavetype.' - '.$description;
				   $att_insert->isupdated = 1;
				   $att_insert->created_at= date('Y-m-d H:i:s');
				   $att_insert->updated_at= date('Y-m-d H:i:s');
				   $att_insert->save();
				}
				else{
					if(!empty($att)){
						if($att->status=='X' or $att->status=='UL'){
							//Update Leave Status begins
							//echo "Data found with absent.";
							if($paid==1){
								$status=$leavetype;
								$paid=1;
							}else{
								$status='UL';
								$paid=0;
							}
							$att->status=$status;
							$att->paid=$paid;							
							$att->remarks=$leavetype.' - '.$description;
							$att->isupdated = 1;
							$att->paid=$paid;
							$att->updated_at= date('Y-m-d H:i:s');
							$att->save();
							//Update Leave Status ends                   
						}else{
							//echo "Data found but not absent.";
						}
						
					}					
				}
                //Store in Att Table Ends			
			//----------------------------------------**************-------------------------------------------					
			
			

		return response()->json(['success' => 'Leave Bypassed.']);				
		
	}		
	
}
