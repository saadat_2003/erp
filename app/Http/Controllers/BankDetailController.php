<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use \App\BankDetail;

class BankDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=\App\User::with('department')->with('designation')->where('iscustomer',0)->where('status',1)->get();
		//$data = \App\UserIncreament::with('increamentofuser')->with('createdby')->orderBy('id','ASC')->get();
        return view('bankdetails.index',compact('users'));
    }

    public function fetch(Request $request){

        $data = \App\BankDetail::with('bank_detail_of_user')->with('createdby')->orderBy('id','ASC')->get();
        //dd($data);
        return DataTables::of($data)
        ->addColumn('id',function($data){
            return $data->id;
        })		
        ->addColumn('user_id',function($data){
            return $data->bank_detail_of_user->fname.' '.$data->bank_detail_of_user->lname;
        })
        ->addColumn('bankName',function($data){
            return $data->bankName;
        })
        ->addColumn('bankAccountNumber',function($data){
            return $data->bankAccountNumber;
        })
        ->addColumn('createdby',function($data){
			return $data->createdby->fname.' '.$data->createdby->lname;
         
        })
        ->addColumn('options',function($data){
            $action = '<span class="action_btn">';
			//edit
			if(Auth::user()->can('increament-edit')){
			$action .= '<button type="button" name="Edit" id="'.$data->id.'" 
												class="edit btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>';
												$action .= '&nbsp;&nbsp;';
			}
			
			//delete
			if(Auth::user()->can('increament-destroy')){
			$action .= '<button type="button" name="Delete" id="'.$data->id.'" 
												class="delete btn btn-danger" title="Delete"><i class="fa fa-trash"></i></button>';
												$action .= '&nbsp;&nbsp;';
			}
		
            $action .= '</span>';
            return $action; 
                                
        })
        ->rawColumns(['options','id','user_id','bankName','bankAccountNumber','createdby'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		//dd($request->all());
		$rules = array(
			'user_id' => 'required',
			'bankName' => 'required',
			'bankAccountNumber' => 'required',
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		$check_dup = \App\BankDetail::where('user_id',$request->get('user_id'))->first();
		if($check_dup === null){		
			$form_data = array(
				'user_id'         => $request->get('user_id'),
				'bankName'         => $request->get('bankName'),
				'bankAccountNumber'         => $request->get('bankAccountNumber'),
				'created_by'        => auth()->user()->id,
				'created_at'         => date('Y-m-d H:i:s'),
				'updated_at'         => date('Y-m-d H:i:s'),
				
			);
			\App\BankDetail::create($form_data);
			return response()->json(['success' => 'Bank Details Added.']);
		}
		else{
			return response()->json(['errors' => 'Bank Details Duplication']);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(request()->ajax())
        {
            $data = \App\BankDetail::findOrFail($id);
            return response()->json(['data' => $data]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
		$rules = array(
			//'increament_of_user' => 'required',
			'bankName' => 'required',
			'bankAccountNumber' => 'required',

		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		
			$form_data = array(
				//'user_id'         => $request->get('increament_of_user'),
				'bankName'         => $request->get('bankName'),
				'bankAccountNumber'         => $request->get('bankAccountNumber'),
				//'created_by'        => auth()->user()->id,
				'updated_at'         => date('Y-m-d H:i:s'),
				
			);
			
			BankDetail::whereId($request->hidden_id)->update($form_data);


		return response()->json(['success' => 'Bank Details Updated.']);	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = BankDetail::findOrFail($id);
        $data->delete();
    }
}
