<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Leave;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use \App\User;
use \App\Preference;
use \App\Holiday;
use \App\Department;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Team;
use App\Notifications\LeaveNotification;
use Notification;

use \App\LeaveApproval;


class LeaveShiftteamController extends Controller
{
    
    public function shiftteam_leaves()
    {
        //
        if(request()->ajax())
        {
				//get shift for the SHIFT MANAGER
				$get_shift = \App\User::with('staffdetails')->where('id',auth()->user()->id)->first();
				//dd($get_shift->staffdetails->shift);
				//get all the user_id on shift(day OR night) and check that in leaves table
				$all_staff_shift = DB::table('staffdetails')->where('shift',$get_shift->staffdetails->shift)->pluck('user_id');
				//dd($all_staff_shift);

				$data = \App\Leave::with('applicant')->with('createdby')->with('modifiedby')->with('leaveapprovalcheck')
				->whereIn('user_id',$all_staff_shift)
				->orderBy('id', 'DESC')->orderBy('status', 'ASC')
				->get();	

//dd($data->toArray());				
			
            return datatables()->of($data)

					->addColumn('leaveapprovalcheck',function($data){
						//return $data->leaveapprovalcheck;
						foreach ($data->leaveapprovalcheck as $leaveAppChk){
							return $leaveAppChk->comments;
						}						
						
					})
			
					->addColumn('leaves_id',function($data){
						return $data->id;
					})
					->addColumn('applicant',function($data){
						return $data->applicant->fname.' '.$data->applicant->lname;
					})
					->addColumn('ispaid', function($data){
						$ispaid = "";
						if($data->ispaid=='1') {
						  $ispaid .= 'Yes';
						}else{
						  $ispaid .= 'No';
						}                  
						return $ispaid;
					})

					->addColumn('createdby',function($data){
						return $data->createdby->fname.' '.$data->createdby->lname;
					})

					->addColumn('modified_by',function($data){
						return $data->modifiedby->fname.' '.$data->modifiedby->lname;
					})
					
					->addColumn('created_at',function($data){
						return $data->created_at;
					})
					
						->addColumn('status', function($data){
							$sta = "";
							if($data->status=='Approved') {
							  $sta .= '<span class="btn btn-success btn-sm">Approved</span>';
							}else if($data->status=='Rejected'){
							  $sta .= '<span class="btn btn-danger btn-sm">Rejected</span>';
							}else if($data->status=='Pending'){
							  $sta .= '<span class="btn btn-warning btn-sm">Pending</span>';
							}else if($data->status=='Bypass'){
							  $sta .= '<span class="btn btn-success btn-sm">Bypass</span>';
							}
							$sta .= '&nbsp;&nbsp;';                    
							return $sta;
						})	
						
                    ->addColumn('action', function($data){
						//myteam_leaves show
						$button = "";
						if(Auth::user()->can('myteam_leaves_show')){
								//showViewDetails
								$button.= '<button type="button" name="viewdetails" data-id="'.$data->id.'" emp_id="'.$data->user_id.'" class="viewdetails btn btn-primary"><i class="fa fa-eye"></i></button>';
								$button.= '&nbsp;&nbsp;';
							}
						if(Auth::user()->can('myleaves-delete')){
							$button.= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger"><i class="fa fa-trash"></i></button>';
							$button.= '&nbsp;&nbsp;';   
						}			
/* 						$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->leaves_id.'" 
									leave_of_user="'.$data->user_id.'" 
									class="add_status btn btn-primary" title="Update leave status"><i class="fa fa-check"></i></button>';
									$button .= '&nbsp;&nbsp;'; */
									
								//ShiftManager	
								//if(Auth()->user()->role_id==22 || Auth()->user()->role_id==5 || Auth()->user()->role_id==1  || Auth()->user()->role_id==25){	
						if(Auth::user()->can('shiftTeamLeavesApprovedRejected')){			
									//show UPDATE LEAVE STATUS if TEAMLEAD approves		//TEACHERS
									//get team id using the SHIFT MANAGER user id
									/* $team_val = \App\Team::where('teamlead_id','=',auth()->user()->id)->first(); */			
									//dd($team_val);
									//get the ALL TEAM IDS from team_user of that SHIFT MANAGER
									/* $pivotteam = DB::table('team_user')->where('team_id',$team_val->id)->pluck('user_id')->toArray();
									foreach ($data->leaveapprovalcheck as $leaveAppChk){
										if(in_array($leaveAppChk->approved_by,$pivotteam) && $data->applicant->role_id==30){
										$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
													leave_of_user="'.$data->user_id.'" 
													class="add_status btn btn-primary" title="Update leave status!"><i class="fa fa-check"></i></button>';
													$button .= '&nbsp;&nbsp;';
										}
									} */
									
									//show UPDATE LEAVE STATUS for non teachers			//NOT TEACHERS
									/* if($data->applicant->role_id!=30 && $data->applicant->role_id!=22){
									$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
												leave_of_user="'.$data->user_id.'" 
												class="add_status btn btn-primary" title="Update leave status!!!"><i class="fa fa-check"></i></button>';
												$button .= '&nbsp;&nbsp;';											
										
									} */
										if(count($data->leaveapprovalcheck)==1){
											$button .= '<button type="button" name="add_status" leave_id_ctrlr="'.$data->id.'" 
														leave_of_user="'.$data->user_id.'" 
														class="add_status btn btn-primary" title="Update leave status!!!"><i class="fa fa-check"></i></button>';
														$button .= '&nbsp;&nbsp;';									
										}									
						}									
									
								//}
								//SuperAdmin
								/* else if(Auth()->user()->role_id==1){
									//bypass leaves
								} */
								
						//		
						if(Auth::user()->can('LeavesBypass')){
							$button .= '<button type="button" name="bypass_leave" leave_id_ctrlr_bypass="'.$data->id.'" 
							leave_of_user_bypass="'.$data->user_id.'" 
							class="bypass_leave btn btn-danger" title="Bypass Leave">Bypass Leave</button>';
							$button .= '&nbsp;&nbsp;';  
						}								
						return $button;
                    })
                    ->rawColumns(['leaveapprovalcheck','id','status','action'])
                    ->make(true);
        }
        return view('leaves.shiftteam_leaves');	
    }	


	/**
	 * myteam_leaves APPROVED/REJECTED
	 * @param  $request
	 * @return mixed
	 */
	public function shiftTeamLeavesApprovedRejected(Request $request){

		$rules = array(
			
			'comments' => 'required',
			'statustype' => 'required|not_in:0',
			'leaves_id_form' => 'required',
			'leave_of_user' => 'required',
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		//MONTH START DATE
		$current_date = '01';
		$current_month = date('m');
		$current_year = date('Y');
		$MONTH_START_DATE = $current_year."-".$current_month."-".$current_date;
		//////////////////
		//MONTH END DATE
		$MONTH_END_DATE  = date('Y-m-t');			
		$check_approval = \App\LeaveApproval::where('leave_id',$request->get('leaves_id_form'))->whereBetween('created_at',[$MONTH_START_DATE,$MONTH_END_DATE])->where('leave_of_user',$request->get('leave_of_user'))->where('approved_by',auth()->user()->id)->first();	
		//check if already leave approval has NOT been updated
		if ($check_approval === null ) {		
			$form_data = array(
				'leave_id'         => $request->get('leaves_id_form'),
				'approved_by'      => auth()->user()->id,		
				'leave_of_user'      => $request->get('leave_of_user'),
				'comments'        => $request->get('comments'),
				'status'            => $request->get('statustype'),

				//'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				//'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				
			);
			\App\LeaveApproval::create($form_data);
			return response()->json(['success' => 'Leave status updated.']);		
 		}else{
			return response()->json(['errors' => 'Leave status Already updated']);
		}			
		
	}	
	
	
    public function shiftteam_leaves_show(Request $request)
    {
        //
        //$this->authorize('show-parents');
        /* if(!empty($request->showstatus)){
            $showstatus=$request->showstatus;
        }else{
            $showstatus=0;
        } */
        $leave_approval_details = \App\LeaveApproval::with('approvedby')->with('leaveofuser')->where('leave_id',$request->id)->get();
		//dd($parents);
		$emp_details = \App\User::where('status',1)->where('id',$request->emp_id)
							->where('iscustomer',0)
							->first();//dd($student_details);
        //$hrleadstatus = Hrleadstatus::where('hrlead_id',$request->id)->get();
		//$addressbooks = \App\Addressbook::with('createdby')->where('user_id',$request->id)->where('type',1)->get();
		//$addressbooksphone = \App\Addressbook::with('createdby')->where('user_id',$request->id)->where('type',2)->get();
		
        if($request->ajax()) {
            return  view('leaves.myteam_showajax')->with(compact('leave_approval_details','emp_details'));
            
        }		
    }	
	
	
	/**
	 * myteam_leaves bypass
	 * @param  $request
	 * @return mixed
	 */
	public function myTeamLeavesBypass(Request $request){

		$rules = array(
			
			'comments_bypass' => 'required',
			'statustype_bypass' => 'required|not_in:0',
			'leaves_id_form_bypass' => 'required',
			'leave_of_user_bypass' => 'required',
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		
			$form_data = array(
				'leave_id'         => $request->get('leaves_id_form_bypass'),
				'approved_by'      => auth()->user()->id,		
				'leave_of_user'      => $request->get('leave_of_user_bypass'),
				'comments'        => $request->get('comments_bypass'),
				'status'            => $request->get('statustype_bypass'),

				//'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				//'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				
			);
			\App\LeaveApproval::create($form_data);
			
			$bypass_data = array(

				'status'            => $request->get('statustype_bypass'),
				'bypass'            => 1,
				

				//'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				//'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
				
			);
			Leave::whereId($request->get('leaves_id_form_bypass'))->update($bypass_data);

		return response()->json(['success' => 'Leave Bypassed.']);				
		
	}		
	
}
