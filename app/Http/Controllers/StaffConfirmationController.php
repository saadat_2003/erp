<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use \App\User;
use \App\Preference;
use \App\Department;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Team;
use \App\StaffConfirmationDetail;


class StaffConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
		//dd($request->all());
		$rules = array(
			'empId_user_pass' => 'required',
			'emp_id' => 'required',
			
			'accounttype' => 'required|not_in:0',
			'username' => 'required',
			'password' => 'required',
			
			
		);	
		$error = Validator::make($request->all(), $rules);

		if($error->fails())
		{
			return response()->json(['errors' => $error->errors()->all()]);
		}
		
		$check_dup = \App\StaffConfirmationDetail::where('user_id',$request->get('empId_user_pass'))->where('accounttype',$request->get('accounttype'))->first();
		if($check_dup === null){
			
			$form_data = array(
				'user_id'			=> $request->get('empId_user_pass'),
				'accounttype'		=> $request->get('accounttype'),
				'username'			=> $request->get('username'),
				'password'			=> $request->get('password'),
				'created_by'		=> auth()->user()->id,
				'created_at'		=> date('Y-m-d H:i:s'),
				'updated_at'		=> date('Y-m-d H:i:s'),
				
			);
			\App\StaffConfirmationDetail::create($form_data);
			return response()->json(['success' => 'Staff Credentials Added.']);
		}
		else{
			//'Same account type duplication found'
			return response()->json(['errors' => 'Same account type duplication found']);
		}

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	



	/**
	 * fetch StaffConfirmationDetail
	 * @param  $request
	 * @return mixed
	 */	
    public function fetchStaffConfirmationDetail(Request $request){

        $data = \App\StaffConfirmationDetail::with('staffconfirmationdetail_of_user')->with('staffconfirmationdetail_comments')->where('user_id', $request->get('empId_user_pass'))->orderBy('id', 'DESC')->get();
        $response=array('data'=>$data);
        //return response()->json($response,200);	
		return json_encode($response); 
    }		
}
