<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Leave;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use \App\User;
use \App\Preference;
use \App\Holiday;
use \App\Department;
use Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Team;
use App\Notifications\LeaveNotification;
use Notification;

use \App\LeaveApproval;


class LeaveController extends Controller
{
    
    public function index()
    {
        $employees = User::where('iscustomer', 0)->where('status', 1)->orderBy('fname', 'ASC')->get();
        return view('leaves.index',compact('employees'));
    }

    public function fetch(Request $request)
    {
     
        $columns = array( 
            'id', 
            'dated',
            'description',
            'leavetype',
            'status',
            'ispaid',
            'isgroup',
            'user_id',
            'created_by',
            'modified_by',
            'created_at',
            'updated_at',
            'id',
            
        );

         $totalData = Leave::count();   
         $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            
                $data = Leave::offset($start)->limit($limit)
                         ->orderBy($order, $dir)
                         ->get();
        }else{
            $search = $request->input('search.value'); 

            $data = Leave::where('id', 'LIKE', '%' . $search . '%')
                              ->orwhere('dated', 'LIKE', '%' . $search . '%')
                              ->orwhere('leavetype', 'LIKE', '%' . $search . '%')
                              ->orwhere('description', 'LIKE', '%' . $search . '%')
                              ->orwhere('status', 'LIKE', '%' . $search . '%')
                              ->offset($start)
                              ->limit($limit)
                              ->orderBy($order, $dir)
                              ->get();


            $totalFiltered = Leave::where('id', 'LIKE', '%' . $search . '%')
                              ->orwhere('dated', 'LIKE', '%' . $search . '%')
                              ->orwhere('leavetype', 'LIKE', '%' . $search . '%')
                              ->orwhere('description', 'LIKE', '%' . $search . '%')
                              ->orwhere('status', 'LIKE', '%' . $search . '%')
                              ->count();                  
           
        }


        $dataArray = array();
        if(!empty($data))
        {
            foreach ($data as $row)
            {
                $id = $row->id;               
                $nestedData['id']  = $row->id;
                $nestedData['user_id'] = $row->applicant->fname.' '.$row->applicant->lname;
                if($row->ispaid==1)
                {
                    $nestedData['ispaid'] = "Yes";
                }else{
                    $nestedData['ispaid'] = "No";
                }

                $nestedData['leavetype'] = $row->leavetype;
                $nestedData['description'] = $row->description;
                $nestedData['dated'] = $row->dated->format('d-M-Y');
                $nestedData['created_by'] = $row->createdby->fname .' '.$row->createdby->lname;
                $nestedData['modified_by'] = $row->modifiedby->fname .' '.$row->modifiedby->lname;
                $nestedData['updated_at'] = $row->updated_at->format('d-M-Y');
                $nestedData['created_at'] = $row->created_at->format('d-M-Y');
                $editdept="";
                $deletedept="";
                $token=csrf_token();
                if($row->status=='Approved') {
                  $nestedData['status'] = '<span class="btn btn-success btn-sm">Approved</span>';
                }else if($row->status=='Rejected'){
                  $nestedData['status'] = '<span class="btn btn-danger btn-sm">Rejected</span>';
                  
                }else if($row->status=='Pending'){
                  $nestedData['status'] = '<span class="btn btn-warning btn-sm">Pending</span>';
                  
                }
				
                if(Auth::user()->can('edit-leave')){
                    $editdept="<a class='btn btn-primary edit' href='javascript:void(0)' data-id='{$id}'><i class='fa fa-edit'></i></a> ";
                }
                if(Auth::user()->can('delete-leave')){
                    $deletedept=" <a class='btn btn-danger delete' href='javascript:void(0)' data-id='{$id}'><i class='fa fa-trash'></i></a>
                    <form id=\"form$id\" action=\"{{action('LeaveController@destroy', $id)}}\" method=\"post\" role='form'>
                      <input name=\"_token\" type=\"hidden\" value=\"$token\">
                      <input name=\"id\" type=\"hidden\" value=\"$id\">
                      <input name=\"_method\" type=\"hidden\" value=\"DELETE\">
                      </form>";
                }
                $nestedData['options'] = $editdept.$deletedept;                             
                
                $dataArray[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $dataArray   
                    );
          return response()->json($json_data);   
    }

   
    public function create()
    {
        exit;
        //
    }
    //Store leave for API begins
    public function storeleaveapi(Request $request){
		//dd($request->all());
		//exit;
        $rules=[
            'empid' => 'required',
            'dated' => 'required',
            'description' => 'required',
            'leavetype' => 'required',
            'ispaid' => 'required',
            'status' => 'required',
        ];
        $errormessage=[
            'empid.required'=>"Employee info is required.",
            'dated.required'=>"Leave date is required.",
            'description.required'=>"Description is required.",
            'leavetype.required'=>"Leave type is required.",
            'ispaid.required'=>"Paid or unpaid status is required.",
            'status.required'=>"Leave status field is required.",
         ];
        $validator = Validator::make($request->all(), $rules,$errormessage);
        if ($validator->fails()) {
            //pass validator errors as errors object for ajax response
            return response()->json(['errors'=>$validator->errors()]);
        }
        $leavetype="";
        $NoOfDays="";
        $paid=0;
        $empid=$request->get('empid');
        $userdata=\App\Staffdetail::where('ccmsid', $empid)->first();
        
                        
        if(!empty($userdata)){
            $user_id=$userdata->user_id;

            if($request->get('leavetype')!=null ){
                switch ($request->get('leavetype')){
                case 'Sick':
                    $leavetype="SL";
                    break;
                case 'Casual':
                    $leavetype="CL";
                    break;
                case 'Other':
                    $leavetype="OL";
                    break;
                }
            }else{
                $leave="SL";
            }
            
            $NoOfDays=$request->get('NoOfDays');
            $paid=$request->get('ispaid');
            //$fromDate=$request->get('fromDate');
            //$toDate=$request->get('toDate');
            if($NoOfDays == 1){
                //Store in Leave Table Begins
                $leave= new \App\Leave;
                $dated=date_create($request->get('fromDate'));
                $dtformat = date_format($dated,"Y-m-d");
                $leave->dated = strtotime($dtformat);
                $leave->description=$request->get('description'); 
                $leave->leavetype=$leavetype;
                $leave->status="Approved";     
                $leave->ispaid=$paid;
                $leave->isgroup=0;        
                $leave->user_id=$user_id;
                $leave->created_by=1;
                $leave->modified_by=1;
                $date=date_create($request->get('dated'));
                $format = date_format($date,"Y-m-d H:i:s");
                $leave->created_at = strtotime($format);
                $leave->updated_at = strtotime($format);
                $leave->save();
                //Store in Leave Table Ends

                //Store in Att Table Begins
				$att=  \App\Attendancesheet::where('dated','<=', $dtformat)->where('user_id',$user_id)->first();
				//dd($att);
				if ($att === null) {
				   // user doesn't exist
				   $att_insert= new \App\Attendancesheet;
				   $att_insert->user_id= $user_id;

				   $att_insert->dated= strtotime($dtformat);
					$date = $dtformat;
					$d    = new DateTime($date);
					$dayname = $d->format('D'); //dayname	   
				   $att_insert->dayname= $dayname;
				   $att_insert->attendancedate= strtotime($dtformat);
                    if($paid==1){
                        $status=$leavetype;
                        $paid=1;
                    }else{
                        $status='UL';
                        $paid=0;
                    }
				   $att_insert->status = $status;
				   $att_insert->paid = $paid;
				   $att_insert->remarks= $leavetype.' - '.$request->get('description');
				   $att_insert->created_at= strtotime($format);
				   $att_insert->updated_at= strtotime($format);
				   $att_insert->save();
				}
				else{
					if(!empty($att)){
						if($att->status=='X' or $att->status=='UL'){
							//Update Leave Status begins
							echo "Data found with absent.";
							if($paid==1){
								$status=$leavetype;
								$paid=1;
							}else{
								$status='UL';
								$paid=0;
							}
							$att->status=$status;
							$att->remarks=$leave->leavetype.' - '.$leave->description;
							$att->paid=$paid;
							$att->save();
							//Update Leave Status ends                   
						}else{
							echo "Data found but not absent.";
						}
						
					}					
				}
                //Store in Att Table Ends
				

            }else{
                //echo $empid."<br>";
                //echo "<hr>".$empleave->fromDate."<br>";
                //echo $empleave->toDate."<br>";
                $begin=new DateTime(date('Y-m-d', strtotime($request->get('fromDate'))));
                $end=new DateTime(date('Y-m-d', strtotime($request->get('toDate'))));      
                $end = $end->modify( '+1 day' );             
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod($begin, $interval ,$end);
                foreach($daterange as $dt){
                    //echo "dated= ".$dt->format('Y-m-d')."<br>";
                    //Store in Leave Table Begins
                    $leave= new \App\Leave;
                    $dated=date_create($dt->format('Y-m-d'));
                    $dtformat = date_format($dated,"Y-m-d");
                    $leave->dated = strtotime($dtformat);
                    $leave->description=$request->get('description'); 
                    $leave->leavetype=$leavetype;
                    $leave->status="Approved";     
                    $leave->ispaid=$paid;
                    $leave->isgroup=0;        
                    $leave->user_id=$user_id;
                    //$leave->user_id=1;
                    $leave->created_by=1;
                    $leave->modified_by=1;
                    $date=date_create($request->get('dated'));
                    $format = date_format($date,"Y-m-d H:i:s");
                    $leave->created_at = strtotime($format);
                    $leave->updated_at = strtotime($format);
                    $leave->save();
                    //Store in Leave Table Ends

					//Store in Att Table Begins
					$att=  \App\Attendancesheet::where('dated','<=', $dtformat)->where('user_id',$user_id)->first();
					//dd($att);
					if ($att === null) {
					   // user doesn't exist
					   $att_insert= new \App\Attendancesheet;
					   $att_insert->user_id= $user_id;

					   $att_insert->dated= strtotime($dtformat);
						$date = $dtformat;
						$d    = new DateTime($date);
						$dayname = $d->format('D'); //dayname	   
					   $att_insert->dayname= $dayname;
					   $att_insert->attendancedate= strtotime($dtformat);
						if($paid==1){
							$status=$leavetype;
							$paid=1;
						}else{
							$status='UL';
							$paid=0;
						}
					   $att_insert->status = $status;
					   $att_insert->paid = $paid;
					   $att_insert->remarks= $leavetype.' - '.$request->get('description');
					   $att_insert->created_at= strtotime($format);
					   $att_insert->updated_at= strtotime($format);
					   $att_insert->save();
					}
					else{
						if(!empty($att)){
							if($att->status=='X' or $att->status=='UL'){
								//Update Leave Status begins
								echo "Data found with absent.";
								if($paid==1){
									$status=$leavetype;
									$paid=1;
								}else{
									$status='UL';
									$paid=0;
								}
								$att->status=$status;
								$att->remarks=$leave->leavetype.' - '.$leave->description;
								$att->paid=$paid;
								$att->save();
								//Update Leave Status ends                   
							}else{
								echo "Data found but not absent.";
							}
							
						}					
					}
					//Store in Att Table Ends


                }
                //echo "<hr>";
            }
        }
        return response()->json(['success'=>'Created successfully.']);
        
    }   
    //Store leave for API ends

    public function store(Request $request)
    {
        
        $this->authorize('create-leave');
        $rules=[
            'user_id' => 'required',
            'dated' => 'required',
            'description' => 'required',
            'leavetype' => 'required',
            'ispaid' => 'required',
            'status' => 'required',
        ];
        $errormessage=[
            'user_id.required'=>"Employee is required.",
            'dated.required'=>"Leave date is required.",
            'description.required'=>"Description is required.",
            'leavetype.required'=>"Leave type is required.",
            'ispaid.required'=>"Paid leave status is required.",
            'status.required'=>"Leave status field is required.",
         ];
        $validator = Validator::make($request->all(), $rules,$errormessage);
        if ($validator->fails()) {
            //pass validator errors as errors object for ajax response
            return response()->json(['errors'=>$validator->errors()]);
        }

        $leave= new \App\Leave;
        $leave->dated=$request->get('dated');
        $leave->description=$request->get('description'); 
        $leave->leavetype=$request->get('leavetype');
        $leave->status=$request->get('status');     
        $leave->ispaid=$request->get('ispaid');
        $leave->isgroup=0;        
        $leave->user_id=$request->get('user_id');
        $leave->created_by=auth()->user()->id;
        $leave->modified_by=auth()->user()->id;
        $date=date_create($request->get('date'));
        $format = date_format($date,"Y-m-d H:i:s");
        $leave->created_at = strtotime($format);
        $leave->updated_at = strtotime($format);
		$leave->save();

        return response()->json(['success'=>'Created successfully.']);
        
    }
    
    
    public function show(Department $department)
    {
        exit; 
    }

    public function edit(Request $request)
    {
        $this->authorize('edit-leave');
        $data = Leave::findOrFail($request->id);
        return response()->json($data);
    }

  
    public function update(Request $request)
    {
        $this->authorize('edit-leave');
        $leave = \App\Leave::findOrFail($request->get('id'));
        $rules=[
            'user_id' => 'required',
            'dated' => 'required',
            'description' => 'required',
            'leavetype' => 'required',
            'ispaid' => 'required',
            'status' => 'required',
        ];
        $errormessage=[
            'user_id.required'=>"Employee is required.",
            'dated.required'=>"Leave date is required.",
            'description.required'=>"Description is required.",
            'leavetype.required'=>"Leave type is required.",
            'ispaid.required'=>"Paid leave status is required.",
            'status.required'=>"Leave status field is required.",
         ];
        $validator = Validator::make($request->all(), $rules,$errormessage);
        if ($validator->fails()) {
            //pass validator errors as errors object for ajax response
            return response()->json(['errors'=>$validator->errors()]);
        }
        $leave->dated=$request->get('dated');
        $leave->description=$request->get('description'); 
        $leave->leavetype=$request->get('leavetype');
        $leave->status=$request->get('status');     
        $leave->ispaid=$request->get('ispaid');
        $leave->isgroup=0;        
        $leave->user_id=$request->get('user_id');
        $leave->created_by=auth()->user()->id;
        $leave->modified_by=auth()->user()->id;
        $date=date_create($request->get('date'));
        $format = date_format($date,"Y-m-d H:i:s");
        $leave->created_at = strtotime($format);
        $leave->updated_at = strtotime($format);
		$leave->save();
        return response()->json(['success'=>'Updated successfully.']);
    }
    

    public function destroy(Request $request, $id)
    {
        $this->authorize('delete-leave');
        try{
            $id=$id;
            $department = \App\Leave::findOrFail($id);
            $department->delete();
            return response()->json(['success'=>'Deleted successfully.']);
        } catch(\Illuminate\Database\QueryException $ex){ 
            return response()->json(['success'=>'Unable to delete, this leave has linked record(s) in system.']);
        }
    }
	
	
	
	
	
	    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    /* myleaves view Begins */
    public function myleaves()
    {
		$employees=\App\User::where('iscustomer',0)->where('status',1)->with('staffdetails')->where('id',auth()->user()->id)->first();
		//casual leaves count
		//FOR CASUAL
		//MONTH START-END/////////////////////////////////////////////////////////////////////////////////////
		//MONTH START DATE
		$current_date = '01';
		$current_month = date('m');
		$current_year = date('Y');
		$MONTH_START_DATE = $current_year."-".$current_month."-".$current_date;
		//////////////////
		//MONTH END DATE
		$MONTH_END_DATE  = date('Y-m-t');
		$rowcount_casual = Leave::where('leavetype','CL')->whereBetween('created_at',[$MONTH_START_DATE,$MONTH_END_DATE])->where('user_id',auth()->user()->id)->count();
		//dd($rowcount_casual);
		
		//sick leaves count
		//FOR SICK
		//START/////////////////////////////////////////////////////////////////////////////////////
		//JANUARY
		$jan_date = '01';
		$jan_month = '01';
		$current_year = date('Y');
		$JANUARY = $current_year."-".$jan_month."-".$jan_date;
		//////////////////
		//DECEMBER
		$dec_date = '31';
		$dec_month = '12';
		$current_year = date('Y');
		$DECEMBER = $current_year."-".$dec_month."-".$dec_date;
		//////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////		
		$rowcount_sick = Leave::where('leavetype','SL')->whereBetween('created_at',[$JANUARY,$DECEMBER])->where('user_id',auth()->user()->id)->count();
		//dd($rowcount_sick);
		//sick leave count from preference
		$preferences_sickleavecount= \App\Preference::whereIn('option',['sickleavecount'])->first();
		$preferences_sickleavecount = $preferences_sickleavecount->value;		
        return view('leaves.myleaves',compact('employees','rowcount_casual','rowcount_sick','preferences_sickleavecount'));
    }	 
    public function myleavesfetch(Request $request)
    {
        //
		$button = '';
        if(request()->ajax())
        {
			$data = \App\Leave::with('applicant')->where('user_id','=',auth()->user()->id)->orderBy('id', 'DESC')->get();
            return datatables()->of($data)
					->addColumn('id',function($data){
						return $data->id;
					})
					->addColumn('applicant',function($data){
						return $data->applicant->fname.' '.$data->applicant->lname;
					})
					->addColumn('ispaid', function($data){
						$ispaid = "";
						if($data->ispaid=='1') {
						  $ispaid .= 'Yes';
						}else{
						  $ispaid .= 'No';
						}                  
						return $ispaid;
					})

					->addColumn('created_by',function($data){
						return $data->createdby->fname.' '.$data->createdby->lname;
					})

					->addColumn('modified_by',function($data){
						return $data->modifiedby->fname.' '.$data->modifiedby->lname;
					})
					
					->addColumn('status', function($data){
						$sta = "";
						if($data->status=='Approved') {
						  $sta .= '<span class="btn btn-success btn-sm">Approved</span>';
						}else if($data->status=='Rejected'){
						  $sta .= '<span class="btn btn-danger btn-sm">Rejected</span>';
						}else if($data->status=='Pending'){
						  $sta .= '<span class="btn btn-warning btn-sm">Pending</span>';
						}
						$sta .= '&nbsp;&nbsp;';                    
						return $sta;
					})			
                    ->addColumn('action', function($data){
						if(Auth::user()->can('myleaves-edit')){
							if($data->status=='Pending'){
								$button = '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-success"><i class="fa fa-edit"></i></button>';
								$button .= '&nbsp;&nbsp;';
							}else{
								//show nothing
								$button = '&nbsp;&nbsp;';
							}
						}
						if(Auth::user()->can('myleaves-delete')){
							if($data->status=='Pending'){							
							$button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger"><i class="fa fa-trash"></i></button>';
							$button .= '&nbsp;&nbsp;';   
							}else{
								//show nothing
								$button = '&nbsp;&nbsp;';								
							}
						}						
						return $button;
                    })
                    ->rawColumns(['id','status','action'])
                    ->make(true);
        }
        //return view('leaves.myleaves');	
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */	
    public function myleaves_create()
    {
        //
    	return view('leaves.myleaves_create');
    }	
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function myleaves_store(Request $request)
    {
        //
		//dd($request->all());

		//Storage::disk('public')->put("icons/".$name_logo,  File::get($cover_icon));


        $rules = array(
            'dated' => 'required',
            'description' => 'required',
            'leavetype' => 'required|not_in:0',
			'emp_id' => 'required',			
        );	
        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }
		
		$check_leave = Leave::where('dated',$request->dated)->where('user_id',$request->emp_id)->first();
		//casual leaves count
		//FOR CASUAL
		//MONTH START-END/////////////////////////////////////////////////////////////////////////////////////
		//MONTH START DATE
		$current_date = '01';
		$current_month = date('m');
		$current_year = date('Y');
		$MONTH_START_DATE = $current_year."-".$current_month."-".$current_date;
		//////////////////
		//MONTH END DATE
		$MONTH_END_DATE  = date('Y-m-t');
		$rowcount_casual = Leave::where('leavetype',$request->leavetype)->whereBetween('created_at',[$MONTH_START_DATE,$MONTH_END_DATE])->where('user_id',auth()->user()->id)->count();
		//dd($rowcount_casual);		

		//sick leaves count
		//FOR SICK
		//START/////////////////////////////////////////////////////////////////////////////////////
		//JANUARY
		$jan_date = '01';
		$jan_month = '01';
		$current_year = date('Y');
		$JANUARY = $current_year."-".$jan_month."-".$jan_date;
		//////////////////
		//DECEMBER
		$dec_date = '31';
		$dec_month = '12';
		$current_year = date('Y');
		$DECEMBER = $current_year."-".$dec_month."-".$dec_date;
		//////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////		
		$rowcount_sick = Leave::where('leavetype','SL')->whereBetween('created_at',[$JANUARY,$DECEMBER])->where('user_id',auth()->user()->id)->count();
		//dd($rowcount_sick);		
		
		//check if already leave has NOT been applied
		if ($check_leave === null ) {
			if($rowcount_casual <1 && $request->leavetype=="CL"){
				$form_data = array(
					'dated'        =>  $request->dated,
					'description'         =>  $request->description,
					'leavetype'             =>  $request->leavetype,
					'user_id'             =>  $request->emp_id,
					'status'             =>  "Pending",
					'created_by'             =>  auth()->user()->id,
					'modified_by'             =>  auth()->user()->id,
				);

				Leave::create($form_data);
				$check_user_in_team = DB::table('team_user')->where('user_id',$request->emp_id)->first();
				//check if user is in team				
				if(Auth()->user()->role_id==30 || $check_user_in_team !== null){
					$team_id = DB::table('team_user')->where('user_id',$request->emp_id)->first();
					$TL_id = $team_id->team_id;
					$teamlead_id = Team::with('teamlead_name')->find($TL_id);
					
					$url=url('/leaves/');
					$creator=auth()->user()->fname.' '.auth()->user()->lname;
					//Send Notification
					$users=\App\User::with('role')->where('iscustomer',0)->where('status',1)->where('id', $teamlead_id->teamlead_id)->with('user_notify')->get();		
					$letter = collect(['title' => 'New Leave Applied','body'=>'A new leave has been applied by '.$creator.', please review it.','redirectURL'=>$url]);
					Notification::send($users, new LeaveNotification($letter));		
				}
				else{
					$url=url('/leaves/');
					$creator=auth()->user()->fname.' '.auth()->user()->lname;
					//Send Notification
					$users=\App\User::with('role')->where('iscustomer',0)->where('status',1)->where('role_id',16)->with('user_notify')->get();		
					$letter = collect(['title' => 'New Leave Applied','body'=>'A new leave has been applied by '.$creator.', please review it.','redirectURL'=>$url]);
					Notification::send($users, new LeaveNotification($letter));	
				}
				return response()->json(['success' => 'Leave Applied successfully.']);
			}
			else if($rowcount_casual >= 1 && $request->leavetype == "CL"){
				return response()->json(['errors' => 'More than 1 Casual levae not allowed in a month']);
			}
			else{
				if($request->leavetype != "CL"){
					if($rowcount_sick <6 && ($request->leavetype=="SL" || $request->leavetype=="UL")){
						$form_data = array(
							'dated'        =>  $request->dated,
							'description'         =>  $request->description,
							'leavetype'             =>  $request->leavetype,
							'user_id'             =>  $request->emp_id,
							'status'             =>  "Pending",
							'created_by'             =>  auth()->user()->id,
							'modified_by'             =>  auth()->user()->id,
						);

						Leave::create($form_data);
						$check_user_in_team = DB::table('team_user')->where('user_id',$request->emp_id)->first();
						//check if user is in team				
						if(Auth()->user()->role_id==30 || $check_user_in_team !== null){
							$team_id = DB::table('team_user')->where('user_id',$request->emp_id)->first();
							$TL_id = $team_id->team_id;
							$teamlead_id = Team::with('teamlead_name')->find($TL_id);
							
							$url=url('/leaves/');
							$creator=auth()->user()->fname.' '.auth()->user()->lname;
							//Send Notification
							$users=\App\User::with('role')->where('iscustomer',0)->where('status',1)->where('id', $teamlead_id->teamlead_id)->with('user_notify')->get();		
							$letter = collect(['title' => 'New Leave Applied','body'=>'A new leave has been applied by '.$creator.', please review it.','redirectURL'=>$url]);
							Notification::send($users, new LeaveNotification($letter));		
						}
						else{
							$url=url('/leaves/');
							$creator=auth()->user()->fname.' '.auth()->user()->lname;
							//Send Notification
							$users=\App\User::with('role')->where('iscustomer',0)->where('status',1)->where('role_id',16)->with('user_notify')->get();		
							$letter = collect(['title' => 'New Leave Applied','body'=>'A new leave has been applied by '.$creator.', please review it.','redirectURL'=>$url]);
							Notification::send($users, new LeaveNotification($letter));	
						}
						return response()->json(['success' => 'Leave Applied successfully.']);
					}
					else{
						return response()->json(['errors' => 'Sick leave limit reached']);
					}
				}
			}
		}
		else{
			//return response()->json(['error' => 'Same leavetype applied on same date']);
			return response()->json(['errors' => 'Same leavetype applied on same date']);
		}

    }	
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myleaves_edit($id)
    {
        //
        if(request()->ajax())
        {
            $data = Leave::findOrFail($id);
            return response()->json(['data' => $data]);
        }		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myleaves_update(Request $request)
    {
        //
		//dd($request->all());
        $rules = array(
            'dated' => 'required',
            'description' => 'required',
            'leavetype' => 'required|not_in:0',
			'emp_id' => 'required',			
        );

            $error = Validator::make($request->all(), $rules);

            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }


        $form_data = array(
            'dated'        =>  $request->dated,
            'description'         =>  $request->description,
            'leavetype'             =>  $request->leavetype,
			'user_id'             =>  $request->emp_id,
			'updated_at'             =>  date('Y-m-d'),
			'modified_by'             =>  auth()->user()->id,
        );
        Leave::whereId($request->hidden_id)->update($form_data);

        return response()->json(['success' => 'Leave updated successfully']);
		
    }	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myleaves_destroy($id)
    {
        //
        $data = Leave::findOrFail($id);
        $data->delete();		
    }	
	

}
