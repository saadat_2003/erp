<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceReview extends Model
{
    protected $fillable =[
		'user_id', 'review', 'complaint_id','created_by','review_date',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
		'review_date',
	];

    public function username(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }
	
    public function createdby(){
		return $this->belongsTo('App\User','created_by')->withDefault();
    }
	
    public function reviewofuser(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }	

    public function complaint_title(){
		return $this->belongsTo('App\Complaint','complaint_id')->withDefault();
    }	
	
}
