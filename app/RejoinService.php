<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejoinService extends Model
{
    protected $table = 'rejoin_services'; 
	

    protected $fillable =[
		'user_id', 'reason','attachment','new_joining_date','old_joining_date','old_ending_date',
	];
	
	protected $dates = [
		'old_joining_date',
		'old_ending_date',	
        'created_at',
		'updated_at',
	];	
	
}
