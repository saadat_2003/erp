<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIncreament extends Model
{
    protected $fillable =[
		'user_id', 'amount', 'effective_date','comments','created_by',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
		'effective_date'
	];
	
    public function createdby(){
		return $this->belongsTo('App\User','created_by')->withDefault();
    }
	
    public function increamentofuser(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }	
	
	
}
