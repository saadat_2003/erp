<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    protected $fillable =[
		'user_id', 'bankName', 'bankAccountNumber','created_by',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
	];
	
    public function createdby(){
		return $this->belongsTo('App\User','created_by')->withDefault();
    }
	
    public function bank_detail_of_user(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }	
	
	
}
