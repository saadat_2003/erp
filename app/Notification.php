<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Notification extends Model
{

	use LogsActivity;

	protected static $logAttributes = ['type', 'notifiable_type' , 'notifiable_id' , 'data' ,'read_at','created_at','updated_at','notifiable'];
    protected static $logOnlyDirty = true;

    
}
