<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAddressCode extends Model
{
    protected $table = 'staff_address_codes'; 
	

    protected $fillable =[
		'user_id','type','code','verified'
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
	];	
	
}
