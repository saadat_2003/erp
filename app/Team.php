<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_name','teamlead_id', 'department_id', 'created_by'
    ];
	
	protected $dates = [
        'created_at',
        'updated_at',
    ];
	
	public function teamlead_name(){
		return $this->belongsTo('App\User','teamlead_id','id');
	}
	
	public function emp_name(){
		return $this->belongsTo('App\User','emp_id','id');
	}

	public function createdby(){
		return $this->belongsTo('App\User','created_by','id');
	}

	public function dept_name(){
		return $this->belongsTo('App\Department','department_id','id');
	}
	
    /**
     * The users that belong to the team.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->where('status',1);
    }	
	
	
	
	
	
	
	
	
	
	
	
	
    public function memberofteam()
    {
        return $this->hasMany('App\TeamUser','team_id');
    }		
	
}
