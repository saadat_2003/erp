<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Meetinglink extends Model
{
    use SoftDeletes;
    protected $fillable =[
		'deptname', 'starttime', 'endtime', 'meetinglink', 'user_id','last_modified_by'
	];
	
	protected $dates = [
        'deleted_at',
		'created_at',
		'updated_at'
	];
	
	public function createdby(){
		return $this->belongsTo('App\User','user_id');
	}
    
    public function modifiedby(){
		return $this->belongsTo('App\User','last_modified_by');
	}

}
