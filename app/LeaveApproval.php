<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveApproval extends Model
{
    protected $fillable =[
		'leave_id', 'approved_by', 'leave_of_user','comments','status',
	];
	
	protected $dates = [
        'created_at',
		'updated_at'
	];
	
    public function approvedby(){
		return $this->belongsTo('App\User','approved_by')->withDefault();
    }
	
    public function leaveofuser(){
		return $this->belongsTo('App\User','leave_of_user')->withDefault();
    }	
	




    public function approvedbyteam(){
		return $this->belongsTo('App\Team','approved_by','teamlead_id');
    }	
	
}
