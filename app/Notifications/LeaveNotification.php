<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

use \App\UserNotification;

class LeaveNotification extends Notification
{
    use Queueable;
	private $leaveinfo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($letter)
    {
        //
		$this->leaveinfo = $letter;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
		//check if mail_chk notification is [1] in user_notifications table
		$check_user = \App\UserNotification::where('user_id',$notifiable->id)->first();	
		//dd($check_user->user_id."---".$check_user->mail_chk);
		if(empty($check_user)){
            return ['mail','database','broadcast'];
        }
        $notify_arr=array();
		if($check_user->mail_chk==1){		
            $notify_arr[]='mail';            
        }
		if($check_user->database_chk==1){		
            $notify_arr[]='database';
		}
		if($check_user->broadcast_chk==1){		
            $notify_arr[]='broadcast';
		}
        return $notify_arr;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if(isset($this->leaveinfo['redirectURL'])){
            $redirectURL=$this->leaveinfo['redirectURL'];
        }else{
            $redirectURL=url('/'); 
        }
        return (new MailMessage)
                    ->subject($this->leaveinfo['title'])
                    ->greeting('Hi '. $notifiable->fname.' '. $notifiable->lname.',' )
                    ->line($this->leaveinfo['body'])
                    ->action('Notification Action', $redirectURL);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
	
    public function toDatabase($notifiable)
    {
        return [
            'letter' => $this->leaveinfo
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'letter' => $this->leaveinfo,
            'count' => $notifiable->unreadNotifications->count(),
        ]);
    }	
}
