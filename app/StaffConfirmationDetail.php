<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffConfirmationDetail extends Model
{
    protected $fillable =[
		'user_id', 'accounttype', 'username','password','created_by',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
	];
	
    public function createdby(){
		return $this->belongsTo('App\User','created_by')->withDefault();
    }
	
    public function staffconfirmationdetail_of_user(){
		return $this->belongsTo('App\User','user_id')->withDefault();
    }	
	
    public function staffconfirmationdetail_comments(){
		return $this->belongsTo('App\Staffdetail','user_id','user_id')->withDefault();
    }	
	
	
}
