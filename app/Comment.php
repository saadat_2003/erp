<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     
    protected $table ='complaintcomments';

    protected $dates = [
        'created_at',
		'updated_at'
	];    
     
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }

    public function complaint()
    {
        return $this->belongsTo('App\Complaint', 'complaint_id');
    }
}
