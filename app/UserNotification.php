<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = 'user_notifications'; 
	

    protected $fillable =[
		'user_id','mail_chk','database_chk','broadcast_chk',
	];
	
	protected $dates = [
        'created_at',
		'updated_at',
	];	
	
}
