<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = [
        'title','description','meeting_datetime','meeting_link','recording_file','created_by',
    ];
        
    protected $dates = [
        'created_at',
        'updated_at',
		'meeting_datetime'
    ];
/*     public function lead()
    {
        return $this->belongsTo('App\Lead', 'lead_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\User', 'created_by');
    } */

	public function participant_name(){
		return $this->belongsTo('App\User','teamlead_id','id');
	}	
	
	public function createdby(){
		return $this->belongsTo('App\User','created_by','id');
	}
    /**
     * The users that belong to the team.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->where('status',1);
    }		
	
	
    public function memberofmeeting()
    {
        return $this->hasMany('App\MeetingUser','meeting_id');
    }	
	
}
