<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamUser extends Model
{

protected $table='team_user';

	public function username(){
		return $this->belongsTo('App\User','user_id','id');
	}
	
	public function teamlead_id(){
		return $this->belongsTo('App\Team','team_id','id');
	}	

	public function createdby(){
		return $this->belongsTo('App\User','created_by','id');
	}

	
}
