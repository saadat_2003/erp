<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingUser extends Model
{

protected $table='meeting_user';

	public function username(){
		return $this->belongsTo('App\User','user_id','id');
	}
	
	public function meeting_id(){
		return $this->belongsTo('App\Team','meeting_id','id');
	}	

	public function createdby(){
		return $this->belongsTo('App\User','created_by','id');
	}

	
}
