@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif
<?php

?>
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Attendance Sheet for the Month of {{date('M-Y' , strtotime($srchmonth."-01"))}}</h3>
              <span class="pull-right">
                  @can('locksalarysheet')<a href="{{ route('locksalarysheet')}}?dated={{$srchmonth."-01"}}" class="btn btn-danger"><li class="fa fa-lock"></li> Lock Salary Sheet </a>@endcan
                  <input class="custom-input" type="month" name="srchmonth" id="srchmonth" autocomplete="off"  min="2019-01" max="{{date('Y-m')}}"  value="{{$srchmonth}}" />
                  <select class="select2-multiple2" id="srcdepartment_id" name="srcdepartment_id[]" multiple="multiple">
                    <option value="">Show All Deparments</option>    
                    @foreach ($departments as $department)
                    <option value="{{$department->id}}" {{(in_array($department->id,$deparment_id)) ? "selected": "" }}>{{$department->deptname}}</option>    
                    @endforeach
                  </select>
				  <!--shift -->
						<select id="shift" name="shift" class="custom-input" data-placeholder="Select Shift" style="width: 10%;" tabindex="-1" aria-hidden="true">
							<option value="">All</option>
							<option value="day" {{ (old('shift')=="day") ? 'selected' : '' }}>Day</option>
							<option value="night" {{ (old('shift')=="night") ? 'selected' : '' }}>Night</option>
							<option value="evening" {{ (old('shift')=="evening") ? 'selected' : '' }}>Evening</option>
						</select>
			  
                  <button class="btn btn-success" id="filterDept"><li class="fa fa-search"></li></button>
                  <button class="btn btn-primary" id="btnExport" onclick="fnExcelReport();"><li class="fa fa-file-excel-o fa-lg"></li></button>
				  @can('reviewStore')<button class="review btn btn-warning" id="review" title="Review"><li class="fa fa-wechat"></li></button>@endcan
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="zui-wrapper">
              <div class="zui-scroller">
              <table id="table_data" class="zui-table table  table-hover" style="width:100%">
                <thead>
                <tr>
                  <th class="zui-sticky-col1">Sr. No. <br> &nbsp;</th>
                  <th class="zui-sticky-col2">Name <br>&nbsp; </th>                
                  <th class="zui-sticky-col3">Basic Salary <br>&nbsp; </th>
                  <th class="zui-sticky-col4">Department <br> &nbsp; </th>
                  <th class="zui-sticky-col5">Designation <br>&nbsp; </th>
                  <th class="zui-sticky-col6">Shift <br> &nbsp; </th>
                  @foreach($daterange as $date)
                  <th class="{{($date->format('D')=='Sun') ? 'bg-green' : '' }}">{{$date->format('d')}} <br> <small>{{$date->format('D')}}</small></th>
                  @endforeach
                  <th>Tardies</th>
                  <th>Short Leaves</th>
                  <th>Absents</th>
                  <th>Paid Leave</th>
                  <th>Unpaid Leave</th>
                  <th>Presents</th>
                  <th>Total Days</th>
                  <th>Deducted Days</th>
                  <th>Earned Salary</th>
                  <th>Gross Salary</th>
                  <th>Other Deductions</th>
                  <th>Absent Fine</th>
                  <th>Salary Deducted</th>
                  <th>Total Deduction</th>
                  <th>Additions</th>
                  <th>Per Day</th>
                  <th>Net Salary</th>
                </tr>
                </thead>
                <tbody>
                  @php ($basicsalarytotal=0)
                  @php ($netsalarytotal=0)
                  @php ($totaldeductions=0)
                  @php ($totaladditions=0)
                  @php ($sumsalarydeduction=0)
                  @php ($earnedsalary=0)
                  @php ($earnedsalarytotal=0)
                  @php ($grosssalarytotal=0)
                  @php ($sumabsentfine=0)
                  @php ($sumsalarydeducteddays=0)
                  @php ($srno=0)
                  @foreach($employees as $emp)
                  <?php
                  $joiningmonth=$emp->staffdetails->joiningdate->format('Y-m-d');
                  $thismonth=date('Y-m-t' , strtotime($srchmonth."-01"));
                  $to = \Carbon\Carbon::createFromFormat('Y-m-d', $joiningmonth);
                  $from = \Carbon\Carbon::createFromFormat('Y-m-d', $thismonth);                  
                  ?>
                  @if($to->lessThan($from))
                  <?php
                  $srno++;
                  $basicsalary=($emp->staffdetails->salary) ? $emp->staffdetails->salary : '0.00';
                  $basicsalarytotal+=$basicsalary;
                  if(!empty($emp->deductions_count)){
                    //$totaldeductions=$totaldeductions + $emp->deductions_count;
                  }
                  if(!empty($emp->additions_count)){
                    $totaladditions+=$emp->additions_count;
                  }
                  $grosssalary=$emp->salary+$basicsalary;
                  $earnedsalarytotal+=$emp->salary;
                  $grosssalarytotal+=$emp->salary+$basicsalary;
                  ?>
                   <tr>
                      <td class="zui-sticky-col1"><a href="{!! url('/admins/'.$emp->id); !!}" target="_blank">{{$srno}}</a></td>
                      <td class="zui-sticky-col2"><a href="{!! url('/admins/'.$emp->id); !!}" target="_blank" data-toggle="tooltip" title="{{$emp->fname}} {{$emp->lname}} :: {{$emp->department->deptname}} ">{{$emp->fname}} {{$emp->lname}}</a></td>
                      <td class="zui-sticky-col3">{{$basicsalary}}</td>
                      <td class="zui-sticky-col4">{{$emp->department->deptname}} &nbsp; </td>
                      <td class="zui-sticky-col5">{{$emp->designation->name}} &nbsp; </td>
                      <td class="zui-sticky-col6">{{ucfirst($emp->staffdetails->shift)}} &nbsp; </td>
                      <?php                      
                        $tardis=0;
                        $shortleave=0;
                        $absents=0;
                        $leaves=0;
                        $upleaves=0;
                        $presents=0;
                        $unpaiddays=0;
                        $totaldays=0;
                        $deducteddays=0;
                        $salarydeduction=0;
                        $deductiontotal=0;
                        $absentfine=0;
                        $deductions_count=0;
                       
                      ?>
                      @foreach($emp->att as $att)
                      <?php
                          if($att['dayname']=='Sun'){
                              $class="bg-green";
                          }elseif($att['status']=='H'){
                              $class="bg-green";
                          }elseif($att['status']=='X'){
                              $class="bg-red";
                          }else{
                              $class="";
                          }
                          $tardis+=$att['tardies'];
                          $shortleave+=$att['shortleaves'];
                          if($att['status']=='X'){
                            if(strtotime($att->dated) < strtotime(date("Y-m-d")) ){
                              $absents++;
                              $absentfine+=$settings['absentfine'];
                            }
                          }elseif($att['status']=='P' or $att['status']=='H'){
                            $presents++;
                          }elseif($att['status']=='-'){
                            $unpaiddays++;
                          }elseif($att['status']=='UL'){
                            $upleaves++;
                          }else{
                            $leaves++;
                          }
                          
                          if(( $att['remarks']=='Late Arrival and Early Left' || $att['remarks']=='Short Leave' || $att['remarks']=='Late Arrival' || $att['remarks']=='Early Left') && $att['status']=="P" ){
                            if(strtotime($att->dated) < strtotime(date("Y-m-d")) ){
                              $statusClass="text-yellow";
                            }else{
                              $statusClass="";
                            }
                          }elseif($att['status']=="P" && $att['dayname']!='Sun'){
                            $statusClass="text-green";
                          }else{
                            $statusClass="";
                          }

                          $totaldays++; 
                      ?>
                      <td class="{{$class}}"> <span class="{{$statusClass}}"  data-toggle="tooltip" title="{{$att['dayno']}} - {{$att['remarks']}}  ({{$att['checkin']}}-{{$att['checkout']}})">{{$att['status']}}</span></td>
                      @endforeach
                      <?php
                        //Salary Calculation beings
                        //Deduction plus fine
                        $deductions_count =($emp->deductions_count) ? $emp->deductions_count : 0;
                        //$deductions_count+= $absentfine;
                        //Tardy conversion to deducted days
                        $deducteddays+=intdiv($tardis,$settings['tardydaydeduct']);
                        //Short leaves conversion to deducted days
                        $deducteddays+=intdiv($shortleave,$settings['shortleavedaydeduct']);
                        //Absents + Unpaid Leave + Unpaid days
                        $deducteddays+=$absents+$upleaves+$unpaiddays;
                        //Per day salary
                        $perdaysalary=$grosssalary/$settings['daysinmonth'];                        
                        //Unpaid days salary
                        $salarydeduction=$perdaysalary * $deducteddays;
                        if($salarydeduction > $grosssalary){
                          $salarydeduction=$grosssalary;
                        }
                        $sumsalarydeducteddays+=$salarydeduction;
                        //Total deductions
                        $deductiontotal=$salarydeduction + $emp->deductions_count + $absentfine;
                        //$totaldeductions+=$emp->deductions_count + $absentfine;
                        $totaldeductions+=$emp->deductions_count;
                        //Net salary
                        $netsalary=$grosssalary - $deductiontotal  + $emp->additions_count;
                        if($netsalary < 0){
                          $netsalary=0;
                        }
                        //Sum Absent Fine
                        $sumabsentfine+=$absentfine;
                        //Salary Calculation ends
                        $netsalarytotal+=$netsalary;
                        $sumsalarydeduction+=$deductiontotal;

                      ?>
                      <td><span class=""  data-toggle="tooltip" title="Tardis">{{$tardis}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Short Leaves">{{$shortleave}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Absents">{{$absents}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Paid Leave">{{$leaves}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Unpaid Leave">{{$upleaves}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Presents">{{$presents}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Total Days">{{$totaldays}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Deducted Days">{{$deducteddays}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Earned Salary">{{$emp->salary}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Gross Salary">{{$emp->salary+$basicsalary}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Other Deductions">{{($deductions_count) ? $deductions_count : '0'}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Absent Fine">{{$absentfine}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Salary Deducted">{{number_format($salarydeduction,2)}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Total Deduction">{{number_format($deductiontotal,2)}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Additions">{{($emp->additions_count) ? $emp->additions_count: '0'}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Per Day">{{number_format($perdaysalary,2)}}</span></td>
                      <td><span class=""  data-toggle="tooltip" title="Net Salary">{{ number_format($netsalary,2) }}</span></td>
                   </tr>
                   @endif
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th class="zui-sticky-col1">&nbsp; <br> &nbsp;</th>
                    <th class="zui-sticky-col2">Total <br>&nbsp; </th>
                    <th class="zui-sticky-col3">{{number_format($basicsalarytotal,2)}} <br>&nbsp; </th>
                    <th class="zui-sticky-col4">Department <br> &nbsp; </th>
                    <th class="zui-sticky-col5">Designation <br>&nbsp; </th>
                    <th class="zui-sticky-col6">Shift <br> &nbsp; </th>
                    @foreach($daterange as $date)
                    <th class="{{($date->format('D')=='Sun') ? 'bg-green' : '' }}">{{$date->format('d')}} <br> <small>{{$date->format('D')}}</small></th>
                    @endforeach
                    <th colspan="8">Total</th>
                    <th>{{number_format($earnedsalarytotal,2)}}</th>
                    <th>{{number_format($grosssalarytotal,2)}}</th>
                    <th>{{number_format($totaldeductions,2)}}</th>
                    <th>{{number_format($sumabsentfine,2)}}</th>
                    <th>{{number_format($sumsalarydeducteddays,2)}}</th>
                    <th>{{number_format($sumsalarydeduction,2)}}</th>
                    <th>{{number_format($totaladditions,2)}}</th>
                    <th>-</th>
                    <th>{{number_format($netsalarytotal,2)}}</th>
                  </tr>
                </tfoot>
              </table>

              </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->        
   

<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
  

		  
<div class="box">
<div class="box-body">
<div class="box box-warning direct-chat direct-chat-warning" >
                <div class="box-header with-border">
                  <h3 class="box-title">Review History</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" id='div_body_review'>
                    <!-- Message. Default to the left -->	  
  
                  </div>
                  <!--/.direct-chat-messages-->  
				  
                </div>
                <!-- /.box-body -->

</div>
</div>				  
</div>	
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
			  <h4 class="modal-title">Post Review</h4>		
           <div class="form-group">
            <label class="control-label col-md-4">Review : </label>
            <div class="col-md-8">
             <textarea type="text" class="form-control" rows="3" id="review" name="review" placeholder="Enter review ..." autocomplete="off" required></textarea>
            </div>
           </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">

				<input type="hidden" name="action_review" id="action_review" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				
				<input type="hidden" id='review_date' name='review_date' value="{{date('Y-m-d' , strtotime($srchmonth."-01"))}}" />
				<input type="submit" name="action_button_review" id="action_button_review" class="btn btn-info" value="Post Review" />					
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>


   
      <div class="loading">
        <div class="loader"></div>
      </div>


@endsection
@push('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

<style>
.select2-container--default .select2-selection--single .select2-selection__rendered{
    line-height: 24px;
}
.select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 8px;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 4px;
}

.loading{
    display: hidden;
    position: fixed;
    left: 0;
    top: 0;
    padding-top: 45vh;
    padding-left:100vh;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: gray;
    opacity: 0.8;
}
.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


.zui-table {
    border: none;
}
.zui-table tbody td {
    white-space: nowrap;
}
.zui-wrapper {
    position: relative;
}
.zui-scroller {
    margin-left: 341px;
    /*overflow-x: scroll;*/
    overflow-y: visible;
    padding-bottom: 5px;
    width: 78%;
}
.zui-table .zui-sticky-col1 {
    left: 0;
    position: absolute;
    top: auto;
    width: 80px;
}

.zui-table .zui-sticky-col2 {
    left: 80px;
    position: absolute;
    top: auto;
    width: 160px;
}

.zui-table .zui-sticky-col3 {
    left: 240px;
    position: absolute;
    top: auto;
    width: 100px;
}

  table td:nth-child(1) {
      background: #f3f3f3;
  }
  table td:nth-child(2) {
        background: #f3f3f3;
        text-align: left;
  }

  table td:nth-child(3) {
       background: #f3f3f3;
       text-align: center;
  }
  table th:nth-child(1) {
      background: #f3f3f3;
  }
  table th:nth-child(2) {
        background: #f3f3f3;
        text-align: left;
  }

  table th:nth-child(3) {
       background: #f3f3f3;
       text-align: center;
  }

  table th, td{
    text-align: center;
  }

  table td:nth-last-child(1){
    background: #f3f3f3;
  } 

  table th:nth-last-child(1){
    background: #f3f3f3;
  }
  

  table td:nth-last-child(3){
    background: #f3f3f3;
  } 

  table th:nth-last-child(3){
    background: #f3f3f3;
  }

  table td:nth-last-child(4){
    background: #f3f3f3;
  } 

  table th:nth-last-child(4){
    background: #f3f3f3;
  } 

  table td:nth-last-child(8){
    background: #f3f3f3;
  } 

  table th:nth-last-child(8){
    background: #f3f3f3;
  } 

  .custom-input{
    height: 28px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
  }

  .select2-results__option .wrap:before{
    font-family:fontAwesome;
    color:#999;
    content:"\f096";
    width:25px;
    height:25px;
    padding-right: 10px;
    
  }
  .select2-results__option[aria-selected=true] .wrap:before{
      content:"\f14a";
  }
  /* not required css */
  .row
  {
    padding: 10px;
  }
  .select2-multiple2
  {
    width: 50%;
  }

</style>

<style>
.container_review {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

.darker {
  border-color: #ccc;
  background-color: #ddd;
}

.container_review::after {
  content: "";
  clear: both;
  display: table;
}

.container_review img {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

.container_review img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

.time-right {
  float: right;
  color: #aaa;
}

.time-left {
  float: left;
  color: #999;
}
</style>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="{{asset('js/jquery.doubleScroll.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

<script type="text/javascript" src="https://rawgit.com/wasikuss/select2-multi-checkboxes/master/select2.multi-checkboxes.js"></script>

<script type="text/javascript" src="{{asset('js/FileSaver.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tableExport.js')}}"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
  
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script type="text/javascript">
$(document).ready(function(){    
/*     $('#userTable').DataTable({
      "bDestroy": true,
      "processing":true,
      "serverSide":true,
      "order" :[ 0, "desc" ],
      "ajax":{
                  "url": "{{ route('review.fetchReview') }}",
                  "dataType": "json",
                  "type": "POST",
                  "data":{   
					_token: "{{csrf_token()}}",
					},
                },
      "columns":[
        {"data":"id"},
        {"data":"user_id"},
        {"data":"review"},
        {"data":"complaint_id"},
        {"data":"review_date"},
      ]
    }); */
	
$.ajax({
   url:"{{ route('review.fetchReview') }}",
   method:'POST',
   dataType:'json',
   "data":{   
					_token: "{{csrf_token()}}",
					},
   success:function(response){
     console.log(response);
/*  */
     var data=response.data;
     var trHTML="";
     for(var i=0;i<data.length;i++){
        var id=data[i].id;
        var user_id=data[i].username.fname+" "+data[i].username.lname;
        var review= data[i].review;
        var complaint_id= data[i].complaint_id;
		var review_date= data[i].review_date;
		review_date = moment(review_date).format('d-M-YYYY');
/*         let trRow="<tr>"+
                  "<td>"+id+"</td>"+
                  "<td>"+user_id+"</td>"+
                  "<td>"+review+"</td>"+
                  "<td>"+complaint_id+"</td>"+
				  "<td>"+review_date+"</td>"
                  +"</tr>"; */
        let trRow="<div class='container_review darker'>Review By:<b>"+user_id+"</b><span class='direct-chat-timestamp pull-right'>"+review_date+"</span><p>"+review+"</p></div>";
       trHTML=trHTML+trRow;
     }
     $('#div_body_review').empty();
     $('#div_body_review').append(trHTML);

   },
   error:function(err){
      console.log(err);
   }
});	
});
//Review
 $(document).on('click', '.review', function(){
  $('.modal-title').text("Post Review");
     $('#action_button_review').val("Post Review");
     $('#action_review').val("Post Review");
//added by me
	$('#review').val('');
	
    $('#form_result').html("");

    //var review_date = document.getElementById("srchmonth").value;
    //document.getElementById("review_date").value = review_date;
	
    event.preventDefault();	
	 
     $('#formModal').modal('show');
	 
 }); 

 $('#sample_form').on('submit', function(event){
  event.preventDefault();
//Review
  if($('#action_review').val() == 'Post Review')
  {
   $.ajax({
    url:"{{ route('reviewStore') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      //html = '<div class="alert alert-danger">';
      /* for(var count = 0; count < data.errors.length; count++)
      {
       html += '<p>' + data.errors[count] + '</p>';
      } */
      //html += '</div>';
	  swal("Error!", "More then 3 reviews Not Allowed", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      //$('#userTable').DataTable().ajax.reload();
	  //$("#div_body_review").load("#div_body_review");
	  swal("Success!", "Review Added !", "success");
//Following ajax call after SUBMIT, so that our div must be populated dynamically	start
$.ajax({
   url:"{{ route('review.fetchReview') }}",
   method:'POST',
   dataType:'json',
   "data":{   
					_token: "{{csrf_token()}}",
					},
   success:function(response){
     console.log(response);
/*  */
     var data=response.data;
     var trHTML="";
     for(var i=0;i<data.length;i++){
        var id=data[i].id;
        var user_id=data[i].username.fname+" "+data[i].username.lname;
        var review= data[i].review;
        var complaint_id= data[i].complaint_id;
		var review_date= data[i].review_date;
		review_date = moment(review_date).format('d-M-YYYY');

        let trRow="<div class='container_review darker'>Review By:<b>"+user_id+"</b><span class='direct-chat-timestamp pull-right'>"+review_date+"</span><p>"+review+"</p></div>";
       trHTML=trHTML+trRow;
     }
     $('#div_body_review').empty();
     $('#div_body_review').append(trHTML);

   },
   error:function(err){
      console.log(err);
   }
});		
//Following ajax call after SUBMIT, so that our div must be populated dynamically	end 
     }
     $('#form_result').html(html);
    }
   })
  }  
  
 });  
 
</script> 
 
 
 

<script type="text/javascript">
$(document).ready(function (e) {
  //InitTable();
  $('#table_data').wrap("<div id=\"scrooll_div\"></div>");
  $('#scrooll_div').doubleScroll();

  $('[data-toggle="tooltip"]').tooltip();   

  $('#filterDept').click( function() {
    var url;
    if($('#srcdepartment_id').val()!="" && $('#srchmonth').val()==""){
      url="{{route('attendancesheet')}}?deparment_id="+$('#srcdepartment_id').val();
    }else if($('#srchmonth').val()!="" && $('#srcdepartment_id').val()==""){
      url="{{route('attendancesheet')}}?srchmonth="+$('#srchmonth').val()+"&shift="+$('#shift').val();
    }else if($('#srchmonth').val()!="" && $('#srcdepartment_id').val()!=""){
      url="{{route('attendancesheet')}}?deparment_id="+$('#srcdepartment_id').val()+"&srchmonth="+$('#srchmonth').val();
    }/* else if($('#shift').val()!=""){
	  url="{{route('attendancesheet')}}?shift="+$('#shift').val();	
	} */
	else{
      url ="{{route('attendancesheet')}}";
    }
    window.location.href =url;
  });

  $('.select2-multiple2').select2MultiCheckboxes({
    templateSelection: function(selected, total) {
      return "Selected " + selected.length + " of " + total;
    },
  })
  $(".loading").fadeOut();

});



//Excel Export begins
function fnExcelReport()
{
  $(".loading").fadeIn();
  $("table").tableExport({type:'excel'});
  $(".loading").fadeOut();
}
//Excel Export ends

</script>

 
@endpush
