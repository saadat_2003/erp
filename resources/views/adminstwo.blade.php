@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif

<style type="text/css">
  .action_btn a{
    margin: 5px;
  }
</style>



<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Staff</h3>
            </div>
            <div class="box-body">
            <table id="userTable" class="display" style="width:100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Designation</th>
                  <th>Department</th>
                  <th>Name</th>
                  <th>Landline</th>
                  <th>Mobile</th>
                  <th>Gaurdain Number</th>
                  <th>Address</th>
                 </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Designation</th>
                    <th>Department</th>
                    <th>Name</th>
                    <th>Landline</th>
                    <th>Mobile</th>
                    <th>Gaurdain Number</th>
                    <th>Address</th>
                  </tr>
                </tfoot>
            </table>
          </div>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>


  <script type="text/javascript">
$(document).ready(function(){    
    $('#userTable').DataTable({
      "bDestroy": true,
      "processing":true,
      "serverSide":true,
      "searching":false,
      "pageLength": 200,
      "dom": 'Bfrtip',
      "buttons": [
             'csv', 'excel' ],


      "order" :[ 0, "asc" ],
      "ajax":"{{ route('admins.fetchtwo') }}",
      "columns":[
        {"data":"id"},
        {"data":"designation"},
        {"data":"department"},
        {"data":"name"},
        {"data":"landline"},
        {"data":"phonenumber"},
        {"data":"gaurdiancontact"},
        {"data":"cstreetaddress"},
      ]
    });

}); 
  </script>
@endsection
