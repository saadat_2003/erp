@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif

<style type="text/css">
  .action_btn a{
    margin: 5px;
  }
</style>

@can('stats-hr-requests')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Status & Counts</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body" style="display: none; ">
		  
			<!-- HR Hiring Request Statistics begins-->
			<div class="row">
			  <div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-blue">
				  <div class="inner">
					<h3>{{$hrstats['totalrequests']}}</h3>

					<p>Requests This Month</p>
				  </div>
				  <div class="icon">
					<i class="ion ion-person-stalker"></i>
				  </div>
				</div>
			  </div>
			<!-- /.col -->

			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
			  <div class="small-box bg-green">
				<div class="inner">
				  <h3>{{$hrstats['completedrequest']}}</h3>

				  <p>Completed/Fulfilled This Month</p>
				</div>
				<div class="icon">
				  <i class="ion ion-paper-airplane"></i>
				</div>
			  </div>
			</div>
			<!-- /.col -->
			<!-- fix for small devices only -->
			<div class="clearfix visible-sm-block"></div>

			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
			  <div class="small-box bg-yellow">
				<div class="inner">
				  <h3>{{$hrstats['inprocessdrequest']}}</h3>

				  <p>Inprocess This Month</p>
				</div>
				<div class="icon">
				  <i class="ion ion-edit"></i>
				</div>
			  </div>
			</div>
			<!-- /.col -->

			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
			  <div class="small-box bg-red">
				<div class="inner">
				  <h3>{{$hrstats['pendingrequest']}}</h3>

				  <p>Pending This Month</p>
				</div>
				<div class="icon">
				  <i class="ion ion-clipboard"></i>
				</div>
			  </div>
			</div>
			<!-- /.col -->


			</div>
			<!-- HR Hiring Request Statistics ends-->
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->
      </div>
</div>  
@endcan


<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Staff</h3>
              @can('create-staff')
              <span class="pull-right">
              <a href="{!! url('/admins/create'); !!}" class="btn btn-info"><span class="fa fa-plus"></span> Add New Staff</a>
              </span>
              @endcan
            </div>
            <div class="box-body">
            <table id="userTable" class="display responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Designation</th>
                  <th>Department</th>
                  <th>Role</th>
                  <th>Joining Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Department</th>
                    <th>Role</th>
                    <th>Joining Date</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
            </table>
          </div>
		  
		  



<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add increament</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
           <div class="form-group">
            <label class="control-label col-md-4" >Increament Amount : </label>
            <div class="col-md-8">
             <input type="number" class="form-control" id="increament" name="increament" placeholder="Enter increament amount" autocomplete="off" required>
            </div>
           </div>		  
           <div class="form-group">
            <label class="control-label col-md-4" >Effective Date : </label>
            <div class="col-md-8">
             <input type="date" class="form-control" id="effective_date" name="effective_date" placeholder="Enter Effective Date" autocomplete="off" required>
            </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-4">Comments : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="comments" name="comments" placeholder="Enter comments" autocomplete="off" required>
            </div>
           </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">

				<input type="hidden" name="action_increament" id="action_increament" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				
				<input type="hidden" id='increament_of_user' name='increament_of_user' />
				<input type="submit" name="action_button_increament" id="action_button_increament" class="btn btn-info" value="Increament" />					
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>




		  

            <!-- /.box-header -->
            {{-- <div class="box-body">
            @if(count($users) > 0)
              <table id="example1" class="display responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Designation</th>
                  <th>Department</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                  <tr>
                    <td>{{$user['id']}}</td>
                    <td>{{$user['fname']}} {{$user['lname']}}</td>
                    <td>{{$user['designation']['name']}}</td>
                    <td>{{$user['department']['deptname']}}</td>
                    <td>{{$user['role']['role_title']}}</td>
                    <td>
                      @if ($user['status'] === 1)
                      <span class="btn btn-success">Active</span>
                      @else
                      <span class="btn btn-danger">Deactive</span>
                      @endif
                    </td>
                    @can('delete-staff')
                     <!-- For Delete Form begin -->
                    <form id="form{{$user['id']}}" action="{{action('UserController@destroy', $user['id'])}}" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                    </form>
                    <!-- For Delete Form Ends -->
                    @endcan
                    <td>
                      @can('show-staff')<a href="{!! url('/admins/'.$user['id']); !!}" class="btn btn-primary" title="View Detail"><i class="fa fa-eye"></i> </a>@endcan
                      @can('edit-staff')<a href="{!! url('/admins/'.$user['id'].'/edit'); !!}"  class="btn btn-success" title="Edit"><i class="fa fa-edit"></i> </a>@endcan
                      @can('status-staff')
                        @if ($user['status'] === 1)
                          <a href="{!! url('/admins/deactivate/'.$user['id']); !!}"  class="btn btn-warning" title="Deactivate"><i class="fa fa-times"></i> </a>
                        @else
                          <a href="{!! url('/admins/active/'.$user['id']); !!}"  class="btn btn-info" title="Active"><i class="fa fa-check"></i> </a>
                        @endif
                      @endcan
                      @can('delete-staff')<button class="btn btn-danger" onclick="archiveFunction('form{{$user['id']}}')"><i class="fa fa-trash"></i></button>@endcan
                      @can('staff-reset-password')<a href="{!! url('/admins/resetpassword/'.$user->id); !!}"  class="btn btn-info" title="Reset Password"><i class="fa fa-key"></i> </a>@endcan
                    </td>                   

                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Designation</th>
                  <th>Department</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
              @else
              <div>No Record found.</div>
              @endif
            </div> --}}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

  <script type="text/javascript">
$(document).ready(function(){    
    $('#userTable').DataTable({
      "bDestroy": true,
      "processing":true,
      "serverSide":true,
      "order" :[ 0, "desc" ],
      "ajax":"{{ route('admins.fetch') }}",
      "columns":[
        {"data":"id"},
        {"data":"name"},
        {"data":"designation"},
        {"data":"department"},
        {"data":"role"},
        {"data":"joiningdate"},
        {"data":"status"},
        {"data":"options",orderable:false,searchable:false},
      ]
    });

	
//Increament
 $(document).on('click', '.increament', function(){
  $('.modal-title').text("Salary Increament");
     $('#action_button_increament').val("Increament");
     $('#action_increament').val("Increament");
//added by me
    $('#increament').val('');
	$('#effective_date').val('');
	$('#comments').val('');
	
    $('#form_result').html("");

    var increament_of_user = $(this).attr('increament_user_id_ctrlr');
    document.getElementById("increament_of_user").value = increament_of_user;
	
    event.preventDefault();	
	 
     $('#formModal').modal('show');
	 
 }); 


 $('#sample_form').on('submit', function(event){
  event.preventDefault();
//Increament
  if($('#action_increament').val() == 'Increament')
  {
   $.ajax({
    url:"{{ route('increamentStore') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
      for(var count = 0; count < data.errors.length; count++)
      {
       html += '<p>' + data.errors[count] + '</p>';
      }
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      $('#user_table').DataTable().ajax.reload();
	  swal("Success!", "Increament Added !", "success");
     }
     $('#form_result').html(html);
    }
   })
  }  
  
 }); 
 
 
}); 
  </script>
@endsection
