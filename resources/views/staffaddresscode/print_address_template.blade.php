<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Employee Address Verification</title>
</head>
<body onload="window.print();">

<table align="center" width="100%" cellpadding="0" cellspacing="0" 
style="color:#000000;font-family:Arial, Helvetica, sans-serif; font-size:12px">


	<tr>
		<!--<td align="center" height="10" colspan="2" style="border-top:#000000 3px solid;
		color:#000000; font-weight:bold; font-size:24px;padding-top:20px;text-transform:uppercase;"><span style=" font-size:24px; color:#000000; font-weight:bold;font-family:Arial, Helvetica, sans-serif;">Employee Address Verification</span> 
		</td>-->
		<td align="center" style="color:#000000; font-weight:bold; font-size:24px;
		padding-top:20px;text-transform:uppercase;">
			<img border="0" vspace="0" hspace="0"
			src="{{asset('img/ycsolutions-logo.png')}}"
			alt="Zeb Fortunes" title="Zeb Fortunes"
			width="340" style="
			width: 87.5%;
			max-width: 340px;
			color: #FFFFFF; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;"/>
		</td>		
	</tr>
	<tr>
		<td align="left" width="77%" style="color:#000000; font-weight:bold; font-size:24px;
		padding-top:20px;text-transform:uppercase;">
		<span style=" font-size:15px; color:#000000; font-weight:bold;font-family:Arial, Helvetica, 
		sans-serif;">Subject: Employee Address Verification</span>
		</td>
		
		<td align="right" valign="top" style="color:#000000; font-size:15px; font-weight:bold;
		padding-top:20px;">DATED: <?php echo date('d-m-Y') ?><br>
		</td>
	</tr>
<tr>

	<td width="23%" align="right" valign="top" style="color:#000000; font-size:24px; font-weight:bold;padding-top:20px; color:#F00">

	
	</td>
</tr>

<tr><td colspan="2"></td></tr>



<tr style="height:30px;">
    <td colspan="2" align="">
        <table align="" width="100%" style="font-size:15px; color:#000000;" 
		cellpadding="0" cellspacing="0">
	

			<!-- loop comes here		start -->
			
			<tr style="height:50px;">
                <td align="" style="">
					<div data-html="true">
						<?php echo $form_data; ?>
					</div>
				</td>
			</tr>

			<!-- loop comes here		end -->
			
			
			<tr style="height:30px;">
				<!--<td colspan="4" align="right" style="font-weight:bold; 
				font-size:15px; color:#000000; padding-right:15px;">Zeb fortunes
				</td>
				<td align="center"></td>-->
			</tr>
        </table>
    </td>
</tr>





<tr style="line-height:100px;">
<td colspan="2"></td>
</tr>

<tr align="center" style="color:#000000;font-weight:bold; font-size:16px;">
	<!--<td colspan="2"><span style="font-size:12px;font-weight:normal;"><strong>
	Zeb Fourtunes Pvt Ltd.</strong></span>
	</td>-->
</tr>

<tr>
	<td colspan="2" align="center" style="font-size:11px;"><br><strong>
	</strong>
	</td>
</tr>
<tr>
	<td colspan="2" style="border-bottom:#000000 3px solid;"></td>
</tr>

</table>

</body>
</html>