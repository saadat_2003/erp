@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
<?php
$user = Auth::user();
?>
<!-- some CSS styling changes and overrides -->
<style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
<div id="PrintArea">
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">My Profile</h3>
              <span class="pull-right">
			  @if($user->user_notify === null)
				@can('storeuserNotification')
				<button type="button" name="userNotification" id="userNotification" class="userNotification btn btn-info" userNotification_id="{{ $user['id'] }}"><span class="fa fa-plus"></span> Add Notification</button>
				@endcan
				
			  @else 
				@can('edituserNotification')
				<button type="button" name="edit_userNotification" id="edit_userNotification" class="edit_userNotification btn btn-primary" userNotification_id="{{ $user['id'] }}"><span class="fa fa-edit"></span> Edit Notification</button>	
				@endcan
			  @endif
			  
	  
			@if($staffaddresscode_present === null)
				<!--dont show button -->
			@else
				@if($staffaddresscode_present['verified'] == 0)
					<button type="button" name="verify_present_address_btn" id="verify_present_address_btn" class="verify_present_address_btn btn btn-primary" user_id="{{ $user['id'] }}" ><span class="fa fa-edit"></span> Verify Present Address</button>	
				@endif		
			@endif
	
			@if($staffaddresscode_permanent === null)
				<!--dont show button -->
			@else
				@if($staffaddresscode_permanent['verified'] == 0)
					<button type="button" name="verify_permanent_address_btn" id="verify_permanent_address_btn" class="verify_permanent_address_btn btn btn-primary" user_id="{{ $user['id'] }}" ><span class="fa fa-edit"></span> Verify Permanent Address</button>	
				@endif				
			@endif				
              </span>			  
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
            <div class="row">
              <div class="col-md-4 text-center">
                  <div class="kv-avatar">
                          <img src="{{ asset('img/staff/'.$user->avatar) }}" width="90%">
                  </div>
              </div> 
              <div class="col-md-8">
              <table class="table table-striped">
                <tr>
                    <td><b>First Name</b></td>
                    <td>{{$user->fname}}</td>
                </tr>
                <tr>
                    <td><b>Last Name</b></td>
                    <td>{{$user->lname}}</td>
                </tr>
                <tr>
                    <td><b>Email</b></td>
                    <td>{{$user->email}}</td>
                </tr>
                <tr>
                    <td><b>Blood Group</b></td>
                    <td>{{$user->staffdetails->bloodgroup}}</td>
                </tr>
                <tr>
                    <td><b>Date of Birth</b></td>
                    <td>{{$user->staffdetails->dob->format('Y-m-d')}}</td>
                </tr>
                <tr>
                    <td><b>CNIC</b></td>
                    <td>{{$user->staffdetails->cnic}}</td>
                </tr>
                <tr>
                    <td><b>Status</b></td>
                    <td>
                        @if ($user->status === 1)
                          <span class="text-green"><b>Active</b></span>
                        @else
                            <span class="text-red"><b>Deactive</b></span>
                        @endif
                    </td>
                </tr>
                
              </table>
                  

              </div>
              </div>

          </div>

</div>



<!-- address row begins -->
<div class="row">
    <div class="col-md-6">
    <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Present Address</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                          <tr>
                            <td>Street Address 1</td>
                            <td>{{ $user->staffdetails->cstreetaddress }}</td>
                          </tr>
                          <tr>
                              <td>Street Address 2</td>
                              <td>{{ $user->staffdetails->cstreetaddress2 }}</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>{{ $user->staffdetails->ccity }}</td>
                              </tr>
                        </table>
                    </div>
                </div>
            </div>
    </div>
    </div>
  
    
    <div class="col-md-6">
            <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Permanent Address</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <tr>
                                  <td>Street Address 1</td>
                                  <td>{{ $user->staffdetails->pstreetaddress }}</td>
                                </tr>
                                <tr>
                                    <td>Street Address 2</td>
                                    <td>{{ $user->staffdetails->pstreetaddress2 }}</td>
                                  </tr>
                                  <tr>
                                      <td>City</td>
                                      <td>{{ $user->staffdetails->pcity }}</td>
                                    </tr>
                              </table>           
                        </div>
                        </div>
                      </div>
            </div>
            </div>
        </div>
<!-- address now end-->

<!-- Gaurdian Info row begins -->
<div class="row">
        <div class="col-md-6">
        <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Gaurdian Info</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">

                            <table class="table table-striped">
                                <tr>
                                  <td>Father/Gaurdian Name</td>
                                  <td>{{ $user->staffdetails->gaurdianname }}</td>
                                </tr>
                                <tr>
                                    <td>Relationship</td>
                                    <td>{{ $user->staffdetails->gaurdianrelation }}</td>
                                  </tr>
                                  <tr>
                                      <td>Contact Number</td>
                                      <td>{{ decrypt($user->staffdetails->gaurdiancontact) }}</td>
                                    </tr>
                              </table>
                        </div>
                    </div>
                </div>
        
        </div>
        </div>
        
        <div class="col-md-6">
                <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Personal Contact Info</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <tr>
                                      <td>Landline Number</td>
                                      <td>{{ decrypt($user->staffdetails->landline) }}</td>
                                    </tr>
                                    <tr>
                                      <td>Mobile Number</td>
                                      <td>{{ decrypt($user->staffdetails->phonenumber) }}</td>
                                    </tr>
                                  </table>
                            </div>
                            </div>
                
                        </div>
                
                </div>
                </div>
            </div>
<!-- Gaurdian Info now end-->

<!-- Regarding User row begins -->
<div class="row">
    <div class="col-md-6">
    <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">User Department & Role</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tr>
                              <td>Deparment</td>
                              <td>{{$user->department->deptname}}</td>
                            </tr>
                            <tr>
                                <td>Designation</td>
                                <td>{{$user->designation->name}}</td>
                              </tr>
                              <tr>
                                  <td>User Role</td>
                                  <td>{{$user->role->role_title}}</td>
                                </tr>
                          </table>
                    </div>
                </div>
            </div>
    </div>
    </div>
    
    <div class="col-md-6">
            <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Account Info</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <tr>
                                  <td>Email</td>
                                  <td>{{ $user->email }}</td>
                                </tr> 
                                <tr>
                                  <td>Status</td>
                                  <td>
                                      @if ($user->status === 1)
                                        <span class="text-green"><b>Active</b></span>
                                      @else
                                        <span class="text-red"><b>Deactive</b></span>
                                      @endif  
                                  </td>
                                </tr>
                              </table>
                        </div>
                        </div>
                    </div>
            
            </div>
            </div>
        </div>
<!-- Regarding User now end-->

<!-- Other Info & Settings begins -->
<div class="row">
        <div class="col-md-12">
        <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Settings & Other Info</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        
                        <div class="col-md-12">

                            <table class="table table-striped">
                                <tr>
                                  <td>Passport No. </td>
                                  <td>{{ $user->staffdetails->passportno }}</td>
                                </tr>
                                <tr>
                                  <td>Shift</td>
                                  <td>{{ $user->staffdetails->shift }}</td>
                                </tr>
                                <tr>
                                  <td>Timing</td>
                                  <td>{{ $user->staffdetails->starttime }} - {{ $user->staffdetails->endtime }}</td>
                                </tr>
                                <tr>
                                  <td>Extension No.</td>
                                  <td>{{$user->staffdetails->extension}}</td>
                                </tr>
                                <tr>
                                  <td>Joining Date</td>
                                  <td>{{$user->staffdetails->joiningdate->format('d-M-Y')}}</td>
                                </tr>
                              </table>                            
    
                        </div>
                    </div>
        
                </div>
        </div>
        </div>
        
</div>
<!-- Other Info & Settings end-->
</div>

<!-- Attendance Logs begins -->
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Attendance</h3>
        <div class="box-tools pull-right">
            <input class="custom-input" type="month" name="srchmonth" id="srchmonth" autocomplete="off"  min="2019-01" max="{{date('Y-m')}}"  value="{{date('Y-m')}}" />
            <button class="btn btn-success" id="filterDept"><li class="fa fa-search"></li></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="">
            @if(count($attlog) > 0)
                <table id="attlogs" class="display responsive wrap" style="width:100%;">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Day</th>
                    <th>Check-In</th>
                    <th>Check-Out</th>
                    <th>Check-Out Marked</th>
                    <th>Tardies</th>
                    <th>Short Leave</th>
                    <th>Hours Worked</th>
                    <th>Remarks</th>
                    <th>Status</th>
					<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $tardies=0;
                  $absents=0;
                  $shortleaves=0;
                  $upleaves=0;
                  $deducteddays=0;
                  $unpaiddays=0;
                  $totaldays=0;
                ?>
                @foreach($attlog as $att)
                    <?php
                        if($att->dayname=='Sun'){
                            $class="bg-green";
                          if($att->status=='-'){  
                            $unpaiddays++;
                          }
                        }elseif($att->status=='Holiday'){
                            $class="bg-green";
                        }elseif($att->status=='X'){
                            $class="bg-red";
                            $absents++;
                        }elseif($att->status=='UL'){
                          $class="";
                          $upleaves++;
                        }elseif($att->status=='-'){
                          $class="";
                          $unpaiddays++;
                        }else{
                            $class="";
                        }
                        $totaldays++;
                        $tardies+=$att->tardies;
                        $shortleaves+=$att->shortleaves;
                    ?>
                    <tr class="{{$class}}">
                    <td>{{$att->dated->format('d-m-Y')}}</td>
                    <td>{{$att->dayname}}</td>
                    <td>{{$att->checkin}}</td>
                    <td>{{$att->checkout}}</td>
                    <td>{{$att->checkoutfound}}</td>
                    <td>{{$att->tardies}}</td>
                    <td>{{$att->shortleaves}}</td>
                    <td>{{$att->workedhours}}</td>
                    <td>{{$att->remarks}}</td>
                    <td>{{$att->status}}</td>
                    <td>
					<?php
						//Show attComplaint button on last 5 days and NOT BEFORE THAT
						$date=strtotime(date('Y-m-d'));
						$att_date = strtotime($att->dated);
						$att->dated;
						//Adding 2 ($attComplaintDaysLimit+2) to balance -2 subtracted in $last_2_days
						$subtracted_date = date("Y-m-d", strtotime("-".($attComplaintDaysLimit+2)." days"));
						$subtracted_date = strtotime($subtracted_date);
						
						//last 2 days
						$last_2_days = date("Y-m-d", strtotime("-2 days"));
						$last_2_days = strtotime($last_2_days);
						
					?>					
					<?php if($att_date<=$subtracted_date) { 
						//do nothing ?>		
					<?php } else if($att_date>$subtracted_date){  ?>

										<?php  if($att_date>$last_2_days){ //show nothing on last 2 days //echo "show no";?>					
										
										
										<?php } else if($att_date>$subtracted_date){  ?>
										<button type="button" name="complaint" att_id="{{$att->id}}" att_date="{{ $att->dated->format('Y-m-d') }}" class="complaint btn btn-primary" title="Complaint"><i class="fa fa-comments"></i></button>		

										 
										
										<?php }
					}else{  ?>

					<?php } ?>									
						&nbsp;
						@if(!empty($att->complaint_of_att['description']))						
						<a href="{!! url('/complaint/show/'.$att->complaint_of_att['id']); !!}" class="btn btn-warning" data-toggle="tooltip" title="{{$att->complaint_of_att['description']}}"><li class="fa fa-warning"></li></a>	
						@else
						@endif
                    </td>
					</tr>
                    @endforeach			  
                    <?php
                      //Tardy conversion to deducted days
                      $deducteddays+=intdiv($tardies,$settings['tardydaydeduct']);
                      //Short leaves conversion to deducted days
                      $deducteddays+=intdiv($shortleaves,$settings['shortleavedaydeduct']);
                      //Absents + Unpaid Leave + Unpaid days
                      $deducteddays+=$absents+$upleaves+$unpaiddays;
                    ?>
                </tbody>
                <tfoot>
                </tfoot>
                </table>				
				
        <!-- Salary data widgets begins -->
        <div class="clearfix"><br></div>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Working Days</span>
                <span class="info-box-number" style="font-size: 20px;" >{{$totaldays}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">Late Comming</span>
                  <span class="info-box-number">{{$tardies}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Short Leaves</span>
                <span class="info-box-number">{{$shortleaves}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>
  
              <div class="info-box-content">
                  <span class="info-box-text">Unpaid Days</span>
                  <span class="info-box-number">{{$deducteddays}}</span>          
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
              <span class="info-box-icon"><i class="fa fa-money"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Basic Salary</span>
                <span class="info-box-number">{{$user->staffdetails->salary}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-money"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Recurring Commission</span>
                <span class="info-box-number">{{$salaries['rec_comm']}}</span>               
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-money"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Ref Commission</span>
              <span class="info-box-number">{{$salaries['ref_comm']}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-money"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Demo Commission</span>
                <span class="info-box-number">{{$salaries['demo_comm']}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          
        </div>
        
        <!-- Salary data widgets ends -->

                @else
                <div>No Record found.</div>
                @endif

    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix" style="">
    </div>
    <!-- /.box-footer -->
</div>
<!-- Attendance Logs ends -->

<!-- Adjustments begins -->
<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Adjustments</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="">
              @if(count($adjustments) > 0)
                <table id="loginlogs" class="display responsive wrap" style="width:100%;">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Created At</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($adjustments as $adjustment)
                    <tr>
                      <td>{{$adjustment->dated->format('d-M-Y')}}</td>
                      <td>
                          @if($adjustment->type==1)
                            Addition
                          @else
                            Deduction
                          @endif
                      </td>
                      <td>{{$adjustment->description}}</td>
                      <td>{{$adjustment->amount}}</td>
                      <td>{{$adjustment->status}}</td>
                      <td>{{$adjustment->createdby->fname}} {{$adjustment->createdby->lname}}</td>
                      <td>{{$adjustment->created_at->format('d-M-Y')}}</td>
                    </tr>
                    @endforeach			  
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
                @else
                <div>No Record found.</div>
                @endif
  
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix" style="">
    </div>
    <!-- /.box-footer -->
  </div>
  <!-- Adjustments ends -->



 <!-- Edit Modal Begins -->   
 <div class="modal fade" id="modal-default-edit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Attendance</h4>
        </div>
        <div class="modal-body">
          <form role="form" action="{{route('attendancesheet.update')}}" method="POST" id="frmEdit">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <input name="id" id="editid" type="hidden" value="0">
            <div class="box-body">
              <div class="form-group">
                <label for="dated">Date</label>
                <input type="text" class="form-control" id="dated" name="dated"  autocomplete="off" readonly>
              </div>
              <div class="form-group">
                <label for="checkin">Check In</label>
                <input type="text" class="form-control" id="checkin" name="checkin" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label for="checkout">Check Out</label>
                <input type="text" class="form-control" id="checkout" name="checkout" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label for="checkoutfound">Check Out Marked</label>
                <select class="form-control" id="checkoutfound" name="checkoutfound">
                  <option value="Yes" selected>Yes</option>
                  <option value="No">No</option>
                </select>
              </div>
              <div class="form-group">
                <label for="shortleaves">Short Leave</label>
                <input type="number" class="form-control" id="shortleaves" name="shortleaves" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label for="tardies">Tardy</label>
                <input type="number" class="form-control" id="tardies" name="tardies" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label for="workedhours">Worked Hours</label>
                <input type="number" class="form-control" id="workedhours" name="workedhours" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label for="remarks">Remarks</label>
                <input type="text" class="form-control" id="remarks" name="remarks" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label for="status">Select Status</label>
                <select class="form-control" id="status" name="status">
                  <option value="P" selected>Present</option>
                  <option value="SL">Sick Leave</option>
                  <option value="CL">Causal Leave</option>
                  <option value="UL">Unpaid Leave</option>
                  <option value="X">Absent</option>
                </select>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <span class="pull-right"><button type="submit" class="btn btn-primary">Submit</button></span>
            </div>
          </form>
        </div>
        
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- Edit Modal Ends -->  

  
  
  
  
  
  
  
<!-- Modal popup-Complaints -->
<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Complaints</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf

           <div class="form-group">
            <label class="control-label col-md-4">Description : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="description" name="description" placeholder="Enter description" autocomplete="off" required>
            </div>
           </div>

		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">
				<input type="hidden" name="action" id="action" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				<input type="hidden" id='att_id' name='att_id' />
				<input type="hidden" id='att_date' name='att_date' />
				<input type="submit" name="action_button" id="action_button" class="btn btn-info" value="Add" />				
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>
<!-- Modal popup-Complaints -->  



<!-- Modal popup-Notifications -->
<div id="formModal_userNotification" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Notifications</h4>
        </div>
        <div class="modal-body">
         <span id="form_result_userNotification"></span>
         <form method="post" id="sample_form_userNotification" class="form-horizontal" enctype="multipart/form-data">
          @csrf
           <div class="form-group">
            <label class="control-label col-md-2" > </label>
            <div class="col-md-8">
                    <!--<span class="button-checkbox">
                    <button type="button" class="btn btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i>&nbsp;Database</button>  --> 
                    <input type="checkbox" class="" id="mail_chk" name="mail_chk" value="1">
					<label>Mail Notification</label>
                    <!--</span>-->  
            </div>
           </div>		  
           <div class="form-group">
            <label class="control-label col-md-2" > </label>
            <div class="col-md-8">
                    <!--<span class="button-checkbox">
                    <button type="button" class="btn btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i>&nbsp;Database</button>-->
                    <input type="checkbox" class="" id="database_chk" name="database_chk" value="1">
					<label>ERP Notification</label>
                    <!--</span>-->
            </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-2"> </label>
            <div class="col-md-8">
                    <!--<span class="button-checkbox">
                    <button type="button" class="btn btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i>&nbsp;Broadcast</button>-->
                    <input type="checkbox" class="" id="broadcast_chk" name="broadcast_chk" value="1">
					<label>Push Notification</label>
                    <!--</span>-->			
            </div>
           </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">

				<input type="hidden" name="action_userNotification" id="action_userNotification" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				
				<input type="hidden" id='notification_of_user' name='notification_of_user' />
				<input type="submit" name="action_button_userNotification" id="action_button_userNotification" class="btn btn-info" value="Update" />					
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>
<!-- Modal popup-Notifications -->  

<!-- Modal popup-Verify Address -->
<div id="formModal_verifyaddress_present" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Verify Present Address</h4>
        </div>
        <div class="modal-body">
         <span id="form_result_verifyaddress_present"></span>
         <form method="post" id="sample_form_verifyaddress_present" class="form-horizontal" enctype="multipart/form-data">
          @csrf

           <div class="form-group">
            <label class="control-label col-md-4">Present Address Code : </label>
            <div class="col-md-8">
             <input type="number" class="form-control" id="present_address_code" name="present_address_code" placeholder="Enter code" autocomplete="off" required>
            </div>
           </div>	   

		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">
				<input type="hidden" name="action_verifyaddress_present" id="action_verifyaddress_present" />
				<input type="hidden" name="hidden_id_present" id="hidden_id_present" value="{{ $staffaddresscode_present['id'] }}" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				<input type="hidden" id='user_id' name='user_id' />
				<input type="hidden" id='type' name='type' value="Present" />
				<input type="submit" name="action_button_verifyaddress_present" id="action_button_verifyaddress_present" class="btn btn-info" value="Verify Present Code" />				
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>
<!-- Modal popup-Verify Address -->

<!-- Modal popup-Verify Address -->
<div id="formModal_verifyaddress_permanent" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Verify Permanent Address</h4>
        </div>
        <div class="modal-body">
         <span id="form_result_verifyaddress_permanent"></span>
         <form method="post" id="sample_form_verifyaddress_permanent" class="form-horizontal" enctype="multipart/form-data">
          @csrf
		   
           <div class="form-group">
            <label class="control-label col-md-4">Permanent Address Code : </label>
            <div class="col-md-8">
             <input type="number" class="form-control" id="permanent_address_code" name="permanent_address_code" placeholder="Enter code" autocomplete="off" required>
            </div>
           </div>		   

		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">
				<input type="hidden" name="action_verifyaddress_permanent" id="action_verifyaddress_permanent" />
				<input type="hidden" name="hidden_id_permanent" id="hidden_id_permanent" value="{{ $staffaddresscode_permanent['id'] }}" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				<input type="hidden" id='user_id' name='user_id' />
				<input type="hidden" id='type' name='type' value="Permanent" />
				<input type="submit" name="action_button_verifyaddress_permanent" id="action_button_verifyaddress_permanent" class="btn btn-info" value="Verify Permanent Code" />				
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>
<!-- Modal popup-Verify Address -->
  
<div class="loading">
    <div class="loader"></div>
</div>

@endsection

@push('scripts')
<style>
    .loading{
        display: hidden;
        position: fixed;
        left: 0;
        top: 0;
        padding-top: 45vh;
        padding-left:100vh;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: gray;
        opacity: 0.8;
    }
    .loader {
      border: 16px solid #f3f3f3; /* Light grey */
      border-top: 16px solid #3498db; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: spin 2s linear infinite;
    }
    
    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
    
    </style>

<script>
$(document).ready(function () {
  $(".loading").fadeOut();
  //Attendance Filter begins
  $('#filterDept').click( function() {
    var url;
    if($('#srchmonth').val()!=""){
      url="{{url('profile')}}/?srchmonth="+$('#srchmonth').val();
    }else{
      url ="{{url('profile')}}";
    }
    window.location.href =url;
  });
  
  
  
	//Complaints
	 $(document).on('click', '.complaint', function(){
	  $('.modal-title').text("Add complaint");
		 $('#action_button').val("Add");
		 $('#action').val("Add");
	//added by me
		$('#description').val('');
		$('#form_result').html("");

		var att_id = $(this).attr('att_id');
		var att_date = $(this).attr('att_date');
		
		//alert(id);
		document.getElementById("att_id").value = att_id;
		document.getElementById("att_date").value = att_date;
		
		event.preventDefault();	
		 
		 $('#formModal').modal('show');
		 
	 });
  
 $('#sample_form').on('submit', function(event){
  event.preventDefault();
  if($('#action').val() == 'Add')
  {
   $.ajax({
    url:"{{ route('attComplaint') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    //dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
/*       for(var count = 0; count < data.errors.length; count++)
      {
       html += '<p>' + data.errors[count] + '</p>';
      } */
	  html += '<p>' + data.errors + '</p>';
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      //$('#attlogs').DataTable().ajax.reload();
	  swal("Success!", "Complaint Added!", "success");
     }
     $('#form_result').html(html);
	 $("#attlogs").load(" #attlogs");
    }
   })
  }
 });  
  
  
  
  
//userNotification
 $('#userNotification').click(function(){
  $('.modal-title').text("Add Notification");
     $('#action_button_userNotification').val("Add Notification");
     $('#action_userNotification').val("Add Notification");
//added by me
    //$('#mail_chk').val('');
	//$('#database_chk').val('');
	//$('#broadcast_chk').val('');
	document.getElementById("mail_chk").checked = false;
	document.getElementById("database_chk").checked = false;
	document.getElementById("broadcast_chk").checked = false;
	
    $('#form_result_userNotification').html("");
    var notification_of_user = $(this).attr('userNotification_id');
    document.getElementById("notification_of_user").value = notification_of_user;	
	 
     $('#formModal_userNotification').modal('show');
	 
 }); 
  
//userNotification store
 $('#sample_form_userNotification').on('submit', function(event){
  event.preventDefault();	 
  if($('#action_userNotification').val() == 'Add Notification')
  {
   $.ajax({
    url:"{{ route('storeuserNotification') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
       html += '<p>' + data.errors + '</p>';
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form_userNotification')[0].reset();
	  swal("Success!", "Notifications Added !", "success");
     }
     $('#form_result_userNotification').html(html);
    }
   })
  }


  if($('#action_userNotification').val() == "Edit Notification")
  {
   $.ajax({
    url:"{{ route('updateuserNotification') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
	  swal("Failed", data.errors + "\r\nERROR.", "error");
      html = '<div class="alert alert-danger">';
      //for(var count = 0; count < data.errors.length; count++)
      //{
       html += '<p>' + data.errors + '</p>';
      //}
      html += '</div>';
     }
     if(data.success)
     {
	  swal("Success", "Notifications Updated.", "success");
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form_userNotification')[0].reset();
     }
     $('#form_result_userNotification').html(html);
	 $('#formModal_userNotification').modal('hide');
    }
   });
  }  
  
 });
  
  
  
 $(document).on('click', '.edit_userNotification', function(){
  var id = $(this).attr('userNotification_id');
  document.getElementById("notification_of_user").value = id;
  //alert(id);
  $('#form_result_userNotification').html('');
  $.ajax({
   url:"/edituserNotification/"+id,
   dataType:"json",
   success:function(html){
	   
	//checking values of 0 & 1 from database to check/uncheck the box			********************************
	if(html.data.mail_chk==1){
		document.getElementById("mail_chk").checked = true;
	}else{
		document.getElementById("mail_chk").checked = false;
	}
	
	if(html.data.database_chk==1){
		document.getElementById("database_chk").checked = true;
	}else{
		document.getElementById("database_chk").checked = false;
	}

	if(html.data.broadcast_chk==1){
		document.getElementById("broadcast_chk").checked = true;
	}else{
		document.getElementById("broadcast_chk").checked = false;
	}	
	//checking values of 0 & 1 from database to check/uncheck the box			********************************

	
	$('#hidden_id').val(html.data.id);
    $('.modal-title').text("Edit Notification");
    $('#action_button_userNotification').val("Edit Notification");
    $('#action_userNotification').val("Edit Notification");
    $('#formModal_userNotification').modal('show');
   }
  })
 });  



//Verify Address - Present
 $(document).on('click', '.verify_present_address_btn', function(){
  $('.modal-title').text("Verify Present Address");
     $('#action_button_verifyaddress_present').val("Verify Present Code");
     $('#action_verifyaddress_present').val("Verify Present Code");
//added by me
    $('#present_address_code').val('');
	
    $('#form_result_verifyaddress_present').html("");

    var id_of_user = $(this).attr('user_id');
    document.getElementById("user_id").value = id_of_user;
	
    event.preventDefault();	
	 
     $('#formModal_verifyaddress_present').modal('show');
	 
 }); 
 
//Verify Address - Permanent
 $(document).on('click', '.verify_permanent_address_btn', function(){
  $('.modal-title').text("Verify Permanent Address");
     $('#action_button_verifyaddress_permanent').val("Verify Permanent Code");
     $('#action_verifyaddress_permanent').val("Verify Permanent Code");
//added by me
	$('#permanent_address_code').val('');
	
    $('#form_result_verifyaddress_permanent').html("");

    var id_of_user = $(this).attr('user_id');
    document.getElementById("user_id").value = id_of_user;
	
    event.preventDefault();	
	 
     $('#formModal_verifyaddress_permanent').modal('show');
	 
 });  


 $('#sample_form_verifyaddress_present').on('submit', function(event){
  event.preventDefault();
//Verify Address
  if($('#action_verifyaddress_present').val() == 'Verify Present Code')
  {
   $.ajax({
    url:"{{ route('staffaddresscode.update') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
      //for(var count = 0; count < data.errors.length; count++)
      //{
       html += '<p>' + data.errors + '</p>';
      //}
      html += '</div>';
	  swal("Error!", data.errors, "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form_verifyaddress_present')[0].reset();
      //$('#user_table').DataTable().ajax.reload();
	  swal("Success!", "Present Address Verified !", "success");
     }
     $('#form_result_verifyaddress_present').html(html);
    }
   })
  }  
  
 }); 

 $('#sample_form_verifyaddress_permanent').on('submit', function(event){
  event.preventDefault();
//Verify Address
  if($('#action_verifyaddress_permanent').val() == 'Verify Permanent Code')
  {
   $.ajax({
    url:"{{ route('staffaddresscode.update') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
      //for(var count = 0; count < data.errors.length; count++)
      //{
       html += '<p>' + data.errors + '</p>';
      //}
      html += '</div>';
	  swal("Error!", data.errors, "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form_verifyaddress_permanent')[0].reset();
      //$('#user_table').DataTable().ajax.reload();
	  swal("Success!", "Present Address Verified !", "success");
     }
     $('#form_result_verifyaddress_permanent').html(html);
    }
   })
  }  
  
 }); 

 
 
});


</script>


<script>

$(document).ready(function (e) {
  //InitTable();
  //$('#table_data').wrap("<div id=\"scrooll_div\"></div>");
  //$('#scrooll_div').doubleScroll();

  $('[data-toggle="tooltip"]').tooltip();   





});



</script>





<script>
$(document).ready(function() { 
    $('.select2').select2({
        placeholder: "Select Staff",
        multiple: false,
    }); 
});
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
</script>


@endpush
