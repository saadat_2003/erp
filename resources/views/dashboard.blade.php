@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif
<?php
 $user = Auth::user();
?>

<!-- Counter Modal Begins -->
<div class="modal fade" id="CounterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Upcoming Terms and Conditions</h3>
      </div>
      <div class="modal-body">
        <div id="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
        <p>
        <span style="color:red; font-weight: 600px; font-size: 18px;">
        To continue with Us , you must agree to the terms and Conditions of YCC or YCS or else we will terminate your contract with the Company.</span>
        <ol style="font-weight: 600px; font-size: 18px;">
          <li>You need to have a functional Laptop/Desktop System.</li>
          <li>You need to have good Internet connection.</li>
          <li>You need to have electricity backup.</li>
          <li>You'll be full time online on Zoom as per your shift timing.</li>
          <li>You must have functional Headphone and Webcam.</li>
          <li>You must have a separate working space.</li>
          <li>No background noise or any other type of distortion.</li>
        </ol>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Counter Modal Ends -->



<!-- DATE FILTER -->
<div class="row">
        <div class="col-xs-12">
		<form class="form-horizontal" action="{!! url('dashboard/'); !!}" method="post" enctype="multipart/form-data">
          @csrf
        
         <div class="form-group">
				<div class="col-md-3 col-sm-6 col-xs-12">
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
				</div>				
				
				
				<div class="col-md-12 col-sm-12 col-xs-12">
            <div class="btn-group">
              <button type="button" class="btn btn-success">Join Room</button>
              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                @foreach($meetinglinks as $meetinglink)
                  <li><a href="{{ $meetinglink->meetinglink }}" target="_blank">{{ $meetinglink->deptname }} ({{ $meetinglink->starttime }} {{ $meetinglink->endtime }})</a></li>
                @endforeach
              </ul>
            </div>&nbsp

        				<span class="pull-right">		
                
                  <div class="input-group">
                 @if($settings['manualatt']==1 && $user->staffdetails->showinsalary==1)
                    @can('manualatt')
                      @if($checkoutmissing==0)
                        <a href="javascript:void(0)" class="btn btn-success btn-flat manuallatt checkin" data-action="1" title="Check In"><li class="fa fa-clock-o"></li> Check-In</a> &nbsp
                        <!-- <a href="javascript:void(0)" class="btn btn-success btn-flat manuallattnew checkinnew" data-action="1" title="Check In"><li class="fa fa-clock-o"></li> Check-In New</a> &nbsp -->
                      @else
                        <a href="javascript:void(0)" class="btn btn-danger btn-flat manuallatt checkout"  data-action="2" title="Check Out"><li class="fa fa-clock-o"></li> Check-Out</a> &nbsp
                        <!-- <a href="javascript:void(0)" class="btn btn-danger btn-flat manuallattnew checkoutnew"  data-action="2" title="Check Out"><li class="fa fa-clock-o"></li> Check-Out New</a> &nbsp -->
                      @endif

                    @endcan
                  @endif
                  
                  <a class="btn btn-primary"  href="https://drive.google.com/open?id=109dQ0H9lDUyxCazroFoBKlpvuRRTKPfd" target="_blank">Zoom Backgrounds</a> &nbsp 
                    <button type="button" class="btn btn-default" id="daterange-btn">
                      <span>{{date('F d, Y')}} - {{date('F d, Y')}}</span>
                      <input type="hidden" name="dateFrom" id="dateFrom" value="{{date('Y-m-d')}}">
                      <input type="hidden" name="dateTo" id="dateTo" value="{{date('Y-m-d')}}">
                      <i class="fa fa-caret-down"></i>
                    </button>
					<button class="btn btn-success" id="filterDept"><li class="fa fa-search"></li></button>
                  </div>
				</span>
				</div>
			  </div>
              <script>
                
                 $(document).ready(function() { 

                 
                  //Date range as a button
                  $('#daterange-btn').daterangepicker(
                    {
                      ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                      },
                      startDate: moment().subtract(29, 'days'),
                      endDate  : moment()
                    },
                     function (start, end) {
                       $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                       $('#dateFrom').val(start.format('YYYY-MM-DD'));
                       $('#dateTo').val(end.format('YYYY-MM-DD'));
                       var maintabledate = $('#table_data').DataTable();
                       maintabledate.column('6').search(
                        $('#dateFrom').val()+','+$('#dateTo').val()
                        ).draw();
                     }
                   );
                   });
                   $('#daterange-btn span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().endOf('month').format('MMMM D, YYYY'));
                


              </script>
		</form>
        </div>
        <!-- /.col -->
</div>



@if(count($myappointments) > 0 && auth()->user()->isGoOnAppoints==1)
<div class="row">
<!-- My Appointments -->
<div class="col-md-12">
    <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">My Appointments</h3>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                @foreach($myappointments as $appointment)
                <li class="item">
                <div class="product-img info-box-icon bg-red" style="height: 60px; width: 60px; font-size: 40px; line-height: 60px; margin:5px;">
                    <i class="fa fa-calendar"></i>
                </div>
                  <div class="product-info">
                  
                    <a href="{!! url('/leads/'.$appointment->lead->id ); !!}" class="product-title">{{$appointment->appointtime->format('d-M-Y h:i:s')}} with {{$appointment->lead->businessName}}</a>
                    <span class="product-description">
                        {{$appointment->note}}<br>
                        @foreach($appointment->users as $staff)
                          {{ $loop->first ? '' : ' ' }}
                          <span class="btn bg-blue btn-xs"><small>{{$staff->fname}} {{$staff->lname}}</small></span>
                        @endforeach
                        <br>
                        By: {{$appointment->createdby->fname}} {{$appointment->createdby->lname}}
                        </span>
                  </div>
                </li>
                @endforeach
                <!-- /.item -->                
              </ul>
            </div>
            <!-- /.box-body -->
           </div>

    </div>
</div>
@endif


@can('stats-number')
<!-- Statistics begins-->
<div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-object-group"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Projects</span>
              <span class="info-box-number">{{$statistics_count['projects']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-hourglass-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Leads</span>
              <span class="info-box-number">{{$statistics_count['leads']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Appointments</span>
              <span class="info-box-number">{{$statistics_count['appointments']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-file-audio-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Recordings</span>
              <span class="info-box-number">{{$statistics_count['recordings']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
</div>
<!-- Statistics ends -->
@endcan


@can('stats-hr')
<!-- HR Staff Statistics begins-->
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <h3>Staff Stats</h3>
</div>
          <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-group"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Active Staff</span>
              <span class="info-box-number">{{$hrstats['activestaff']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-check-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Joined This Month</span>
              <span class="info-box-number">{{$hrstats['joined']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-remove"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Left This Month</span>
              <span class="info-box-number">{{$hrstats['left']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
</div>
<!-- HR Staff Statistics ends -->
@endcan

@can('stats-hr-requests')
<!-- HR Hiring Request Statistics begins-->
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <h3>HR Requests Stats</h3>
</div>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-blue">
      <div class="inner">
        <h3>{{$hrstats['totalrequests']}}</h3>

        <p>Requests This Month</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-stalker"></i>
      </div>
    </div>
  </div>
<!-- /.col -->

<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-green">
    <div class="inner">
      <h3>{{$hrstats['completedrequest']}}</h3>

      <p>Completed/Fulfilled This Month</p>
    </div>
    <div class="icon">
      <i class="ion ion-paper-airplane"></i>
    </div>
  </div>
</div>
<!-- /.col -->
<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-yellow">
    <div class="inner">
      <h3>{{$hrstats['inprocessdrequest']}}</h3>

      <p>Inprocess This Month</p>
    </div>
    <div class="icon">
      <i class="ion ion-edit"></i>
    </div>
  </div>
</div>
<!-- /.col -->

<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-red">
    <div class="inner">
      <h3>{{$hrstats['pendingrequest']}}</h3>

      <p>Pending This Month</p>
    </div>
    <div class="icon">
      <i class="ion ion-clipboard"></i>
    </div>
  </div>
</div>
<!-- /.col -->


</div>
<!-- HR Hiring Request Statistics ends-->
@endcan



@can('stats-hr-leads')
<!-- HR Hiring Leads Statistics begins-->
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <h3>HR Leads Stats</h3>
</div>
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="ion ion-clipboard"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Leads this month</span>
              <span class="info-box-number">{{$hrstats['leadsthismonth']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->		
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-check-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">New Leads</span>
              <span class="info-box-number">{{$hrstats['leads_new']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-hand-stop-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Rejected</span>
              <span class="info-box-number">{{$hrstats['leads_rejected']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->		
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-headphones"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Not Picking Call</span>
              <span class="info-box-number">{{$hrstats['leads_not_picking_call']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->		

<!-- NEXT ROW -->			
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-info-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Incorrect Information</span>
              <span class="info-box-number">{{$hrstats['leads_incorrect_information']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->	

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-phone"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Call made</span>
              <span class="info-box-number">{{$hrstats['leads_call_made']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->	
		
			
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Interview scheduled</span>
              <span class="info-box-number">{{$hrstats['leads_interview_scheduled']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->		

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-user-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Not Appeared</span>
              <span class="info-box-number">{{$hrstats['leads_not_appeared']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->	

<!-- NEXT ROW -->		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-step-forward"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Rescheduled</span>
              <span class="info-box-number">{{$hrstats['leads_rescheduled']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->			

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Appeared</span>
              <span class="info-box-number">{{$hrstats['leads_appeared']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-repeat"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Second Interview</span>
              <span class="info-box-number">{{$hrstats['leads_second_interview']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->		
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-beige"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Short Listed</span>
              <span class="info-box-number">{{$hrstats['leads_short_listed']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->	

		
<!-- NEXT ROW -->			
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-odnoklassniki"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Selected</span>
              <span class="info-box-number">{{$hrstats['leads_selected']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->			
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-user-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Not Joined</span>
              <span class="info-box-number">{{$hrstats['leads_not_joined']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->				
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check-square"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Candidate Joined</span>
              <span class="info-box-number">{{$hrstats['leads_joined']}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->	

	
</div>
<!-- HR Hiring Leads Statistics ends-->
@endcan

  <div class="row">
    @can('lead-chart-10')
    <!-- Lead Chart Begins-->
    <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Last 10 days leads </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="lineChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </div>
    <!-- Lead Chart Ends -->
    @endcan
    @can('appointment-chart-10')
    <!-- Appointments Chart Begins-->
    <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Last 10 days Appointments </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="lineChart2" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </div>
    <!-- Appointments Chart Ends -->
    @endcan
    @can('today-appointments')
    <!-- Today Appointments -->
    <div class="col-md-12">
    <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Today Appointments</h3>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(count($recentappointments) > 0)
              <ul class="products-list product-list-in-box">
                @foreach($recentappointments as $appointment)
                <li class="item">
                <div class="product-img info-box-icon bg-green" style="height: 60px; width: 60px; font-size: 40px; line-height: 60px; margin:5px;">
                    <i class="fa fa-calendar"></i>
                </div>
                  <div class="product-info">
                  
                    <a href="{!! url('/leads/'.$appointment->lead->id ); !!}" class="product-title">{{$appointment->appointtime->format('d-M-Y h:i:s')}} with {{$appointment->lead->businessName}}</a>
                    <span class="product-description">
                        {{$appointment->note}}<br>
                        @foreach($appointment->users as $staff)
                          {{ $loop->first ? '' : ' ' }}
                          <span class="btn bg-blue btn-xs"><small>{{$staff->fname}} {{$staff->lname}}</small></span>
                        @endforeach
                        <br>
                        By: {{$appointment->createdby->fname}} {{$appointment->createdby->lname}}
                        </span>
                  </div>
                </li>
                @endforeach
                <!-- /.item -->                
              </ul>
              @else
                <p>No Record(s) found.</p>
              @endif

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
            <a href="{!! url('/appointments/'); !!}" class="uppercase">View All Appointments</a>
            </div>
            <!-- /.box-footer -->
          </div>

    </div>
    @endcan
    @can('latest-appointments')
        <!-- Appointments -->
        <div class="col-md-12">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Appointments of the Month</h3>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(count($appointments) > 0)
              <ul class="products-list product-list-in-box">
                @foreach($appointments as $appointment)
                <li class="item">
                <div class="product-img info-box-icon bg-aqua" style="height: 60px; width: 60px; font-size: 40px; line-height: 60px; margin:5px;">
                    <i class="fa fa-calendar"></i>
                </div>
                  <div class="product-info">
                    <a href="{!! url('/leads/'.$appointment->lead->id ); !!}" class="product-title">{{$appointment->appointtime->format('d-M-Y h:i:s')}} with {{$appointment->lead->businessName}}</a>
                    <span class="product-description">
                        {{$appointment->note}}<br>
                        @foreach($appointment->users as $staff)
                          {{ $loop->first ? '' : ' ' }}
                          <span class="btn bg-blue btn-xs"><small>{{$staff->fname}} {{$staff->lname}}</small></span>
                        @endforeach
                        <br>
                        By: {{$appointment->createdby->fname}} {{$appointment->createdby->lname}}
                        </span>
                  </div>
                </li>
                @endforeach
                <!-- /.item -->                
              </ul>
              @else
                <p>No Record(s) found.</p>
              @endif

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="{!! url('/appointments/'); !!}" class="uppercase">View All Appointments</a>
            </div>
            <!-- /.box-footer -->
          </div>

    </div>

    </div>
    @endcan
    <div class="row">
    @can('latest-leads')
      <!-- Lastest Leads -->
    <div class="col-md-12">
    <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Leads of the Month</h3>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(count($leads) > 0)
              <ul class="products-list product-list-in-box">
                @foreach($leads as $lead)
                <li class="item">
                <div class="product-img info-box-icon bg-red" style="height: 60px; width: 60px; font-size: 40px; line-height: 60px; margin:5px;">
                    <i class="fa fa-hourglass-o"></i>
                </div>
                  <div class="product-info">
                    <a href="{!! url('/leads/'.$lead->id ); !!}" class="product-title">{{$lead->businessName}} ({{$lead->user->fname}} {{$lead->user->lname}}) 
                    <span class="product-description">
                            {{$lead->businessAddress}}, 
                            {{$lead->businessNature}}<br>
                            By: {{$lead->createdby->fname}} {{$lead->createdby->lname}}
                        </span>
                  </div>
                </li>
                @endforeach
                <!-- /.item -->                
              </ul>
              @else
                <p>No Record(s) found.</p>
              @endif

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="{!! url('/leads/' ); !!}" class="uppercase">View All Leads</a>
            </div>
            <!-- /.box-footer -->
          </div>
    </div>
@endcan
@can('latest-recordings')
       <div class="col-md-12">
      <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Latest Recordings</h3>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="notificationtable" class="table responsive nowrap">
                <tbody><tr>
                  <th>Note</th>
                  <th>Recording</th>
                </tr>
                @foreach($recordings as $recording)
                <tr>
                <td>
                  <a href="{!! url('leads',$recording->lead_id); !!}">{{$recording->lead->businessName}}
                  ({{$recording->lead->user->fname}} {{$recording->lead->user->lname}})</a> @
                  <small> {{$recording->created_at->format('d-M-Y')}} </small><br>
                  {{$recording->title}}
                </td>
                  <td>
                  @if($recording->link)
										<audio controls>
											<source src="{{$recording->link=="" ? "" : "$recording->link"}}" type="audio/mpeg">
											Your browser does not support the audio element.
										</audio>
										@elseif($recording->recording_file && Storage::disk('local')->exists('public/leads_assets/recordings/'.$recording->recording_file))
                      @if(File::extension('public/leads_assets/recordings/'.$recording->recording_file)=="mp4")
                        <video controls style="width: 350px;">
                          <source src="{{Storage::disk('local')->url('public/leads_assets/recordings/'.$recording->recording_file)}}" type="audio/mpeg">
                          Your browser does not support the audio element.
                        </video>
                      @else
                        <audio controls>
                          <source src="{{Storage::disk('local')->url('public/leads_assets/recordings/'.$recording->recording_file)}}" type="audio/mpeg">
                          Your browser does not support the audio element.
                        </audio>
                      @endif
										@else
											NA
										@endif
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
            <a href="{!! url('/recordings/'); !!}" class="uppercase">View All Recordings</a>
            </div>
          </div>
          
      </div>
@endcan

    </div>


@can('pending-proposal')
<!-- Box Proposal Begins -->

<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Proposals</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body" style="">
      @if(count($proposals) > 0)
          <table id="nofeaturesproposal" class="display responsive wrap" style="width:100%;">
          <thead>
          <tr>
            <th>Title & Note</th>
            <th>Lead</th>
            <th>File</th>
            <th>Created by</th>
          </tr>
          </thead>
          <tbody>
          @foreach($proposals as $proposal)
            <tr>
            <td><b>{{$proposal->title}}</b> <small> @ {{$proposal->created_at->format('d-m-Y')}}</small><br>{{$proposal->note}}</td>
            <td><b><a href="{!! url('leads/'.$proposal->lead_id ); !!}">{{$proposal->lead->businessName}}</a></td>
            <td>
            @if($proposal->docfile)
              @if(Storage::disk('local')->exists('public/leads_assets/proposal/'.$proposal->docfile))
              <a href="{{Storage::disk('local')->url('public/leads_assets/proposal/'.$proposal->docfile)}}" target="_blank" class="btn btn-info"><li class="fa fa-file"></li> View</a>
              @else
                NA
              @endif
            @else
            <a href="{!! url('leads/uploadproposal/'.$proposal->lead_id.'/'.$proposal['id'].'' ); !!}" class="btn btn-danger" title="Upload Proposal File"><li class="fa fa-exclamation-triangle"></li></a>
            @endif
            </td>
            <td>{{$proposal->createdby->fname}} {{$proposal->createdby->lname}}</td>
            
            </tr>
            @endforeach			  
          </tbody>
          <tfoot>
          </tfoot>
          </table>
        @else
        <div>No Record found.</div>
        @endif		

  </div>
  <!-- /.box-body -->
</div>
<!-- Box Proposal ends -->
@endcan
@can('show-dashboard-calendar')
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">
          <div class="panel-heading">Schedule Appointments</div>

          <div class="panel-body">
              {!! $calendar->calendar() !!}
          </div>
      </div>
  </div>
</div>
@endcan
<div class="loading">
  <div class="loader"></div>
</div>
@endsection
@push('scripts')
<style>
.loading{
    display: hidden;
    position: fixed;
    left: 0;
    top: 0;
    padding-top: 45vh;
    padding-left:100vh;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: gray;
    opacity: 0.8;
}
.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

#timer {
  background: #222;
  display: inline-block;
  line-height: 1;
  padding: 10px;
  font-size: 40px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  color: white;
  font-weight: 100;
}
#timer span {
  display: block;
  font-size: 20px;
  color: white;
}

#days {
  font-size: 30px;
  color: #db4844;
  float: left;
  padding: 10px;
  width: 25%;
}
#hours {
  font-size: 30px;
  color: #f07c22;
  float: left;
  padding: 10px;
  width: 25%;
}
#minutes {
  font-size: 30px;
  color: #f6da74;
  float: left;
  padding: 10px;
  width: 25%;
}
#seconds {
  font-size: 30px;
  color: #abcd58;
  float: left;
  padding: 10px;
  width: 25%;
}

</style>
<!-- For Charts -->
<script src="{{asset('bower_components/chart.js/Chart.js')}}"></script>
<script>
  $(function () {
    /* ChartJS
     * -------
     */
    var data="{{$chartcreatedat}}";
    var areaChartData = {
      labels  : JSON.parse(data.replace(/&quot;/g,'"')),
      datasets: [
        {
          label               : 'Leads',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : {{$chartleads}}
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    
    //-------------
    //- Lead CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = areaChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)

    var dataappointment="{{$appointmentchartcreatedat}}";
    var AppointmentChartData = {
      labels  : JSON.parse(dataappointment.replace(/&quot;/g,'"')),
      datasets: [
        {
          label               : 'Leads',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : {{$chartleadsappointments}}
        }
      ]
    }


    //-------------
    //- Appointment CHART -
    //--------------

    var barChartCanvas                   = $('#lineChart2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = AppointmentChartData
    barChartData.datasets[0].fillColor   = '#00a65a'
    barChartData.datasets[0].strokeColor = '#00a65a'
    barChartData.datasets[0].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(AppointmentChartData, barChartOptions)

    

  })
</script>
@can('show-dashboard-calendar')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
{!! $calendar->script() !!}
@endcan


<script>
var dated=moment().format('YYYY-MM-DD HH:mm:ss');
var today=moment().format('YYYY-MM-DD');
$(document).ready(function (e) {
  $('#CounterModal').show();
  $('#CounterModal').modal({backdrop: 'static', keyboard: false});
  $(".loading").fadeOut();
  //console.log(moment().format('YYYY-MM-DD HH:mm:ss'));
  @if($settings['manualatt']==1 && $user->staffdetails->showinsalary==1)
  @can('manualatt')
  var checkoutmissing={{ $checkoutmissing }};
  if(today=='{{$attdated}}' && checkoutmissing==0 ){
    $('.checkin').hide();
    //$('.checkinnew').hide();
    //$('.checkout').hide();
  }
  

  //console.log(moment().format('YYYY-MM-DD HH:mm:ss'));
  var today=moment().format('YYYY-MM-DD');
  $('.checkin').html('<li class="fa fa-clock-o"></li> Check-In for: '+ moment().format('DD-MMM-YYYY') + '<span id="timed"></span>');
  //$('.checkinnew').html('<li class="fa fa-clock-o"></li> Check-In New for: '+ moment().format('DD-MMM-YYYY'));
  var checkoutmissing={{ $checkoutmissing }};
  if(today=='{{$attdated}}' && checkoutmissing==0 ){
    $('.checkin').hide();
    //$('.checkinnew').hide();
    
  }
  
  if(today!='{{$attdated}}' ){
    $('.checkout').attr("title", "You might have missed the {{$attdatedsrv}} check-out. Please mark first.");   
  }
  $('.checkout').html('<li class="fa fa-clock-o"></li> Check Out for: {{$attdatedsrv}} <span id="timed"></span> ');
  //$('.checkoutnew').html('<li class="fa fa-clock-o"></li> Check Out NEw for: {{$attdatedsrv}} ');

  //Mark Attendance Begins
  $(document).on('click', '.manuallatt', function()
  {
    var dataaction = $('.manuallatt').attr('data-action');
    var atttitle="";
    if(dataaction==1){
      atttitle="Check-In";
    }else{
      atttitle="Check-Out";
    }
      swal({
        title: "Are you sure want to Mark " + atttitle + "?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {          
          url="{{route('markmanualattnew')}}";
          //dated=dated.getUTCDate();
          $.ajax({
            "url":url,
            type: "POST",
            data: {'attmethod':dataaction,'dated':dated ,_token: '{{csrf_token()}}'},
            dataType : "json",
            beforeSend : function()
            {
              $(".loading").fadeIn();
            },
            statusCode: {
            403: function() {
              $(".loading").fadeOut();                
              swal("Failed", "Permission deneid for this action." , "error");
              return false;
            }
           },
            success: function(data)
            {
              $(".loading").fadeOut();
              swal("Success", data.success, "success");
              $(".loading").fadeOut();
              setTimeout(function(){
                window.location.reload(1);
              }, 3000);
            },
              error: function(){
                $(".loading").fadeOut();
                swal("Failed", "Unable to mark attendance, Please try again." , "error");
              },          
          });
        } 
      });

    });
    //Mark Attendance Ends








  //Mark Attendance New Begins
  $(document).on('click', '.manuallattnew', function()
  {
    return false;
    var dataaction = $('.manuallatt').attr('data-action');
    var atttitle="";
    if(dataaction==1){
      atttitle="Check-In new";
    }else{
      atttitle="Check-Out new";
    }
      swal({
        title: "Are you sure want to Mark " + atttitle + "?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {          
          url="{{route('markmanualattnew')}}";
          //dated=dated.getUTCDate();
          $.ajax({
            "url":url,
            type: "POST",
            data: {'attmethod':dataaction,'dated':dated ,_token: '{{csrf_token()}}'},
            dataType : "json",
            beforeSend : function()
            {
              $(".loading").fadeIn();
            },
            statusCode: {
            403: function() {
              $(".loading").fadeOut();                
              swal("Failed", "Permission deneid for this action." , "error");
              return false;
            }
           },
            success: function(data)
            {
              $(".loading").fadeOut();
              swal("Success", data.success, "success");
              $(".loading").fadeOut();
              setTimeout(function(){
                window.location.reload(1);
              }, 3000);
            },
              error: function(){
                $(".loading").fadeOut();
                swal("Failed", "Unable to mark attendance, Please try again." , "error");
              },          
          });
        } 
      });

    });
    //Mark New Attendance Ends


    @endcan
    @endif

});


var $hOut = $('#hours'),
    $mOut = $('#minutes'),
    $sOut = $('#seconds'),
    $ampmOut = $('#ampm');
var months = [
  'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
];

var days = [
  'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
];

function update(){
  var date = new Date();
  
  var ampm = date.getHours() < 12
             ? 'AM'
             : 'PM';
  
  var hours = date.getHours() == 0
              ? 12
              : date.getHours() > 12
                ? date.getHours() - 12
                : date.getHours();
  
  var minutes = date.getMinutes() < 10 
                ? '0' + date.getMinutes() 
                : date.getMinutes();
  
  var seconds = date.getSeconds() < 10 
                ? '0' + date.getSeconds() 
                : date.getSeconds();
  
  var dayOfWeek = days[date.getDay()];
  var month = months[date.getMonth()];
  var day = date.getDate();
  var year = date.getFullYear();
  
  var dateString = dayOfWeek + ', ' + month + ' ' + day + ', ' + year;
  var currenttimed = " "+ hours +':'+ minutes +':'+ seconds + ' ' + ampm;
  dated=moment().format('YYYY-MM-DD HH:mm:ss');
  $('#timed').text(currenttimed);
} 

update();
window.setInterval(update, 1000);


function makeTimer() {

	//		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
		var endTime = new Date("1 May 2020 11:00:00");			
			endTime = (Date.parse(endTime) / 1000);

			var now = new Date();
			now = (Date.parse(now) / 1000);

			var timeLeft = endTime - now;

			var days = Math.floor(timeLeft / 86400); 
			var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
			var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
			var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
			if (hours < "10") { hours = "0" + hours; }
			if (minutes < "10") { minutes = "0" + minutes; }
			if (seconds < "10") { seconds = "0" + seconds; }

			$("#days").html(days + "<span>Days</span>");
			$("#hours").html(hours + "<span>Hours</span>");
			$("#minutes").html(minutes + "<span>Minutes</span>");
			$("#seconds").html(seconds + "<span>Seconds</span>");		

	}

	setInterval(function() { makeTimer(); }, 1000);
</script>



@endpush
