@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif 
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Meeting Details</h3>
				      <span class="pull-right">
    				    <a href="{!! url('/meetings'); !!}" class="btn btn-danger"><span class="fa fa-backward"></span> Go Back</a>
				      </span>	
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-striped">
                <tr>
                    <td><b>Meeting Title</b></td>
                    <td><strong>{{$par_val['title']}}</strong></td>
                </tr>
                <tr>
                    <td><b>Meeting Recording</b></td>
                    <td><a href="{{Storage::disk('local')->url('public/meetings_assets/videos/'.$par_val['recording_file'])}}" target="_blank"> Open Link</a></td>
                </tr>
                <tr>
                    <td><b>Meeting Date & Time</b></td>
                    <td>{{ $par_val->meeting_datetime->format('d-M-Y H:i') }}</td>
                </tr>
                <tr>
                    <td><b>Description</b></td>
                    <td>{{ $par_val->description }}</td>
                </tr>
                
                <tr>
                    <td><b>Created By</b></td>
                    <td>{{ $par_val->createdby->fname }} {{ $par_val->createdby->lname }}</td>
                </tr>
                <tr>
                    <td><b>Created At</b></td>
                    <td>{{$par_val->created_at->format('d-M-Y H:i')}}</td>
                </tr>
              </table>
            @if(count($showparticipants) > 0)
              <table id="example1" class="table table-bordered display responsive nowrap" style="width:100%">
                <thead>
                <tr>
				  <!--<th>Team name</th>
                  <th>Teamlead</th>-->
				  <th>Meeting Participants</th>

                </tr>
                </thead>
                <tbody>
                @foreach($showparticipants as $showteam)
                  <tr>
					<!--<td>{{ $showteam->pivot->pivotParent->team_name }} </td>
					<td>{{ $showteam->pivot->pivotParent->participant_name['fname'] }} {{ $showteam->pivot->pivotParent->participant_name['lname'] }}</td>
				    
					<td>{{ $showteam['id'] }} </td>	-->
					<td>{{ $showteam['fname'] }} {{ $showteam['lname'] }}  </td>
					<!--<td>{{ $showteam->pivot->team_id }} </td>
					<td>{{ $showteam->pivot->user_id }} </td>-->
					


					
                  </tr>
                  @endforeach
                </tbody>
              </table>
              @else
              <div>No Record found.</div>
              @endif

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   




<script type="text/javascript">
$(document).ready(function(){ 

	
 

 
}); 
</script>

@endsection
