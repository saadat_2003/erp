@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif
 
<style type="text/css">
  .action_btn a{
    margin: 5px;
  }
</style>

<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Meetings</h3>
              @can('meetings-store')
              <span class="pull-right">
              <a href="{!! url('/meetings/create'); !!}" class="btn btn-info"><span class="fa fa-plus"></span> Add New Meeting</a>
              </span>
              @endcan
            </div>
            <div class="box-body">
            <table id="userTable" class="display responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Meeting Time</th>
                  <th>Created by</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                  <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Meeting Time</th>
                  <th>Created by</th>
                  <th>Action</th>
                  </tr>
                </tfoot>
            </table>
          </div>


<!--Delete modal popup -->
<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this Record?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>	
		  

            <!-- /.box-header -->
            {{-- <div class="box-body">
            @if(count($users) > 0)
              <table id="example1" class="display responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Designation</th>
                  <th>Department</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                  <tr>
                    <td>{{$user['id']}}</td>
                    <td>{{$user['fname']}} {{$user['lname']}}</td>
                    <td>{{$user['designation']['name']}}</td>
                    <td>{{$user['department']['deptname']}}</td>
                    <td>{{$user['role']['role_title']}}</td>
                    <td>
                      @if ($user['status'] === 1)
                      <span class="btn btn-success">Active</span>
                      @else
                      <span class="btn btn-danger">Deactive</span>
                      @endif
                    </td>
                    @can('delete-staff')
                     <!-- For Delete Form begin -->
                    <form id="form{{$user['id']}}" action="{{action('UserController@destroy', $user['id'])}}" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                    </form>
                    <!-- For Delete Form Ends -->
                    @endcan
                    <td>
                      @can('show-staff')<a href="{!! url('/admins/'.$user['id']); !!}" class="btn btn-primary" title="View Detail"><i class="fa fa-eye"></i> </a>@endcan
                      @can('edit-staff')<a href="{!! url('/admins/'.$user['id'].'/edit'); !!}"  class="btn btn-success" title="Edit"><i class="fa fa-edit"></i> </a>@endcan
                      @can('status-staff')
                        @if ($user['status'] === 1)
                          <a href="{!! url('/admins/deactivate/'.$user['id']); !!}"  class="btn btn-warning" title="Deactivate"><i class="fa fa-times"></i> </a>
                        @else
                          <a href="{!! url('/admins/active/'.$user['id']); !!}"  class="btn btn-info" title="Active"><i class="fa fa-check"></i> </a>
                        @endif
                      @endcan
                      @can('delete-staff')<button class="btn btn-danger" onclick="archiveFunction('form{{$user['id']}}')"><i class="fa fa-trash"></i></button>@endcan
                      @can('staff-reset-password')<a href="{!! url('/admins/resetpassword/'.$user->id); !!}"  class="btn btn-info" title="Reset Password"><i class="fa fa-key"></i> </a>@endcan
                    </td>                   

                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Designation</th>
                  <th>Department</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
              @else
              <div>No Record found.</div>
              @endif
            </div> --}}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

  <script type="text/javascript">
$(document).ready(function(){    
    $('#userTable').DataTable({
      "bDestroy": true,
      "processing":true,
      "serverSide":true,
      "order" :[ 0, "desc" ],
      "ajax":"{{ route('meetings.fetch') }}",
      "columns":[
        {"data":"id"},
        {"data":"title"},
        {"data":"meeting_datetime"},
        {"data":"created_by"},
        {"data":"options",orderable:false,searchable:false},
      ]
    });



	
	
	
 var meet_id;

 $(document).on('click', '.delete_meet', function(){
  meet_id = $(this).attr('id');
  $('#confirmModal').modal('show');
  $('#ok_button').text('Ok');
 });

 $('#ok_button').click(function(){
  $.ajax({
   url:"meetings/destroy/"+meet_id,
   beforeSend:function(){
    $('#ok_button').text('Deleting...');
   },
   success:function(data)
   {
    setTimeout(function(){
     $('#confirmModal').modal('hide');
     $('#userTable').DataTable().ajax.reload();
    }, 2000);
   }
  })
 }); 
 
}); 
  </script>
@endsection
