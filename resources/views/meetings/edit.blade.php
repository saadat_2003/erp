@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif

    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Meeting</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{action('MeetingController@update', $meeting->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
          <div class="box-body" >
			
            <div class="row">			
			<!-- Customer Info -->	
              <div class="col-md-12">

              <div class="col-md-12">
                <div class="form-group">
                  <label for="title" class="col-sm-3 control-label">Title</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" autocomplete="off" value="{{$meeting->title}}" required >
                    @if ($errors->has('title'))
                          <span class="text-red">
                              <strong>{{ $errors->first('title') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>

				<div class="form-group">
					  <label for="description" class="col-sm-3 control-label">Description</label>

					  <div class="col-sm-9">
						<textarea type="text" class="form-control" id="description" name="description" placeholder="Any description, please put here.">{{$meeting->description}}</textarea>
						@if ($errors->has('description'))
							  <span class="text-red">
								  <strong>{{ $errors->first('description') }}</strong>
							  </span>
						  @endif
					  </div>
				</div>

                <div class="form-group">
                  <label for="title" class="col-sm-3 control-label">Select Date & Time</label>

                  <div class="col-sm-9">
                        <div>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" id="meeting_datetime" name="meeting_datetime" autocomplete="off" required value="{{$meeting->meeting_datetime->format('m/d/Y h:i A')}}" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                    <script type="text/javascript">
                        $(function () {
							
							var meeting_datetime = $(this).attr('meeting_datetime');
                            $('#datetimepicker1').datetimepicker({
                                sideBySide: true,
                                //minDate: new Date(<?php echo date('Y',strtotime($meeting->meeting_datetime))?>, <?php echo date('m',strtotime($meeting->meeting_datetime))?> - 1, <?php echo date('d',strtotime($meeting->meeting_datetime))?> , <?php echo date('H',strtotime($meeting->meeting_datetime))?>, <?php echo date('i',strtotime($meeting->meeting_datetime))?>),
                            });
                        });
                    </script>
                    
                        @if ($errors->has('meeting_datetime'))
                          <span class="text-red">
                              <strong>{{ $errors->first('meeting_datetime') }}</strong>
                          </span>
                        @endif
                  </div>
                </div>
				
                <div class="form-group">
                  <label for="meeting_link" class="col-sm-3 control-label">Meeting Link</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="meeting_link" name="meeting_link" placeholder="Zoom Room Meeting Link Optional" autocomplete="off" value="{{$meeting->meeting_link}}" required>
                    @if ($errors->has('meeting_link'))
                          <span class="text-red">
                              <strong>{{ $errors->first('meeting_link') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>
				
				<div class="form-group">
					  <label for="recording_file" class="col-sm-3 control-label">Select file</label>

					  <div class="col-sm-9">
						<input class='form-control' type="file" name="recording_file" id="recording_file">						
                        <span class="text-red">Only Video files are allowed</span>
						@if ($errors->has('recording_file'))
							  <span class="text-red">
								  <strong>{{ $errors->first('recording_file') }}</strong>
							  </span>
						  @endif
					  </div>
				</div>
				<input type='hidden' name='hidden_recording_file' value='{{$meeting->recording_file}}' />



                <?php  $users = json_decode(json_encode($meeting->users));
                $ids = array_column($users, 'id');
                //dd($ids); ?>

              <div class="form-group">
                <label for="emp_id" class="col-sm-3 control-label">Participants</label>
                <div class="col-sm-9" >
                <select class="form-control select2" name="emp_id[]" multiple="multiple" data-placeholder="Select a Participants" style="width: 100%;">
                  @foreach($user as $row)
                  <option value="{{ $row->id }}" @if(in_array($row->id, $ids)) selected @endif>{{ $row->fname }} {{  $row->lname }} ({{$row->department->deptname}} - {{$row->designation->name}})</option>
                  @endforeach
                </select>
              </div>
              </div>

			
              </div>
              </div>

          </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{!! url('/meetings'); !!}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>

<link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>




<script>
$(function() {
    $('.select2').select2();
});

$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});

</script>
@endsection