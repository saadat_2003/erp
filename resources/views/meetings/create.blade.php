@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif

 
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">	
  <!-- iCheck 1.0.1 -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- script for multi select under ADD COURSE -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">New Meeting</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{!! url('/meetings/'); !!}" method="post" enctype="multipart/form-data">
            @csrf
          <div class="box-body" >
			
            <div class="row">			
				<!--lead_id against which recording will be stored -->
              <div class="col-md-12">
                <div class="form-group">
                  <label for="title" class="col-sm-3 control-label">Title</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" autocomplete="off" value="" required >
                    @if ($errors->has('title'))
                          <span class="text-red">
                              <strong>{{ $errors->first('title') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>

				<div class="form-group">
					  <label for="description" class="col-sm-3 control-label">Description</label>

					  <div class="col-sm-9">
						<textarea type="text" class="form-control" id="description" name="description" placeholder="Any description, please put here.">{{old('description')}}</textarea>
						@if ($errors->has('description'))
							  <span class="text-red">
								  <strong>{{ $errors->first('description') }}</strong>
							  </span>
						  @endif
					  </div>
				</div>

                <div class="form-group">
                  <label for="title" class="col-sm-3 control-label">Select Date & Time</label>

                  <div class="col-sm-9">
                        <div>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="meeting_datetime" autocomplete="off" required />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker1').datetimepicker({
                                sideBySide: true,
                                //minDate: new Date(<?php echo date('Y')?>, <?php echo date('m')?> , <?php echo date('d')-5?>),
                            });
                        });
                    </script>
                    
                        @if ($errors->has('meeting_datetime'))
                          <span class="text-red">
                              <strong>{{ $errors->first('meeting_datetime') }}</strong>
                          </span>
                        @endif
                  </div>
                </div>				
				
                <div class="form-group">
                  <label for="meeting_link" class="col-sm-3 control-label">Meeting Link</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="meeting_link" name="meeting_link" placeholder="Zoom Room Meeting Link Optional" value="" autocomplete="off" require>
                    @if ($errors->has('meeting_link'))
                          <span class="text-red">
                              <strong>{{ $errors->first('meeting_link') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>
				
				<div class="form-group">
					  <label for="recording_file" class="col-sm-3 control-label">Select file</label>

					  <div class="col-sm-9">
						<input class='form-control' type="file" name="recording_file" id="recording_file">						
                        <span class="text-red">Only Video files are allowed</span>
						@if ($errors->has('recording_file'))
							  <span class="text-red">
								  <strong>{{ $errors->first('recording_file') }}</strong>
							  </span>
						  @endif
					  </div>
				</div>
				
				

					<div class="form-group">
					  <label for="empID" class="col-sm-3 control-label">Select Participants</label>
					  <div class="col-sm-9">
						<select class="form-control select2" multiple="multiple" id="empID" name="empID[]" data-placeholder="Select Participants"
								style="width: 100%;">
							<option value="0">Select Employee</option>	
							@if ($employees!='')
								@foreach($employees as $key => $emp_A)
								<option value="{{ $emp_A->id }}" >{{ $emp_A->fname }} {{ $emp_A->lname }} [ {{ $emp_A->department->deptname }} - {{ $emp_A->designation->name }} ]</option>							
								@endforeach
							@endif							
						</select>
							
							@if ($errors->has('empID'))
							  <span class="text-red">
								  <strong>{{ $errors->first('empID') }}</strong>
							  </span>
						  @endif
					  </div>
					</div>				
			
			
              </div>
              </div>

          </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <a href="{!! url('/meetings/'); !!}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Upload</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>


<script>
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});






//select2 multi checkbox
/* $(document).ready(function() {
$('#empID').multiselect({
nonSelectedText: 'Select Emp',
buttonWidth:'100%'
});
}); */

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

  })

  
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

</script>

@endsection