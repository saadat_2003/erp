@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif

<div class="row">
    <div class="col-md-12">
        <!--<form class="form-horizontal" action="{!! url('leaves_qa/'); !!}" method="post" enctype="multipart/form-data">
          @csrf-->
        <div class="box box-success collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Advance Filter</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body" style="display: none;">
            
            <!--Search Form Begins -->


			  
			  <div class="form-group col-md-6">
			  <div class="col-sm-12">
                <label>Select Department</label>
                <select id="srcdepartment_id" name="srcdepartment_id" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Show all departments" style="width: 100%;" tabindex="-1" aria-hidden="true">
					<option value="">Show all departments</option>
					@if ($departments!='')
						@foreach($departments as $key => $department)
							<option value="{{$department->id}}" >{{$department->deptname}}</option>    
						@endforeach
					@endif              
                </select>		
				
              </div>
			  </div>
			  
			  
			  
              <div class="form-group col-md-6"> 
			  <div class="col-sm-12">			  
                  <label>Select Date Range:</label>  
                  <div class="input-group">
                    <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                      <span>{{date('F d, Y')}} - {{date('F d, Y')}}</span>
                      <input type="hidden" name="dateFrom" id="dateFrom" value="{{date('Y-m-d')}}">
                      <input type="hidden" name="dateTo" id="dateTo" value="{{date('Y-m-d')}}">
                      <i class="fa fa-caret-down"></i>
                    </button>
                  </div>
			  </div>
			  </div>

			 
              <script>
                
                 $(document).ready(function() { 


                 
                  //Date range as a button
                  $('#daterange-btn').daterangepicker(
                    {
                      ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                      },
                      startDate: moment().subtract(29, 'days'),
                      endDate  : moment()
                    },
                     function (start, end) {
                       $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                       $('#dateFrom').val(start.format('YYYY-MM-DD'));
                       $('#dateTo').val(end.format('YYYY-MM-DD'));
                       var maintabledate = $('#table_data').DataTable();
                       maintabledate.column('6').search(
                        $('#dateFrom').val()+','+$('#dateTo').val()
                        ).draw();
                     }
                   );
                   });
                   $('#daterange-btn span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().endOf('month').format('MMMM D, YYYY'));


              </script>
            <!-- Search Form Ends -->


            <!-- Search Form Ends -->
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
              <button type="button" onclick='InitTable()' class="pull-right btn btn-primary" id="searchRecords">Search
                <i class="fa fa-search"></i></button>
				<input name='search-submit' value='1' type='hidden' />
          </div>
        </div>
        <!-- /.box -->
      <!--</form>-->
      </div>
</div>  

<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Leaves Assesment QA</h3>
              <span class="pull-right">
              @can('myleaves-create')             
                <!--<button type="button" name="create_record" id="create_record" class="btn btn-info"><span class="fa fa-plus"></span> Add New Leave</button>-->			
              @endcan
            </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user_table" class="display responsive wrap" style="width:100%">
                <thead>
                <tr>

				  
                  <th>Id</th>
                  <th>Employee</th>
                  <th>Dated</th>
                  <th>Description</th>
                  <th>Leave Type</th>
                  <th>Paid Leave</th>
                  <th>Created By</th>
                  <th>Created At</th>
                  <th>Status</th>

                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               	  
                </tbody>
                <tfoot>
                <tr>

					
                    <th>Id</th>
                    <th>Employee</th>
                    <th>Dated</th>
                    <th>Description</th>
                    <th>Leave Type</th>
                    <th>Paid Leave</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>Status</th>

                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row --> 

    

		
		
		
	
		
		
		

		
		
@endsection
@push('scripts')
<style>
.loading{
    display: hidden;
    position: fixed;
    left: 0;
    top: 0;
    padding-top: 45vh;
    padding-left:100vh;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: gray;
    opacity: 0.8;
}
.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<style>
.select2-container--classic .select2-selection--single .select2-selection__rendered{
    line-height: 35px;
    
}
.select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 8px;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #3c8dbc;
    border-radius: 4px;
}

</style>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.8.9/jquery.csv.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>



<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script>
$(document).ready(function(){
  $('body').tooltip({selector: '[data-toggle="tooltip"]'});

 InitTable();
 
 
 



 

});


//Fetch Data Begins
  function InitTable() {
    $('#user_table').DataTable({
      "bDestroy": true,
      "processing":true,
      "serverSide":true,
      "order" :[ 0, "desc" ],
      "ajax":{
                  "url": "{{ route('leaves_qa.fetch') }}",
                  "dataType": "json",
                  "type": "POST",
                  "data":{   
					_token: "{{csrf_token()}}",
                srcdepartment_id: $('#srcdepartment_id').val(), 
                dateFrom: $('#dateFrom').val(),
                dateTo: $('#dateTo').val()					
					},
                },
      "columns":[
        {"data":"leaves_id"},
        {"data":"applicant"},
        {"data":"dated"},
        {"data":"description"},
        {"data":"leavetype"},
        {"data":"ispaid"},
		{"data":"createdby"},
		{"data":"created_at"},
		{"data":"status"},		
		
        {"data":"action",orderable:false,searchable:false},
      ]
    });
}
//Fetch Data Ends
</script>		
<!-- Select2 script START -->
<script>        
		 $(document).ready(function() { 
			  $('.select2').select2({
				  placeholder: "Select From DropDown",
				  multiple: false,
			  }); 
			  $('.select2').change(
				console.log("select2-console-log")
			  );
		  });

</script>
<!-- Select2 script ENDS -->
@endpush