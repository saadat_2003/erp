 @foreach($notifications as $notification)
    @if(isset($notification->read_at) && $notification->read_at!='')
          <tr data-status="pendiente">
            <td>
              <div class="media">
                <div class="media-body">
                  <span class="pull-right">
                    <label class="badge label label-primary" id="{{$notification->id}}" onclick="markUnread(this.id)">Mark Unread</label>
                    <span class="media-meta"><?php  echo   date("M  d,Y", strtotime($notification->created_at)); ?></span>
                  </span>
                  <h4 class="title">
                    <a href="{{$notification->data['letter']['redirectURL']}}">
                      {{$notification->data['letter']['title']}}
                        </a>
                  </h4>
                  <p class="summary">{{$notification->data['letter']['body']}}</p>                                    
                </div>
              </div>
            </td>
        </tr>
    @else
      <tr data-status="pagado" id="trunread{{$notification->id}}" class="selected">
      <td>
        <div class="media">
          <div class="media-body">
          <span class="pull-right">
            <label style="display: none;" class="badge label label-primary unread{{$notification->id}}" id="{{$notification->id}}" onclick="markUnread(this.id)">Mark Unread</label>
            <span class="media-meta">  
                <?php  echo   date("M  d,Y", strtotime($notification->created_at)); ?>
              </span>
            </span>
            <h4 class="title unread">
              <a href="{{$notification->data['letter']['redirectURL']}}" data-notif-id="{{$notification->id}}" data-notify="1">
                {{$notification->data['letter']['title']}} <label class="badge label label-info read{{$notification->id}}"> Unread</label>
              </a>
            </h4>
            <p class="summary">{{$notification->data['letter']['body']}}</p>
          </div>
        </div>
        </a>
      </td>
    </tr>
    @endif
@endforeach   