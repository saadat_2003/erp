
<option>--- Select Teamlead ---</option>
@if(!empty($team_leads))
  @foreach($team_leads as $key => $tls)
    <option value="{{ $tls->id }}">{{ $tls->fname }} {{ $tls->lname }} [ {{ $tls->department->deptname }} - {{ $tls->designation->name }} ]</option>
  @endforeach
@endif


