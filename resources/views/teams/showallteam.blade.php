@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Show ALL Teams</h3><br><br>			  
				<span class="pull-right">

				</span>	


				
			            @csrf

            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            @if(count($showteams) > 0)
              <table id="example1" class="table table-bordered display responsive nowrap" style="width:100%">
                <thead>
                <tr>
				  <!--<th>Team name</th>
                  <th>Teamlead</th>-->
				  <th>Team member</th>
				  <th>Team Lead </th>
				  <th>Status</th>
				  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($showteams as $showteam)
                  <tr>

					<td>{{ $showteam->username['fname'] }} {{ $showteam->username['lname'] }}  </td>
					<td>{{ $showteam->teamlead_id['team_name'] }} </td>
					
					<td>
					  <?php $check_user = \App\Staffdetail::where('user_id',$showteam['user_id'])->first(); 
						if($check_user['is_confirm']==1){
					  ?>
						<span class="label label-success">Confirmed</span>
						<?php } else{ ?>
						<span class="label label-warning">Pending</span>			
						<?php }  ?>					
					</td>
					
                    <!--<td>{{ $showteam->username['fname'] }} {{ $showteam->username['lname'] }}</td>-->
					@can('deletemember-teams')
                     <!-- For Delete Form begin -->
                    <form id="form{{$showteam['user_id']}}" action="{{action('TeamController@destroy', $showteam['user_id'])}}" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                    </form>
                    <!-- For Delete Form Ends -->
                    @endcan
                    <td>
					
					  <?php $check_user = \App\Staffdetail::where('user_id',$showteam['user_id'])->first(); 
						if($check_user['is_confirm']==1){
					  ?>	
					@can('empConfirmUserPass')
					  <button type="button" name="user_pass" id="user_pass_{{$showteam['user_id']}}" empId_user_pass="{{$showteam['user_id']}}" class="emp_user_pass btn btn-primary" title="Add Username/Password"><i class="fa fa-plus"></i></button>
					@endcan
						<?php } else{ ?>
					@can('empConfirm')
					  <!--<button type="button" name="confirm" id="confirm_{{$showteam['user_id']}}" empConfirmId="{{$showteam['user_id']}}" class="empConfirm btn btn-warning" title="Confirm">Make Confirm</button>-->	
					@endcan
						<?php }  ?>	
					
					@can('deletemember-teams')
                      <!--<button class="btn btn-danger" onclick="archiveFunction('form{{$showteam['user_id']}}')"><i class="fa fa-trash"></i></button>-->
					@endcan
					</td>					
                  </tr>
                  @endforeach
                </tbody>
              </table>
              @else
              <div>No Record found.</div>
              @endif

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   
	  
<!--empConfirm modal popup -->
<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Employee Confirmation</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
           <div class="form-group">
            <label class="control-label col-md-4">Comments : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="comments" name="comments" placeholder="Enter comments" autocomplete="off" required>
            </div>
           </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">

				<input type="hidden" name="action_confirm" id="action_confirm" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				
				<input type="hidden" id='empConfirmId' name='empConfirmId' />
				<input type="submit" name="action_button_confirm" id="action_button_confirm" class="btn btn-info" value="Confirm" />					
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>  


<!--add username password modal popup -->
<div id="formModal_user_pass" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title_user_pass">Add Username/Password</h4>
        </div>

          <div class="box">
            <div class="box-body">
              <table id="table_data" class="table table-bordered display responsive wrap" style="width:100%">
                <thead>
                <tr>
                  <th>Emp Name</th>
                  <th>Account Type</th>
                  <th>UserName</th>
                  <th>Password</th>
				  <th>Comments</th>
                </tr>
                </thead>
                <tbody id="table_body">
               	  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
		
        <div class="modal-body">
         <span id="form_result_user_pass"></span>
         <form method="post" id="sample_form_user_pass" class="form-horizontal" enctype="multipart/form-data">
          @csrf
		  
		  <div class="form-group">
			<label for="accounttype" class="control-label col-md-4">Account Type</label>
			  <div class="col-md-8">
				  <select class="form-control select2" id="accounttype" name="accounttype" required>
					<option value="0" selected>Select Account Type</option>    
					<option value="Domain">Domain</option>
					<option value="CCMS">CCMS</option>
					<option value="Skype">Skype</option>
					<option value="Zoom">Zoom</option>
				  </select>
			  </div>
		  </div>		  
		  
           <div class="form-group">
            <label class="control-label col-md-4">Username : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" autocomplete="off" required>
            </div>
           </div>
		   
           <div class="form-group">
            <label class="control-label col-md-4">Password : </label>
            <div class="col-md-8">
             <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" autocomplete="off" required>
            </div>
           </div>		   
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">

				<input type="hidden" name="action_user_pass" id="action_user_pass" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				
				<input type="hidden" id='empId_user_pass' name='empId_user_pass' />
				<input type="submit" name="action_button_user_pass" id="action_button_user_pass" class="btn btn-info" value="Add Credentials" />					
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div> 


<script type="text/javascript">
$(document).ready(function(){ 

//Employee Confirmation
 $(document).on('click', '.empConfirm', function(){
  $('.modal-title').text("Employee Confirmation");
     $('#action_button_confirm').val("Confirm");
     $('#action_confirm').val("Confirm");
//added by me
	$('#comments').val('');
	
    $('#form_result').html("");

    var empConfirmId = $(this).attr('empConfirmId');
    document.getElementById("empConfirmId").value = empConfirmId;
	
    event.preventDefault();	
	 
     $('#formModal').modal('show');
	 
 }); 		
 

//Username/Password
 $(document).on('click', '.emp_user_pass', function(){
  $('.modal-title_user_pass').text("Add Username/Password against Account type");
     $('#action_button_user_pass').val("Add Credentials");
     $('#action_user_pass').val("Add Credentials");
//added by me
	$('#comments').val('');
	
    $('#form_result_user_pass').html("");

    var empId_user_pass = $(this).attr('empId_user_pass');
    document.getElementById("empId_user_pass").value = empId_user_pass;
	
    event.preventDefault();	
	 
     $('#formModal_user_pass').modal('show');
	 
//Following ajax call for dynamically populating table			start
$.ajax({
   url:"{{ route('fetchStaffConfirmationDetail') }}",
   method:'POST',
   dataType:'json',
   "data":{   
			_token: "{{csrf_token()}}",
			empId_user_pass: document.getElementById("empId_user_pass").value, 
		},
   success:function(response){
     console.log(response);
/*  */
     var data=response.data;
     var trHTML="";
     for(var i=0;i<data.length;i++){
        var id=data[i].id;
        var user_id=data[i].staffconfirmationdetail_of_user.fname+" "+data[i].staffconfirmationdetail_of_user.lname;
        var accounttype= data[i].accounttype;
        var username= data[i].username;
		var password= data[i].password;
		var comments=data[i].staffconfirmationdetail_comments.comments_confirm;     
        let trRow="<tr>"+
                  "<td>"+user_id+"</td>"+
                  "<td>"+accounttype+"</td>"+
                  "<td>"+username+"</td>"+
				  "<td>"+password+"</td>"+
				  "<td>"+comments+"</td>"
                  +"</tr>";
       trHTML=trHTML+trRow;
     }
     $('#table_body').empty();
     $('#table_body').append(trHTML);

   },
   error:function(err){
      console.log(err);
   }
});		
//Following ajax call for dynamically populating table			end   	 

 }); 
 
 
 $('#sample_form').on('submit', function(event){
  event.preventDefault();
//Employee Confirmation
  if($('#action_confirm').val() == 'Confirm')
  {
   $.ajax({
    url:"{{ route('empConfirmUpdate') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
      for(var count = 0; count < data.errors.length; count++)
      {
       html += '<p>' + data.errors[count] + '</p>';
      }
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      //$('#userTable').DataTable().ajax.reload();
	  //$("#example1").load("#example1");
	  //$(".Confirm").innerHTML("<button type='button'  class='btn btn-success' title='Confirmed'>Confirmed</button>");
	  empConfirmId_css = document.getElementById("empConfirmId").value;
	  //$('#confirm_'+empConfirmId_css).html("Confirmed");
	  //$('#confirm_'+empConfirmId_css).removeClass('empConfirm btn btn-warning').addClass('btn btn-success');
	  swal("Success!", "Employee Confirmed !", "success").then( () => {
			location.reload();
		});
     }
     $('#form_result').html(html);
    }
   })
  }
 }); 
 
 
 $('#sample_form_user_pass').on('submit', function(event){
  event.preventDefault();
//Username/Password
  if($('#action_user_pass').val() == 'Add Credentials')
  {
   $.ajax({
    url:"{{ route('empConfirmUserPass') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
/*       for(var count = 0; count < data.errors.length; count++)
      { */
       html +=  data.errors ;
/*       } */
      html += '</div>';
	  swal("Error!", "Same account type duplication found!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form_user_pass')[0].reset();
      //$('#userTable').DataTable().ajax.reload();
	  swal("Success!", "Staff Credentials Added !!!", "success");	
//Following ajax call for dynamically populating table			start
$.ajax({
   url:"{{ route('fetchStaffConfirmationDetail') }}",
   method:'POST',
   dataType:'json',
   "data":{   
			_token: "{{csrf_token()}}",
			empId_user_pass: document.getElementById("empId_user_pass").value, 
		},
   success:function(response){
     console.log(response);
/*  */
     var data=response.data;
     var trHTML="";
     for(var i=0;i<data.length;i++){
        var id=data[i].id;
        var user_id=data[i].staffconfirmationdetail_of_user.fname+" "+data[i].staffconfirmationdetail_of_user.lname;
        var accounttype= data[i].accounttype;
        var username= data[i].username;
		var password= data[i].password;
		var comments=data[i].staffconfirmationdetail_comments.comments_confirm;		
        let trRow="<tr>"+
                  "<td>"+user_id+"</td>"+
                  "<td>"+accounttype+"</td>"+
                  "<td>"+username+"</td>"+
				  "<td>"+password+"</td>"+
				  "<td>"+comments+"</td>"				  
                  +"</tr>";
       trHTML=trHTML+trRow;
     }
     $('#table_body').empty();
     $('#table_body').append(trHTML);

   },
   error:function(err){
      console.log(err);
   }
});		
//Following ajax call for dynamically populating table			end   	 	  
     }
     $('#form_result_user_pass').html(html);
    }
   })
  }
 });  
 
 
}); 
</script>

@endsection
