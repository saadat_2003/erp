@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Teams</h3>
			  @can('create-teams')
				<span class="pull-right">
				  <a href="{!! url('/teams/create'); !!}" class="btn btn-info"><span class="fa fa-plus"></span> Add Team</a>
				</span>
			  @endcan
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            @if(count($teams) > 0)
              <table id="example1" class="table table-bordered display responsive nowrap" style="width:100%">
                <thead>
                <tr>
				  <th>Team name</th>
                  <th>Teamlead</th>
				  <th>Department</th>
                  <th>Created by</th>
				  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($teams as $team)
                  <tr>
                    <td>{{ $team['team_name'] }} </td>					
                    <td>{{ $team->teamlead_name['fname'] }} {{ $team->teamlead_name['lname'] }}</td>
					<td>{{ $team->dept_name['deptname'] }} </td>
					<td>{{ $team->createdby['fname'] }} {{ $team->createdby['lname'] }}</td>
                    @can('delete-staff')
                    <!-- For Delete Form begin -->
                    <!-- For Delete Form Ends -->
                    @endcan
                    <td>
					@can('showteam-teams')
                      <a href="{!! url('/teams/showteam/'.$team['id']); !!}" class="btn btn-primary" title="View Detail"><i class="fa fa-eye"></i> </a>                      
                    @endcan
					@can('edit-teams')
					<a href="{!! url('/teams/'.$team['id'].'/edit'); !!}" class="btn btn-success" title="Edit"><i class="fa fa-edit"></i> </a>					  
					@endcan
					</td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
				  <th>Team name</th>
                  <th>Teamlead</th>
				  <th>Department</th>
                  <th>Created by</th>
				  <th>Action</th>
                </tr>
                </tfoot>
              </table>
              @else
              <div>No Record found.</div>
              @endif

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   

@endsection