
<option>--- Select Emp List ---</option>
@if(!empty($emp_available))
  @foreach($emp_available as $key => $emp_A)
    <option value="{{ $emp_A->id }}">{{ $emp_A->fname }} {{ $emp_A->lname }} [ {{ $emp_A->department->deptname }} - {{ $emp_A->designation->name }} ]</option>
  @endforeach
@endif


