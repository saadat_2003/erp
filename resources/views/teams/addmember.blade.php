@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
<!-- some CSS styling changes and overrides -->
<style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}













.facet-container{
  width: 330px;
}
.right {
  float: right;
}
.left {
  float: left;
}
p {
  clear: both;
  padding-top: 1em;
}
.facet-list {
  list-style-type: none;
  margin: 0;
  padding: 0;
  margin-right: 10px;
  background: #eee;
  padding: 5px;
  width: 143px;
  min-height: 1.5em;
  font-size: 0.85em;
}
.facet-list li {
  margin: 5px;
  padding: 5px;
  font-size: 1.2em;
  width: 120px;
}
.facet-list li.placeholder {
  height: 1.2em
}
.facet {
  border: 1px solid #bbb;
  background-color: #fafafa;
  cursor: move;
}
.facet.ui-sortable-helper {
  opacity: 0.5;
}
.placeholder {
  border: 1px solid orange;
  background-color: #fffffd;
}
</style>
	<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script> 
	
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">	
  <!-- iCheck 1.0.1 -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- script for multi select under ADD COURSE -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Assign Employees to : <b>{{ $team_val->team_name }} ({{$team_val->teamlead_name['fname']}} {{$team_val->teamlead_name['lname']}})</b></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none"></div>
            <form class="form-horizontal" action="{!! url('/teams/savemember'); !!}" method="post" enctype="multipart/form-data">
            @csrf
            <input name="team_id" type="hidden" value="{{ $id }}">
            <div class="box-body" >
            <div class="row">
            <div class="col-md-6">
				<div class="form-group">
				  <label for="team_name" class="col-sm-3 control-label">Team Name</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" id="team_name" name="team_name" placeholder="Team Name" autocomplete="off" value="{{ $team_val->team_name }}" readonly='readonly'>
					@if ($errors->has('team_name'))
						  <span class="text-red">
							  <strong>{{ $errors->first('team_name') }}</strong>
						  </span>
					  @endif
				  </div>
				</div>	
				
				<div class="form-group">
				  <label for="teamlead_id" class="col-sm-3 control-label">Team Lead</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" id="teamlead_id" name="teamlead_id" placeholder="Team Name" autocomplete="off" value="{{$team_val->teamlead_name['fname']}} {{$team_val->teamlead_name['lname']}}" readonly='readonly'>
					@if ($errors->has('teamlead_id'))
						  <span class="text-red">
							  <strong>{{ $errors->first('teamlead_id') }}</strong>
						  </span>
					  @endif
				  </div>
				</div>	
				
					
				
					<div class="form-group">
					  <label for="empID" class="col-sm-3 control-label">Select Employees</label>
					  <div class="col-sm-9">
						<select class="form-control select2" multiple="multiple" id="empID" name="empID[]" data-placeholder="Select Emp"
								style="width: 100%;">
							<option value="0">Select Employee</option>	
							@if ($emp_available!='')
								@foreach($emp_available as $key => $emp_A)
								<option value="{{ $emp_A->id }}" >{{ $emp_A->fname }} {{ $emp_A->lname }} [ {{ $emp_A->department->deptname }} - {{ $emp_A->designation->name }} ]</option>							
								@endforeach
							@endif							
						</select>
							
							@if ($errors->has('empID'))
							  <span class="text-red">
								  <strong>{{ $errors->first('empID') }}</strong>
							  </span>
						  @endif
					  </div>
					</div>
					
					
					
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
  
			</div>
          <!-- /.row -->


			
			
			
			</div>		
              
			<div class="box-footer">
                <a href="{!! url('teams/showteam/'.$id); !!}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
	</div>
              <!-- /.box-body -->
              
            </form>
</div>

<script>
$(function() {
    $("#allFacets, #userFacets").sortable({
      connectWith: "ul",
      placeholder: "placeholder",
      delay: 150
    })
    .disableSelection()
    .dblclick( function(e){
      var item = e.target;
      if (e.currentTarget.id === 'allFacets') {
        //move from all to user
        $(item).fadeOut('fast', function() {
          $(item).appendTo($('#userFacets')).fadeIn('slow');
        });
      } else {
        //move from user to all
        $(item).fadeOut('fast', function() {
          $(item).appendTo($('#allFacets')).fadeIn('slow');
        });
      }
    });
  });

</script>

<script type="text/javascript">
  $("select[name='department_id']").change(function(){
      var department_id = $(this).val();
	  console.log(department_id);
      var token = $("input[name='_token']").val();
	  //alert(usertype_teamlead);
	  $.ajax({
          url: "<?php echo route('/teams/select-ajax') ?>",
		  dataType : 'json',
          method: 'POST',
          data: {department_id:department_id,_token:token},
          success: function(data) {
			  console.log(token);
			  console.log(data);
            $("select[name='teamlead_id'").html('');
            $("select[name='teamlead_id'").html(data.options);
          }
      });
  });
  
  $("select[name='teamlead_id']").change(function(){
      var teamlead_id = $(this).val();
	  console.log(teamlead_id);
      var token = $("input[name='_token']").val();
	  //alert(usertype_teamlead);
	  $.ajax({
          url: "<?php echo route('/teams/select-ajax-emp') ?>",
		  dataType : 'json',
          method: 'POST',
          data: {teamlead_id:teamlead_id,_token:token},
          success: function(data) {
			  console.log(token);
			  console.log(data);
            $("select[id='empID'").html('');
            $("select[id='empID'").html(data.options);
          }
      });
  });  
  

  
</script>



<script>
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});






//select2 multi checkbox
/* $(document).ready(function() {
$('#empID').multiselect({
nonSelectedText: 'Select Emp',
buttonWidth:'100%'
});
}); */

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

  })

  
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

</script>

@endsection