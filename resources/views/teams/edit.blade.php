@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
<!-- some CSS styling changes and overrides -->
<style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
    <div class="box box-info">


            <div class="box-header with-border">
              <h3 class="box-title">Edit Team Lead </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none"></div>
            <form class="form-horizontal" action="{{action('TeamController@update', $id)}}" method="post" enctype="multipart/form-data" >
            @csrf
<input name="_method" type="hidden" value="PATCH">
        <div class="box-body" >
            <div class="row">
              <div class="col-md-12">
	


				<div class="form-group">
				  <label for="team_name" class="col-sm-3 control-label">Team Name</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" id="team_name" name="team_name" placeholder="Team Name" 
					autocomplete="off" value="{{ $team->team_name }}" require>
					@if ($errors->has('team_name'))
						  <span class="text-red">
							  <strong>{{ $errors->first('team_name') }}</strong>
						  </span>
					  @endif
				  </div>
				</div>	
				
				<div class="form-group">
					  <label for="department_id" class="col-sm-3 control-label">Department</label>
					  <div class="col-sm-9">
							<select id="department_id" name="department_id" class="form-control m-bot15" disabled>
								<option value="0">Select Department</option>	
							@if ($depts!='')
								@foreach($depts as $key => $dept)
								<option value="{{ $dept->id }}" {{ $dept->id == $team->department_id ? 'selected=selected' : '' }} >{{ $dept->deptname }}</option>							
								@endforeach
							@endif
							</select>
							@if ($errors->has('department_id'))
							  <span class="text-red">
								  <strong>{{ $errors->first('department_id') }}</strong>
							  </span>
							@endif
					  </div>
				</div>		



				


				<div class="form-group">
					  <label for="teamlead_id" class="col-sm-3 control-label">Select Teamlead</label>
					  <div class="col-sm-9">
							<select id="teamlead_id" name="teamlead_id" class="form-control select2">
							<option value="0">Select Teamlead</option>	
							@if ($team_leads!='')
								@foreach($team_leads as $key => $team_lead)
								<option value="{{ $team_lead->id }}" {{ $team_lead->id == $team->teamlead_id ? 'selected=selected' : '' }} >{{ $team_lead['fname'] }} {{ $team_lead['lname'] }} [ {{ $team_lead->department->deptname }} - {{ $team_lead->designation->name }} ]</option>							
								@endforeach
							@endif
							</select>
							@if ($errors->has('teamlead_id'))
							  <span class="text-red">
								  <strong>{{ $errors->first('teamlead_id') }}</strong>
							  </span>
							@endif
					  </div>
				</div>	

				

              </div>
              </div>

        </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{!! url('/teams'); !!}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>


<script type="text/javascript">
  $("select[name='department_id']").change(function(){
      var department_id = $(this).val();
	  console.log(department_id);
      var token = $("input[name='_token']").val();
	  //alert(usertype_teamlead);
	  $.ajax({
          url: "<?php echo route('/teams/select-ajax') ?>",
		  dataType : 'json',
          method: 'POST',
          data: {department_id:department_id,_token:token},
          success: function(data) {
			  console.log(token);
			  console.log(data);
            $("select[name='teamlead_id'").html('');
            $("select[name='teamlead_id'").html(data.options);
          }
      });
  });
  

</script>


<script>
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
</script>


<!-- Select2 script START -->
<script>        
		 $(document).ready(function() { 
			  $('.select2').select2({
				  placeholder: "Select From DropDown",
				  multiple: false,
			  }); 
			  $('.select2').change(
				console.log("select2-console-log")
			  );
		  });

</script>
<!-- Select2 script ENDS -->
@endsection