@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif

<style type="text/css">
  .action_btn a{
    margin: 5px;
  }
</style>

<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Bank Details</h3>
			@can('increament-store')
              <span class="pull-right">
				<button type="button" name="create_record" id="create_record" class="btn btn-info"><span class="fa fa-plus"></span> Add Bank Details</button>
              </span>
			@endcan
            </div>
            <div class="box-body">
            <table id="userTable" class="display responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Bank Name </th>
                    <th>Bank Account Number</th>
                    <th>Created by</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Bank Name </th>
                    <th>Bank Account Number</th>
                    <th>Created by</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
            </table>
          </div>
	  </div>
	  <!-- /.box -->
	</div>
	<!-- /.col -->
  </div>
  <!-- /.row -->   
  
<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Bank Details</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf

           <div class="form-group">
            <label class="control-label col-md-4" >Employee : </label>
                <div class="col-md-8">
                <select id="user_id" name="user_id" class="form-control select2 select2-hidden-accessible" data-placeholder="Select Employee" style="width: 100%;" tabindex="-1" aria-hidden="true">
					<option value="">Select Employee</option>
					@if ($users!='')
						@foreach($users as $key => $user)
							<option value="{{ $user->id }}" >{{ $user->fname }} {{ $user->lname }} [ {{ $user->department->deptname }} - {{ $user->designation->name }} ]</option>    
						@endforeach
					@endif              
                </select>
				</div>
			  </div>  		  

		  
           <div class="form-group">
            <label class="control-label col-md-4" >Bank Name : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="bankName" name="bankName" placeholder="Enter Bank Name " autocomplete="off" required>
            </div>
           </div>		  
           <div class="form-group">
            <label class="control-label col-md-4" >Bank Account Number : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="bankAccountNumber" name="bankAccountNumber" placeholder="Bank Account Number" autocomplete="off" required>
            </div>
           </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">

				<input type="hidden" name="action_bankdetails" id="action_bankdetails" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				
				<input type="hidden" id='bankdetails_of_user' name='bankdetails_of_user' />
				<input type="submit" name="action_button_bankdetails" id="action_button_bankdetails" class="btn btn-info" value="Add" />					
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>  

<!--Delete modal popup -->
<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Delete Bank Details</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this Record?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>	

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

  <script type="text/javascript">
$(document).ready(function(){    
    $('#userTable').DataTable({
      "bDestroy": true,
      "processing":true,
      "serverSide":true,
      "order" :[ 0, "desc" ],
      "ajax":{
                  "url": "{{ route('bankdetails.fetch') }}",
                  "dataType": "json",
                  "type": "POST",
                  "data":{   
					_token: "{{csrf_token()}}",
					},
                },
      "columns":[
        {"data":"id"},
        {"data":"user_id"},
        {"data":"bankName"},
        {"data":"bankAccountNumber"},
        {"data":"createdby"},
        {"data":"options",orderable:false,searchable:false},
      ]
    });

 $('#create_record').click(function(){
  $('.modal-title').text("Add Bank Details");
     $('#action_button_bankdetails').val("Add");
     $('#action_bankdetails').val("Add");
//added by me
    $('#bankName').val('');
	$('#bankAccountNumber').val('');
	
    $('#form_result').html("");
	
	 
     $('#formModal').modal('show');
	 
 });
	



 $('#sample_form').on('submit', function(event){
  event.preventDefault();
//Bank Details
  if($('#action_bankdetails').val() == 'Add')
  {
   $.ajax({
    url:"{{ route('bankdetails.store') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
/*       for(var count = 0; count < data.errors.length; count++)
      { */
       html += data.errors ;
/*       } */
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      $('#userTable').DataTable().ajax.reload();
	  swal("Success!", "Bank Details Added !", "success");
     }
     $('#form_result').html(html);
    }
   })
  }

  if($('#action_bankdetails').val() == "Edit")
  {
   $.ajax({
    url:"{{ route('bankdetails.update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
	  swal("Failed", data.errors + "\r\nERROR.", "error");
      html = '<div class="alert alert-danger">';
/*       for(var count = 0; count < data.errors.length; count++)
      { */
       html += data.errors;
/*       } */
      html += '</div>';
     }
     if(data.success)
     {
		swal("Success", "Bank Details updated successfully .", "success");
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      $('#userTable').DataTable().ajax.reload();
     }
     $('#form_result').html(html);
    }
   });
  }
  
 }); 
 


 $(document).on('click', '.edit', function(){
  var id = $(this).attr('id');
  //alert(id);
  $('#form_result').html('');
  $.ajax({
   url:"bankdetails/"+id+"/edit",
   dataType:"json",
   success:function(html){
	   //var d = Date.parse("March 21, 2012");
	$('#user_id').val(html.data.user_id);
    $('#bankName').val(html.data.bankName);
	$('#bankAccountNumber').val(html.data.bankAccountNumber);
	
	$('#hidden_id').val(html.data.id);
    $('.modal-title').text("Edit Bank Details");
    $('#action_button_bankdetails').val("Edit");
    $('#action_bankdetails').val("Edit");
    $('#formModal').modal('show');
   }
  })
 });  

 
 var bankdetails_id;

 $(document).on('click', '.delete', function(){
  $('.modal-title').text("Delete Bank Details");
  $('#ok_button').text('OK');
  bankdetails_id = $(this).attr('id');
  $('#confirmModal').modal('show');
 });

 $('#ok_button').click(function(){
  $.ajax({
   url:"bankdetails/destroy/"+bankdetails_id,
   beforeSend:function(){
    $('#ok_button').text('Deleting...');
   },
   success:function(data)
   {
    setTimeout(function(){
     $('#confirmModal').modal('hide');
     $('#userTable').DataTable().ajax.reload();
    }, 2000);
   }
  })
 }); 
 
}); 
  </script>
  
<!-- Select2 script START -->
<script>        
		 $(document).ready(function() { 
			  $('.select2').select2({
				  placeholder: "Select From DropDown",
				  multiple: false,
			  }); 
			  $('.select2').change(
				console.log("select2-console-log")
			  );
		  });

</script>
<!-- Select2 script ENDS -->  
@endsection
