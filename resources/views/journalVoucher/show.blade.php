<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>ERP JV Print</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print()">
<div class="wrapper">
    <div class="modal-header">
        <div class="row">
          <div class="col-md-6">
            <div style="margin-right: 20px;">
              <label>Voucher No: &nbsp;<small>{{$data['journalVoucher']->voucher_no}}</small></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="pull-right">
                <label>Date: &nbsp;<small>{{$data['journalVoucher']->dated}}</small></label>
            </div>
          </div>
        </div>
        <label>Description</label>
        <p id="view_description">{{$data['journalVoucher']->description}}</p>
        
      </div>
    <table id="print_table_data" class="table table-striped table-bordered responsive" style="width:100%">
      <thead>
      <tr>
        <th>ID</th>
        <th>Account</th>
        <th>Debit</th>
        <th>Credit</th>
      </tr>
      </thead>
      <tbody>
        @php
            $credit_sum = 0;
            $debit_sum = 0;
        @endphp
        @foreach($data['journalVoucher']->journalVoucherDetail as $row)
        @php
            $credit_sum += $row->credit;
            $debit_sum += $row->debit;
        @endphp
          <tr>
            <td>{{$row->id}}</td>
            <td>{{$row->account->account_name}}</td>
            <td>{{$row->debit}}</td>
            <td>{{$row->credit}}</td>
          </tr>
        @endforeach 
          <tr style="background-color: #c7c5c5;">
            <td></td>
            <td><b>Total</b></td>
            <td><b>{{ $debit_sum }}</b></td>
            <td><b>{{ $credit_sum }}</b></td>
          </tr>
      </tbody>
    </table>
    <b>Created By: {{ $data['journalVoucher']->createdBy->fname . ' '. $data['journalVoucher']->createdBy->lname }}</b>
</div>

</body>
</html>



