
<!-- Widget: user widget style 1 -->
	<div class="box box-widget widget-user">
	  <!-- Add the bg color to the header using any of the bg-* classes -->
	  <div class="widget-user-header bg-aqua-active">
		<h3 class="widget-user-username"></h3>
		<h5 class="widget-user-desc"></h5>
	  </div>
	  <div class="widget-user-image">
		<img class="img-circle" src="{{asset('img/default_avatar_male.jpg')}}" alt="User Avatar">
	  </div>
	  <div class="box-footer">
		<div class="row">
		  <div class="col-sm-12">
			<div class="description-block">
			  <h5 class="description-header"></h5>
			  <span class="description-text"></span><br>
			  <span class="description-text"><b>Employee Name: {{ $emp_details['fname'] }} {{ $emp_details['lname'] }}</b></span>
			</div>
			<!-- /.description-block -->
		  </div>
		</div>
		<!-- /.row -->
	  </div>
	</div>
	<!-- /.widget-user -->
	<div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Leave Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed responsvie" id="statustable">
                <tbody>
				<tr>
                  <th>Leave id</th>
				  <th>Employee</th>
				  <th>Action By</th>
				  <th>Comments</th>
				  <th>Status</th>
				  <th>Created at</th>
				  <th>Updated at</th>
				</tr>
				@foreach ($leave_approval_details as $LAD)
				<tr>
				  <td>{{$LAD['leave_id']}} </td>
				  <td>{{$LAD->leaveofuser['fname']}} {{$LAD->leaveofuser['lname']}}</td>
				  <td>{{$LAD->approvedby['fname']}} {{$LAD->approvedby['lname']}}</td> 
				  <td>{{$LAD['comments']}}</td>
				  <td>{{$LAD['status']}}</td>
				  <td>{{$LAD['created_at']->format('d-m-Y')}}</td>
				  <td>{{$LAD['updated_at']->format('d-m-Y')}}</td>
				  
				</tr>
				@endforeach
			  </tbody>
			</table>
            </div>
			<!-- /.box-body -->
          </div>

		  
		  
<div class="row">

        

</div>		  
		  
 
		  
		  
		  
