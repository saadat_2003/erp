@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Shift Team Leaves</h3>
              <span class="pull-right">
              @can('myleaves-create')             
                <!--<button type="button" name="create_record" id="create_record" class="btn btn-info"><span class="fa fa-plus"></span> Add New Leave</button>-->			
              @endcan
            </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user_table" class="display responsive wrap" style="width:100%">
                <thead>
                <tr>

                  <th>Id</th>
                  <th>Employee</th>
                  <th>Dated</th>
                  <th>Description</th>
                  <th>Leave Type</th>
                  <th>Paid Leave</th>
                  <th>Created By</th>
                  <th>Modified By</th>
                  <th>Created At</th>
                  <th>Status</th>

                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               	  
                </tbody>
                <tfoot>
                <tr>

                    <th>Id</th>
                    <th>Employee</th>
                    <th>Dated</th>
                    <th>Description</th>
                    <th>Leave Type</th>
                    <th>Paid Leave</th>
                    <th>Created By</th>
                    <th>Modified By</th>
                    <th>Created At</th>
                    <th>Status</th>

                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   


<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add status-Shift Manager</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <!--<div class="form-group">
            <label class="control-label col-md-4" >Dated : </label>
            <div class="col-md-8">
             <input type="date" class="form-control" id="dated" name="dated" placeholder="Enter Leave Date" autocomplete="off" required>
            </div>
           </div>-->
           <div class="form-group">
            <label class="control-label col-md-4">Comments : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="comments" name="comments" placeholder="Enter comments" autocomplete="off" required>
            </div>
           </div>
			<div class="form-group">
					<label class="control-label col-md-4">Status : </label>
					<div class="col-md-8">		
							<select class="form-control select2" id="statustype" name="statustype" required>
								<option value="0" selected>Select Status</option>    
								<option value="Approved">Approved</option>
								<option value="Rejected">Rejected</option>
							  </select>
					</div>		  
            </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">
				<input type="hidden" name="action" id="action" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				<input type="hidden" id='leaves_id_form' name='leaves_id_form' />
				<input type="hidden" id='leave_of_user' name='leave_of_user' />
				<input type="submit" name="action_button" id="action_button" class="btn btn-info" value="Add" />				
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>

<!--BypassLeave -->
<div id="formModalBypassLeave" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Bypass status</h4>
        </div>
        <div class="modal-body">
         <span id="form_result_bypass"></span>
         <form method="post" id="sample_form_bypass" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <!--<div class="form-group">
            <label class="control-label col-md-4" >Dated : </label>
            <div class="col-md-8">
             <input type="date" class="form-control" id="dated" name="dated" placeholder="Enter Leave Date" autocomplete="off" required>
            </div>
           </div>-->
           <div class="form-group">
            <label class="control-label col-md-4">Comments : </label>
            <div class="col-md-8">
             <input type="text" class="form-control" id="comments_bypass" name="comments_bypass" placeholder="Enter comments" autocomplete="off" required>
            </div>
           </div>
			<div class="form-group">
					<label class="control-label col-md-4">Status : </label>
					<div class="col-md-8">		
							<select class="form-control select2" id="statustype_bypass" name="statustype_bypass" required>
								<option value="0" selected>Select Status</option>    
								<option value="Approved">Approved</option>
								<option value="Rejected">Rejected</option>
								<option value="Bypass">Bypass</option>
							  </select>
					</div>		  
            </div>
		   		   
           <br />

			<div class="box-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			  <span class="pull-right">
				<input type="hidden" name="action_bypass" id="action_bypass" />
				<input type="hidden" name="hidden_id" id="hidden_id" />
				<input type="hidden" name="emp_id" id="emp_id" value="{{ auth()->user()->id }}" />
				<input type="hidden" id='leaves_id_form_bypass' name='leaves_id_form_bypass' />
				<input type="hidden" id='leave_of_user_bypass' name='leave_of_user_bypass' />
				<input type="submit" name="action_button_bypass" id="action_button_bypass" class="btn btn-info" value="Bypass" />				
			  </span>
			</div>		   
         </form>
        </div>
     </div>
    </div>
</div>

<!--Delete modal popup -->
<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>	          

		
		
		
	
		
		
		
        <!-- Show and Action Modal Begins -->   
      <div class="modal fade" id="modal-default-show">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body" id="showViewDetails">
             
            </div>
                <!-- /.box-body -->
  
                <div class="box-footer">
                  <span class="pull-right">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  </span>
                </div>
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->	
		
		
@endsection
@push('scripts')
<style>
.loading{
    display: hidden;
    position: fixed;
    left: 0;
    top: 0;
    padding-top: 45vh;
    padding-left:100vh;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: gray;
    opacity: 0.8;
}
.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<style>
.select2-container--classic .select2-selection--single .select2-selection__rendered{
    line-height: 35px;
    
}
.select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 8px;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #3c8dbc;
    border-radius: 4px;
}

</style>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.8.9/jquery.csv.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>



<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script>
$(document).ready(function(){

  $('body').tooltip({selector: '[data-toggle="tooltip"]'});

 $('#user_table').DataTable({
  processing: true,
  serverSide: true,
  "ordering": false,
  ajax:{
   url: "{{ route('leaves.shiftteam_leaves') }}",
  },
  columns:[

  
/*    {
    data: 'leaveapprovalcheck',
    name: 'leaveapprovalcheck'
   }, */

   
  
   {
    data: 'leaves_id',
    name: 'leaves_id'
   },
   {
    data: 'applicant',
    name: 'applicant'
   },
   {
    data: 'dated',
    name: 'dated'
   },
   {
    data: 'description',
    name: 'description'
   },
   {
    data: 'leavetype',
    name: 'leavetype'
   },
   {
    data: 'ispaid',
    name: 'ispaid'
   },
   
   {
    data: 'createdby',
    name: 'createdby'
   },
   {
    data: 'modified_by',
    name: 'modified_by'
   },
   {
    data: 'created_at',
    name: 'created_at'
   },   
   {
    data: 'status',
    name: 'status'
   },  
   
   

   {
    data: 'action',
    name: 'action',
   }
  ]
 });

//MyTeamLeave
 $(document).on('click', '.add_status', function(){
  $('.modal-title').text("Add Shift Leave Status");
     $('#action_button').val("Add");
     $('#action').val("Add");
//added by me
    $('#comments').val('');
    $('#form_result').html("");

    var id = $(this).attr('leave_id_ctrlr');
	var leave_of_user = $(this).attr('leave_of_user');
    //alert(id);
    document.getElementById("leaves_id_form").value = id;
	document.getElementById("leave_of_user").value = leave_of_user;
	
    event.preventDefault();	
	 
     $('#formModal').modal('show');
	 
 });
 
//BypassLeave
 $(document).on('click', '.bypass_leave', function(){
  $('.modal-title').text("Bypass Shift Leave Status");
     $('#action_button_bypass').val("Bypass");
     $('#action_bypass').val("Bypass");
//added by me
    $('#comments_bypass').val('');
    $('#form_result_bypass').html("");

    var id_bypass = $(this).attr('leave_id_ctrlr_bypass');
	var leave_of_user_bypass = $(this).attr('leave_of_user_bypass');
    //alert(id_bypass);
    document.getElementById("leaves_id_form_bypass").value = id_bypass;
	document.getElementById("leave_of_user_bypass").value = leave_of_user_bypass;
	
    event.preventDefault();	
	 
     $('#formModalBypassLeave').modal('show');
	 
 }); 

 $('#sample_form').on('submit', function(event){
  event.preventDefault();
  if($('#action').val() == 'Add')
  {
   $.ajax({
    url:"{{ route('shiftTeamLeavesApprovedRejected') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
      //for(var count = 0; count < data.errors.length; count++)
      //{
       html += '<p>' + data.errors + '</p>';
      //}
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      $('#user_table').DataTable().ajax.reload();
	  swal("Success!", "Leave status Added!", "success");
     }
     $('#form_result').html(html);
    }
   })
  }


  
/*   if($('#action').val() == "Edit")
  {
   $.ajax({
    url:"{{ route('leaves.myleaves_update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
	  swal("Failed", data.errors + "\r\nERROR.", "error");
      html = '<div class="alert alert-danger">';
      for(var count = 0; count < data.errors.length; count++)
      {
       html += '<p>' + data.errors[count] + '</p>';
      }
      html += '</div>';
     }
     if(data.success)
     {
		swal("Success", "Leave updated successfully JS.", "success");
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form')[0].reset();
      $('#user_table').DataTable().ajax.reload();
     }
     $('#form_result').html(html);
    }
   });
  } */
 });

/*  $(document).on('click', '.edit', function(){
  var id = $(this).attr('id');
  $('#form_result').html('');
  $.ajax({
   url:"/myleaves/"+id+"/edit",
   dataType:"json",
   success:function(html){
	   var d = Date.parse("March 21, 2012");
    $('#dated').val(html.data.dated);
		var date = new Date(html.data.dated);
		var dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
				.toISOString()
				.split("T")[0];
	$('#dated').val(dateString);	
    $('#description').val(html.data.description);
	$('#leavetype').val(html.data.leavetype);
	
	$('#hidden_id').val(html.data.id);
    $('.modal-title').text("Edit My Leave");
    $('#action_button').val("Edit");
    $('#action').val("Edit");
    $('#formModal').modal('show');
   }
  })
 }); */
 
//showViewDetails 
 $(document).on('click', '.viewdetails', function(){
	 event.preventDefault();
  var leave_id = $(this).attr('data-id');
  var emp_id = $(this).attr('emp_id');
  
  //alert(leave_id);
  //alert(emp_id);
  //$('#form_result').html('');
  $.ajax({
   "url": "{{route('leaves.shiftteam_leaves_show')}}",
   //dataType:"json",
   type: "POST",
   data: {'id': leave_id, 'emp_id': emp_id,_token: '{{csrf_token()}}'},
   success:function(data){
          $('#modal-default-show').modal('toggle');
          $('#showViewDetails').html(data);
	
	
    //$('#showViewDetails').modal('show');
   },
   //error: function(){},          
  })
 }); 
 
 
 $('#sample_form_bypass').on('submit', function(event){
  event.preventDefault();
//BypassLeave
  if($('#action_bypass').val() == 'Bypass')
  {
   $.ajax({
    url:"{{ route('myTeamLeavesBypass') }}",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
     var html = '';
     if(data.errors)
     {
      html = '<div class="alert alert-danger">';
      for(var count = 0; count < data.errors.length; count++)
      {
       html += '<p>' + data.errors[count] + '</p>';
      }
      html += '</div>';
	  swal("Error!", "Error!", "error");
     }
     if(data.success)
     {
      html = '<div class="alert alert-success">' + data.success + '</div>';
      $('#sample_form_bypass')[0].reset();
      $('#user_table').DataTable().ajax.reload();
	  swal("Bypass!", "Leave data Bypassed!", "success");
     }
     $('#form_result_bypass').html(html);
    }
   })
  }  
  
 });

 var user_id;

 $(document).on('click', '.delete', function(){
  user_id = $(this).attr('id');
  $('#confirmModal').modal('show');
 });

 $('#ok_button').click(function(){
  $.ajax({
   url:"myleaves/myleaves_destroy/"+user_id,
   beforeSend:function(){
    $('#ok_button').text('Deleting...');
   },
   success:function(data)
   {
    setTimeout(function(){
     $('#confirmModal').modal('hide');
     $('#user_table').DataTable().ajax.reload();
    }, 2000);
   }
  })
 });
 
 
 
 
 
 
 
 
 



 

});
</script>		

@endpush